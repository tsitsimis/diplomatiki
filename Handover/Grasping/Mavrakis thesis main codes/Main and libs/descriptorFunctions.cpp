

#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <string>
#include <vector>
#include <fstream>

#include <alproxies/alvideodeviceproxy.h>
#include <alproxies/almotionproxy.h>
#include <alvision/alimage.h>
#include <alvision/alvisiondefinitions.h>
#include <alerror/alerror.h>


using namespace std;
using namespace cv;
using namespace AL;



// Pca and global r-theta descriptor extraction in an binary shape or contour
// Input: dst = contour image
// Output: descriptor = global descriptor matrix (normalised)
// 			format
// 			r1 | theta1
// 			r2 | theta2
// 			...
void pcaComputation(Mat dst,Mat& descriptor)
{
	// Declarations	
	int i,j;


	// Thresholding
	double thrValue,thresh;
	thresh = 40;
	thrValue = threshold(dst,dst,thresh,255,THRESH_BINARY);


	// Contour finding
	vector<Point> contours,contoursChanged;
	vector<vector<Point> >	contours1;
	Point po;
	
	Mat cont = Mat::zeros(dst.size(), dst.type());
	Mat area = dst.clone();
	Mat edges;

	findContours(area, contours1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);	
	drawContours(area, contours1, -1, Scalar::all(255), -1);

	Canny(area, edges, 10, 50, 3,false);
	for (i=0; i<edges.rows; i++)
		for (j=0; j<edges.cols; j++)
			if (edges.at<uchar>(i,j) == 255)
			{
				po.x = j;
				po.y = i;
				contours.push_back(po);
			}


	// Find the image centroid
	Moments m;
	float xm,ym,ymChanged;
	m = moments(area,true);

	xm = (m.m10)/(m.m00);
	ym = (m.m01)/(m.m00);

	xm = cvRound(xm);
	ym = cvRound(ym);

	Point2f centroid;
	centroid.x = xm;
	centroid.y = ym;


	// Change the image coordinate system, from top-left to bottom-left
	contoursChanged = contours;
	for (i=0 ; i<contours.size(); i++)
		contoursChanged[i].y = area.rows - contours[i].y;


	// Change the centroid coordinate system
	ymChanged = area.rows- ym;

	
	// Preparing the contour points for PCA
	Mat R(contoursChanged,true);
	Mat T = R.reshape(1,0);
	Mat mean;
	R.convertTo(R,CV_32F);
	T.convertTo(T,CV_32F);


	// PCA computation
	PCA pca(T,mean,CV_PCA_DATA_AS_ROW,0);

	
	// Projection to the PCA coordinate system
	Mat K(R.rows,R.cols,CV_32F);
	pca.project(T,K);
	K = K.reshape(1,0);	


	// Centroid and principal axes creation (used in imshow later)
	Point2f p1,p2;
	Point2f	p;
	p1.x = cvRound( pca.eigenvectors.at<float>(0,0))+xm;
	p1.y = area.rows - (ymChanged + cvRound( pca.eigenvectors.at<float>(0,1)));
	p2.x = cvRound(pca.eigenvectors.at<float>(1,0))+xm;
	p2.y = area.rows - (ymChanged +  cvRound(pca.eigenvectors.at<float>(1,1)));

	p1.x = (p1.x-centroid.x)*50 + p1.x;
	p1.y = (p1.y-centroid.y)*50 + p1.y;
	p2.x = (p2.x-centroid.x)*20 + p2.x;
	p2.y = (p2.y-centroid.y)*20 + p2.y;

	Mat cont1;
	cont1 = dst.clone();
	cvtColor(cont1, cont1, CV_GRAY2RGB);
	line(cont1, centroid,p1,  Scalar(0, 0, 255), 1, 8);
	line(cont1, centroid,p2,  Scalar(0, 0, 255), 1, 8);
	circle(cont1, centroid, 3, Scalar(0, 255,255 ) , -1, 8, 0);

	
	// Conversion of the projected cartesian points to polar
	Mat r,theta;
	cartToPolar(K.col(0),K.col(1),r,theta,true);


	// Sort the radius and angle matrices (ascending)
	float temp;
	Point2f tempP;
	for(i=0; i<theta.rows-1; i++)
		for(j=0; j<(theta.rows-i-1); j++)
		{
			if (theta.at<float>(j,0) > theta.at<float>(j+1,0))
			{
				temp = theta.at<float>(j,0);
				theta.at<float>(j,0) = theta.at<float>(j+1,0);
				theta.at<float>(j+1,0) = temp ;

				temp = r.at<float>(j,0);
				r.at<float>(j,0) = r.at<float>(j+1,0);
				r.at<float>(j+1,0) = temp ;

				tempP = contours[j];
				contours[j] = contours[j+1];
				contours[j+1] = tempP;
			}
		}


	//Find the max radius
	float maxR = r.at<float>(0,0);
	for(i=0;i<r.rows; i++)
		if (r.at<float>(i,0) > maxR)
			maxR = r.at<float>(i,0);


	// Normalize the radius matrix
	r = r/(maxR);


	// Merge the radius and angle matrices
	Mat signature(r.rows,2,CV_32F);
	r.col(0).copyTo(signature.col(0));
	theta.col(0).copyTo(signature.col(1));


	// Show the results	
	namedWindow("Centroid and axes");
	imshow("Centroid and axes",cont1);
	imwrite("pca.jpg",cont1);


	// Saves
	//imwrite( "Ball area.jpg", area );
	//imwrite( "Toothbrush contour.jpg", cont );
	//imwrite( "Circle centroid and axes.jpg", cont1 );
	
	descriptor = signature.clone();
}








// Create the r-theta local descriptors
// Input: Binary contour image of the object 
// Outputs: localDescr = Descriptor matrix. Each column is on the format 
//
// 			point.x
// 			point.y
// 			descriptor values..
// 			..
// 			..
// sp = Sampled contour points
// contourPoints = All contour points
// oppositePoints =  Opposite contour points (each sampled contour point has its' normal opposite. The matrices have correspondance to each element)
//
void localDescriptorExtraction(Mat contImage,Mat& localDescr,Mat& sp, Mat& contourPoints ,Mat& oppositePoints)
{
	
	// Declarations
	int i, j,k,l;

	Mat filled,contImage1;
	contImage.copyTo(filled);
	contImage.copyTo(contImage1);
	vector<vector<Point> >  contours;
	findContours(contImage1, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);	
	drawContours(filled, contours, -1, Scalar::all(255), -1);
Mat tempImage;
	tempImage = contImage.clone();

	// Local r-theta descriptors for circle	
	vector<Point> cCont;
	for (i=0; i<contImage.rows; i++)
		for (j=0; j<contImage.cols; j++)
			if(contImage.at<uchar>(i,j) == 255 )
		 	{
				Point po;
				po.x = j;
				po.y = contImage.rows - i;
				cCont.push_back(po);
			}


	// PCA computation	
	Mat R(cCont,true);
	Mat T = R.reshape(1,0);
	Mat mean;
	R.convertTo(R,CV_32F);
	T.convertTo(T,CV_32F);

	PCA pca(T,mean,CV_PCA_DATA_AS_ROW,0);
	

	// Projection to the PCA coordinate system
	Mat K(R.rows,R.cols,CV_32F);
	pca.project(T,K);
	K = K.reshape(1,0);	


	// Transformation to r-�
	Mat cR,cTheta;
	cartToPolar(K.col(0),K.col(1),cR,cTheta,true);


	// Sort the radius and angle matrices (ascending)
	float temp;
	Point2f tempP;
	for(i=0; i<cTheta.rows-1; i++)
		for(j=0; j<(cTheta.rows-i-1); j++)
		{
			if (cTheta.at<float>(j,0) >= cTheta.at<float>(j+1,0))
			{
				temp = cTheta.at<float>(j,0);
				cTheta.at<float>(j,0) = cTheta.at<float>(j+1,0);
				cTheta.at<float>(j+1,0) = temp ;

				temp = cR.at<float>(j,0);
				cR.at<float>(j,0) = cR.at<float>(j+1,0);
				cR.at<float>(j+1,0) = temp ;

				tempP = cCont[j];
				cCont[j] = cCont[j+1];
				cCont[j+1] = tempP;
			}
		}


	// Change the image y coordinates back to normal
	for( i = 0; i< cCont.size(); i++)
		cCont[i].y = contImage.rows - cCont[i].y;


	// Sample the shape contour and fit a line at the next 5 points
	Point2f p1,p2,pl,p3,p4;
	Mat allLines;
	Mat line;
	vector<Point> points, sampledPoints,normalPoints;
	int step = 10;

	vector<Point2f> v1;
	vector<Point2f>  v2;

	int found;

	for (i=0; i<cCont.size(); i=i+step)
	{
		if ( (i+step) < cCont.size() )
		{
			for (j=0; j<step-1; j++)
				points.push_back(cCont[i+j]);

			fitLine(points, line,CV_DIST_L1, 0, 0.01, 0.01);
	
			p1.x = line.at<float>(0,0);
			p1.y = line.at<float>(1,0);
			v1.push_back(p1);

			p2.x = line.at<float>(2,0);
			p2.y = line.at<float>(3,0);
			v2.push_back(p2);

			sampledPoints.push_back(cCont[i]);
						
			

			points.clear();

		}
		else
		{
//			sampledPoints.push_back(cCont[cCont.size()-1]);
			break;
		}
	}


 	
	// Save the sampled contour to Mat form
	cvtColor(contImage,contImage,CV_GRAY2RGB);
	Mat samPoints(sampledPoints.size(),2,CV_32F);
	for(i=0; i<sampledPoints.size(); i++)
	{
		circle(contImage,sampledPoints[i],2,Scalar(0,0,255),-1,8,0);
		samPoints.at<float>(i,0) = sampledPoints[i].x;
		samPoints.at<float>(i,1) = sampledPoints[i].y;
	}
	namedWindow("sp");
	imshow("sp",filled);
	imwrite("sampledContour.jpg",contImage);
	waitKey(0);

	FileStorage fs("CircleCont.yml", FileStorage::WRITE);
	FileStorage fs1("Circle.yml", FileStorage::WRITE);
	fs << "sampledContour" << samPoints;


	// Extract the normal point (normal opposite on the contour) for each sampled point
	int coun=0;
	int key;
	Point q1,q2,q3,q4,q5,q6,q,q7,q8;
	for (i=0; i<sampledPoints.size(); i++)
	{
		j = 2;
		q1 =  Point(cvRound(v2[i].x - v1[i].y*j),cvRound(v2[i].y + v1[i].x*j));
		q5 =  Point(cvRound(v2[i].x + v1[i].y*j),cvRound(v2[i].y - v1[i].x*j));

//		q7 =  Point(cvRound(v2[i].x - 10*v1[i].y*j),cvRound(v2[i].y + 10*v1[i].x*j));
	//	q8 =  Point(cvRound(v2[i].x + 10*v1[i].y*j),cvRound(v2[i].y - 10*v1[i].x*j));



	////	circle(contImage,normalPoints[i],2,Scalar(0,255,255),-1,8,0);
		//	cv::	line(contImage, sampledPoints[i],q1,Scalar(255,120,255),1,8,0);
		//	cv::	line(contImage, sampledPoints[i],q5,Scalar(255,120,255),1,8,0);

//		imshow("sp",contImage);
//	waitKey(0);

		if (filled.at<uchar>(q1.y,q1.x) == 255)
		{
			key = 1;
		}
		else if (filled.at<uchar>(q5.y,q5.x) == 255)
		{
			key = 2;
		}
		found =0;
		cout <<key;
		while ( (found ==0) )
		{
			if (key == 1)
			{
				q1 =  Point(cvRound(v2[i].x - v1[i].y*j),cvRound(v2[i].y + v1[i].x*j));
				if ((q1.x < 320) && ((q1.y < 240)) && (q1.x > 0) && ((q1.y > 0)) )
				{
				
					q2 =  Point(cvRound(v2[i].x - v1[i].y*(j+1)),cvRound(v2[i].y + v1[i].x*(j+1)));
					q3.x = q1.x;
					q3.y = q2.y;
					q4.x = q2.x;
					q4.y = q1.y;

	
					for(k=0; k< cCont.size(); k++)
					{
						if ( (q1.x == cCont[k].x)  &&   (q1.y == cCont[k].y))
						{
							q = q1;
							found = 1;
							break;
						}
						else if (( (q3.x == cCont[k].x)  &&   (q3.y == cCont[k].y)))
						{
						        q = q3;
							found = 1;
							break;
						}
						else if (( (q4.x == cCont[k].x)  &&   (q4.y == cCont[k].y)) )
						{
							q = q4;
							found = 1;
							break;
						}		

					}
				}
			}
			else if (key == 2)
			{
				q5 =  Point(cvRound(v2[i].x + v1[i].y*j),cvRound(v2[i].y - v1[i].x*j));
				if ((q5.x < 320) && ((q5.y < 240)) && (q5.x > 0) && ((q5.y > 0)) )
				{
					q6 =  Point(cvRound(v2[i].x + v1[i].y*(j+1)),cvRound(v2[i].y - v1[i].x*(j+1)));
					q3.x = q5.x;
					q3.y = q6.y;
					q4.x = q6.x;
					q4.y = q5.y;

	
					for(k=0; k< cCont.size(); k++)
					{
						if ( (q5.x == cCont[k].x) && (q5.y == cCont[k].y) )
						{
							q = q5;
							found = 1;
							break;
						}
						else if  ( (q3.x == cCont[k].x) && (q3.y == cCont[k].y) )
						{
						        q = q3;
							found = 1;
							break;
						}
						else if   ( (q4.x == cCont[k].x) && (q4.y == cCont[k].y) )
						{
							q = q4;
							found = 1;
							break;
						}		

					}
				
				}			
			}
			j++;
			
		}
		normalPoints.push_back(q);
//		cout <<q;
//		circle(contImage,normalPoints[i],2,Scalar(0,255,255),-1,8,0);
//			cv::	line(contImage, sampledPoints[i],q,Scalar(255,0,255),1,8,0);

//		imshow("sp",contImage);
//	waitKey(0);
	}



	// Calculate the r distances for each sampled point, from -35 deg to 35 deg
	int angleBord = 35;
	Mat descriptors(2*angleBord/5 +2, sampledPoints.size(), CV_64F);
	Mat sp;
	Mat distances;
	vector<Point2d> chosenPoints;
	vector<Point2d> chosenQ,left,right;
	double limitAngle = 60;
	double testAng;
	Point2d testPoint;
	vector<Point> v;
	Mat dMat;
	vector<double> dist;
	double di ;

	for(i=0; i<sampledPoints.size(); i++)
	{
		v.push_back(normalPoints[i]);
		v.push_back(sampledPoints[i]);

		dMat.push_back(sampledPoints[i].x);
		dMat.push_back(sampledPoints[i].y);
		dMat.convertTo(dMat,CV_64F);

		fitLine(v, line,CV_DIST_L1, 0, 0.01, 0.01);
	
		p1.x = line.at<float>(0,0);
		p1.y = line.at<float>(1,0);

		for(j=-angleBord ; j<angleBord+5; j=j+5)
		{
			found = 0;
			k=5;

			while (found ==0 )
			{
				if (sampledPoints[i].x >= normalPoints[i].x)
				{
					q1 =  Point( cvRound (sampledPoints[i].x+( cos(j*CV_PI / 180)*p1.x-sin(j*CV_PI / 180)*p1.y )*(-k)) , cvRound( sampledPoints[i].y+ ( sin(j*CV_PI / 180)*p1.x+cos(j*CV_PI / 180)*p1.y)*(-k) ));
					q2 =  Point( cvRound (sampledPoints[i].x+( cos(j*CV_PI / 180)*p1.x-sin(j*CV_PI / 180)*p1.y )*(-k-1)) , cvRound( sampledPoints[i].y+ ( sin(j*CV_PI / 180)*p1.x+cos(j*CV_PI / 180)*p1.y)*(-k-1) ));
				}
				else
				{
					q1 =  Point( cvRound (sampledPoints[i].x+( cos(j*CV_PI / 180)*p1.x-sin(j*CV_PI / 180)*p1.y )*k) , cvRound( sampledPoints[i].y+ ( sin(j*CV_PI / 180)*p1.x+cos(j*CV_PI / 180)*p1.y)*k ));
					q2 =  Point( cvRound (sampledPoints[i].x+( cos(j*CV_PI / 180)*p1.x-sin(j*CV_PI / 180)*p1.y )*(k+1)) , cvRound( sampledPoints[i].y+ ( sin(j*CV_PI / 180)*p1.x+cos(j*CV_PI / 180)*p1.y)*(k+1) ));
				}


				q3.x = q1.x;
				q3.y = q2.y;
				q4.x = q2.x;
				q4.y = q1.y;

//					circle(cir,q1,0,Scalar(255,0,255),-1,8,0);
//					circle(cir,q3,0,Scalar(255,0,255),-1,8,0);
//				circle(cir,q4,0,Scalar(255,0,255),-1,8,0);
//						imshow("circle",cir);
//				waitKey(0);


				if ((q1.x >= 320) || ((q1.y >= 240)) || (q1.x <= 0) || ((q1.y <= 0)) )
				{
//					q = Point(0,0);
					q = q1;
					found = 1;
				}
				else
				{
					for(l=0; l< cCont.size(); l++)
					{
						if ( (q1.x == cCont[l].x) && (q1.y == cCont[l].y) )
						{
							q = q1;
							found = 1;
							break;
						}
						else if  ( (q3.x == cCont[l].x) && (q3.y == cCont[l].y) )
						{
						        q = q3;
							found = 1 ;
							break;
						}
						else if   ( (q4.x == cCont[l].x) && (q4.y == cCont[l].y) )
						{
							q = q4;
							found = 1;
							break;
						}	

					}
				}
				k++;
			}
//if ( (sampledPoints[i].x == sampledPoints[30].x) && (sampledPoints[i].y == sampledPoints[30].y) )
//					{
//				cv::	circle(contImage,q,3,Scalar(255,0,0),-1,8,0);
//				cv::	line(contImage, sampledPoints[i],q,Scalar(255,120,0),1,8,0);
//					}
			if (j==0)
			{
				double vl;
				testPoint.x =   -sampledPoints[i].x + q.x;
				testPoint.y =   -sampledPoints[i].y + q.y;
				testAng = atan2(testPoint.y,testPoint.x);
				vl = (testAng/CV_PI)*180 ;

				if ((testAng/CV_PI)*180 < 0 ) 
					vl = 360 + (testAng/CV_PI)*180; 

				if ( (vl <= 295) && (vl >=160) && (((q.x < 320) && ((q.y < 240)) && (q.x > 0) && ((q.y > 0)) )) )
				{
					chosenPoints.push_back(sampledPoints[i]);
					chosenQ.push_back(q);
	//			cv::	circle(contImage,sampledPoints[i],3,Scalar(255,255,0),-1,8,0);
			//	cv::	line(contImage, sampledPoints[i],q,Scalar(0,0,255),1,8,0);
				}

			}
			if (j== -angleBord)
			{
				left.push_back(q);
		//			if ( (sampledPoints[i].x == sampledPoints[30].x) && (sampledPoints[i].y == sampledPoints[30].y) )
		//			{
		//		cv::	circle(contImage,q,3,Scalar(255,0,0),-1,8,0);
		//		cv::	line(contImage, sampledPoints[i],q,Scalar(255,120,0),1,8,0);
			//		}
			}
			if (j== angleBord)
			{
				right.push_back(q);
		//			if ( (sampledPoints[i].x == sampledPoints[30].x) && (sampledPoints[i].y == sampledPoints[30].y) )
		//	{
		//			cv::	circle(contImage,q,3,Scalar(255,0,0),-1,8,0);
		//		cv::	line(contImage, sampledPoints[i],q,Scalar(255,120,0),1,8,0);
		//		imshow("sp",contImage);
		///		imwrite("Image.jpg",contImage);
		//		waitKey(0);
		//	}
			}
		//	imshow("sp",contImage);
	//		waitKey(0);
		

			dist.push_back(q.x-sampledPoints[i].x);
			dist.push_back(q.y-sampledPoints[i].y);
			di = norm(dist, NORM_L2,noArray());
			dist.clear();
			sp.push_back(di);	
			vconcat(dMat,sp,descriptors.col(i));	

		}
	dMat.release();
	v.clear();
	sp.release();
	distances.release();
	}



	// Create the final r-� descriptor for each sampled point
	Mat finalDescr(descriptors.rows,chosenPoints.size(),CV_64F);
	Mat leftM(chosenPoints.size(), 2, CV_64F);
	Mat rightM(chosenPoints.size(), 2, CV_64F);
	Mat opposite(chosenPoints.size(), 2, CV_64F);
	Mat contour(chosenPoints.size(), 2, CV_64F);

	k = 0;

	for (i=0; i< descriptors.cols; i++)
		for (j=0; j< chosenPoints.size(); j++)
			{
				if ( (descriptors.at<double>(0,i) == chosenPoints[j].x) && (descriptors.at<double>(1,i) == chosenPoints[j].y))
				{
					descriptors.col(i).copyTo(finalDescr.col(k));
			          	leftM.at<double>(k, 0) = left[i].x;
			          	leftM.at<double>(k, 1) = left[i].y;

			          	rightM.at<double>(k, 0) = right[i].x;
			          	rightM.at<double>(k, 1) = right[i].y;

					opposite.at<double>(k, 0) = chosenQ[j].x;
			          	opposite.at<double>(k, 1) = chosenQ[j].y;

					contour.at<double>(k, 0) = chosenPoints[j].x;
			          	contour.at<double>(k, 1) = chosenPoints[j].y;

					k++;
				}
				
			}

	

	// Normalize the r distances
	for (i=0; i< finalDescr.cols; i++)
	{
		double max = finalDescr.at<double>(2,i);
		for (j=3; j<finalDescr.rows; j++)
			if ( (finalDescr.at<double>(j,i)) >= max )
				max = finalDescr.at<double>(j,i);
		for (j=2; j<finalDescr.rows; j++)
			finalDescr.at<double>(j,i) = finalDescr.at<double>(j,i)/max;
	}


//	FileStorage desc("descriptor.yml", FileStorage:: WRITE);
//	desc << "finalDescr" << finalDescr;
//	desc << "left" << leftM;
//	desc << "right" << rightM;

	
	localDescr =  finalDescr.clone();
	sp = samPoints;
	oppositePoints = opposite.clone();
	contourPoints = contour.clone();
	
	

	// Code for ground truth image
	/*
	cvtColor(tempImage,tempImage,CV_GRAY2RGB);
	Mat mpla;
	tempImage.copyTo(mpla);
	int answer;
	for (i=0;i<chosenPoints.size();i++)
	{	
		tempImage.copyTo(mpla);
		namedWindow("sp1");
		Point n;
		n.x = chosenPoints[i].x;
		n.y = chosenPoints[i].y;
		cv::	circle(mpla,n,3,Scalar(0,255,0),-1,8,0);
	//	cv::	circle(mpla,n,3,Scalar(0,0,255),-1,8,0);
		imshow("sp1",mpla);
		waitKey(0);
		
	////	cout << "Select color 1-0-2";
	//	cin >> answer;
	//	if (answer ==1)
	//			cv::	circle(tempImage,n,3,Scalar(0,255,0),-1,8,0);
	//	if (answer ==2)
	//			cv::	circle(tempImage,n,3,Scalar(130,130,130),-1,8,0);
	//////	if (answer ==3)
//				cv::	circle(tempImage,n,3,Scalar(255,0,0),-1,8,0);

		destroyWindow("sp1");

		
	}
	imwrite("Ground Truth.jpg",tempImage);
	*/
}








// Training an SVM
// Input: sampledContour = sampled contour 
// 	  matrix = local descriptor matrix
// 	  image = image of the selected object
// 	  contourPoints = All contour points	
// 	  oppositePoints = opposite points
// 	  Trains an SVM according to a TrainData.yml file 
//
void svmTrain(Mat sampledContour, Mat matrix, Mat& image, Mat& contourPoints, Mat& oppositePoints)
{

	// Declarations and train data reading
	int i,j;
	Mat trainDesc,trainLabels;
	FileStorage trainData("Traindata.yml", FileStorage:: READ);
	trainData["trainDesc"] >> trainDesc;
	trainData["trainLabels"]>> trainLabels;
	
	// SVM setup and training
	CvSVM svm;

	CvSVMParams params,optParams;
	params.svm_type    = CvSVM::C_SVC;
//	params.svm_type    = CvSVM::C_SVC;
	params.kernel_type = CvSVM::POLY;
	params.degree = 8;
	params.gamma = 0.9;
	params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);

	trainLabels.convertTo(trainLabels, CV_32F);
	trainDesc.convertTo(trainDesc, CV_32F);

	svm.train(trainDesc, trainLabels, Mat(), Mat(), params);
	cvtColor(image,image,CV_GRAY2RGB);


	// Visualisation of the selected points
	Mat assignedLabels(matrix.cols,3,CV_32F);
	int count=0;
	for (i=0; i<matrix.cols; i++)
	{
		Point2f p;
		p.x = cvRound (matrix.at<double>(0,i));
		p.y = cvRound( matrix.at<double>(1,i));

		Mat sampleMat = matrix(Range(2,matrix.rows),Range(i,i+1));
		sampleMat.convertTo(sampleMat, CV_32F);
		
		float response = svm.predict(sampleMat.t());
		
		assignedLabels.at<float>(i,0) = p.x;
		assignedLabels.at<float>(i,1) = p.y;
		assignedLabels.at<float>(i,2) = response;


		if (response == 1)
		{
			cv::circle(image,p,3,Scalar(0,255,0),-1,8,0);
			count++;
		}
                else if (response == 0)
			cv::circle(image,p,3,Scalar(0,0,255),-1,8,0);
		else if (response == -1)
			cv::circle(image,p,3,Scalar(120,0,120),-1,8,0);

	}
		

	Mat goodPoints = Mat(count,2,CV_32F);
	int k=0;
	for(i=0; i<assignedLabels.rows; i++)
	{
		if (assignedLabels.at<float>(i,2) == 1)
		{
			goodPoints.at<float>(k,0) = assignedLabels.at<float>(i,0);
			goodPoints.at<float>(k,1) = assignedLabels.at<float>(i,1);
			k++;
		}
	}
	// Label assignment
	Mat finalLabels(sampledContour.rows,3,CV_32F);

	for (i=0; i <finalLabels.rows; i++)
		finalLabels.at<float>(i,2) = -1;

	for (i=0; i <sampledContour.rows; i++)
	{
		for(j=0; j<assignedLabels.rows; j++)
		{
			if ( (sampledContour.at<uchar>(i,0) == assignedLabels.at<float>(j,0)) && (sampledContour.at<uchar>(i,1) == assignedLabels.at<float>(j,1)) )
			{
				finalLabels.at<float>(i,0) = sampledContour.at<uchar>(i,0);
				finalLabels.at<float>(i,1) = sampledContour.at<uchar>(i,1);
				finalLabels.at<float>(i,2) = assignedLabels.at<float>(j,2);
				break;

			}
		}
	}


	// Contour sweeping to integrate rogue points
	for (i=1; i<assignedLabels.rows-1; i++)
		if ( (assignedLabels.at<float>(i,2) == 0) && (assignedLabels.at<float>(i-1,2) == 1) && (assignedLabels.at<float>(i+1,2) == 1) )
		{
			assignedLabels.at<float>(i,2) = 1;
			cv::circle(image,Point(cvRound(assignedLabels.at<float>(i,0)), cvRound(assignedLabels.at<float>(i,1))),3,Scalar(0,255,0),-1,8,0);
		}

	for (i=1; i<assignedLabels.rows-1; i++)
		if ( (assignedLabels.at<float>(i,2) == 1) && (assignedLabels.at<float>(i-1,2) == 0) && (assignedLabels.at<float>(i+1,2) == 0) )
		{
			assignedLabels.at<float>(i,2) = 0;
			cv::circle(image,Point(cvRound(assignedLabels.at<float>(i,0)), cvRound(assignedLabels.at<float>(i,1))),3,Scalar(0,0,255),-1,8,0);
		}




	imshow("image", image);
			waitKey(0);
	for (i=0; i<finalLabels.rows; i++)
	{
		Point2f q;
		q.x = finalLabels.at<float>(i,0);
		
		}

	// Median computation 
	int flag=0;
	int changed = 0;
	vector<Point2f> temp;
	int median;
	for (i=0; i<finalLabels.rows; i++)
	{

		if (  (finalLabels.at<float>(i,2) == 1)  ) 
		{
			Point2f q;
			q.x = finalLabels.at<float>(i,0);
			q.y = finalLabels.at<float>(i,1);
			temp.push_back(q);
			flag = 1;
		}
		else if ( (finalLabels.at<float>(i,2) == 0) && (flag == 1)  )
		{


			median = cvRound((temp.size())/2);
			cout <<"temp" << temp.size();
			cout << "\n median \n" << median;
			cv::circle(image,temp[median],3,Scalar(90,10,120),-1,8,0);
			cout << "\n the median is"  << temp[median];
			temp.clear();
			imshow("image", image);
			waitKey(0);
			flag = 0;
			changed = 1;

		}
	}

	if (changed == 0)
	{
		median = cvRound((temp.size())/2);
			cout <<"temp" << temp.size();
			cout << "\n median \n" << median;
			cv::circle(image,temp[median],3,Scalar(90,10,120),-1,8,0);
			cout << "\n the median is"  << temp[median];
			temp.clear();
			imshow("image", image);
			waitKey(0);
	}
	
	imshow("image", image);
	waitKey(0);


	Mat goodPointsOpposite = Mat(count,2,CV_32F);
	k = 0;
	for(i=0; i<goodPoints.rows; i++)
	{
		for (j=0; j<contourPoints.rows; j++)
			if (  (goodPoints.at<float>(i,0) == contourPoints.at<double>(j,0) ) &&  (goodPoints.at<float>(i,1) == contourPoints.at<double>(j,1) ) )
			{
				goodPointsOpposite.at<float>(k,0) = oppositePoints.at<double>(j,0);
				goodPointsOpposite.at<float>(k,1) = oppositePoints.at<double>(j,1);
				k++;
			}
	}
	imshow("image", image);
	waitKey(0);
	imwrite("Sample.jpg",image);

	contourPoints = goodPoints.clone();
	oppositePoints = goodPointsOpposite.clone();
	
}

