

#ifndef DESCRIPTOREXTRACTIONFUNCTIONS_HPP
#define DESCRIPTOREXTRACTIONFUNCTIONS_HPP
 
#include <iostream>
#include <string>
#include <vector>
 
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>

using namespace cv;


void pcaComputation(Mat contImage,Mat& descriptor);	// Function for computing the PCA of a contour
void localDescriptorExtraction(Mat contImage, Mat& localDescr,Mat& sv, Mat& contourPoints, Mat& oppositePoints);	// Function for extracting the local r-� descriptors;
void svmTrain(Mat sampledContour , Mat matrix, Mat& image, Mat& contourPoints, Mat& oppositePoints);
 
#endif

