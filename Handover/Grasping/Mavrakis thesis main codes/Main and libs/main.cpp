



// Main thesis program
// Incorporates all the needed functions



// Aldebaran includes.
#include <alproxies/alvideodeviceproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <alvision/alimage.h>
#include <alvision/alvisiondefinitions.h>
#include <alerror/alerror.h>
#include <alremoteproxies/almotionproxyremote.h>
#include <alvalue/alvalue.h>




// Opencv includes.
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>



// Other includes
#include <iostream>
#include <string>
#include "visionFunctions.hpp"
#include "descriptorFunctions.hpp"
#include <math.h>
#include <fstream>
#include <vector>
#include "windows.h" 


// Namespaces
using namespace AL;
using namespace std;
using namespace cv;



int main(int argc, char* argv[])
{

  if (argc < 2)
  {
    cerr << "Usage thesis robotIp'" << endl;
    return 1;
  }

  // Robot ip
  const string robotIp(argv[1]);

  try
  {
	// Proxies for the project
	ALVideoDeviceProxy camProxy(robotIp, 9559);		// Camera proxy	
	ALMotionProxy motionProxy(robotIp, 9559);		// Motion proxy
	ALTextToSpeechProxy ttsProxy(robotIp,9559);		// Text to speech proxy

	// Take the background picture
	Mat background;
	getPicture(camProxy,background);

	imshow("background.jpg", background);

	// Notify to put the object 
	ttsProxy.say("Please give me an object to grab");
	Sleep(3000);
	// Take the 1st picture
	Mat image1 = Mat(Size(320, 240), CV_8UC3);
	getPicture(camProxy,image1);

	// Display the 1st picture on screen.
	namedWindow("Image 1");
	imshow("Image 1", image1);

	imwrite("Image1.jpg",image1);


imwrite("Image1.jpg",image1);

	

	



	// Segment the object to grab, from the 1st image
	Mat contourImage;
	Mat _3DObject = _3DImage.clone();
	segmentImage(background,image1,contourImage);

		namedWindow("Im 2");
	imshow("Im 2", contourImage);
	
	imwrite("cont_image.jpg",contourImage);
	waitKey(0);

	Mat contourImage;
	contourImage = imread("complex1.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	

	Size patternsize(7,5); //interior number of corners
//	cvtColor(image1,image1,CV_RGB2GRAY);
vector<Point2f> corners; //this will be filled by the detected corners

//CALIB_CB_FAST_CHECK saves a lot of time on images
//that do not contain any chessboard corners
//bool patternfound = findChessboardCorners(image1, patternsize, corners,
  //      CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
    //    + CALIB_CB_FAST_CHECK);

//if(!patternfound)
//		cout <<"not found";
 // cornerSubPix(image1, corners, Size(11, 11), Size(-1, -1),
   // TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


//drawChessboardCorners(image1, patternsize, Mat(corners), patternfound);
//		namedWindow("Image 3");
//	imshow("Image 3", image1);
//waitKey(0);
	


	// Fill the contour image (for the PCA centroid calculation)
//	vector<vector<Point> >	contours;
//	Mat areaImage;
//	findContours(contourImage, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);	
//	drawContours(areaImage, contours, -1, Scalar::all(255), CV_FILLED);



	// PCA implementation
	Mat  globalDesc;
	cout << "\n Computing PCA...";
	pcaComputation(contourImage,globalDesc);	

  	ofstream myfile;
  myfile.open ("example.txt");
  myfile << globalDesc;
  myfile.close();

	// Extract the local r-� descriptor for the SVM
	Mat localDesc;
	Mat sv;
	Mat oppositePoints, contourPoints;
	cout << "\n Descriptor extracting...";
	localDescriptorExtraction(contourImage,localDesc,sv,contourPoints,oppositePoints);

		
	// Test the SVM and extract the final thumb point
	cout << "\n SVM training...";	
	svmTrain(sv , localDesc, contourImage,contourPoints,oppositePoints);

	// Find the 3d position of the grasp points
	Mat _3dCoords;
	cout <<"\n 3d Coordinate extracting...";
	get3dCoordinates(contourPoints,_3dCoords);
	cout << _3dCoords;

	
	
	
	// Choose a point
		int si;
	 si = rand() % _3dCoords.rows;
//	si = _3dCoords.rows;
	double theta;

	cout << si << contourPoints.rows;

 
	Point2d p;
	p.x = contourPoints.at<float>(si,0);
	p.y = contourPoints.at<float>(si,1);
	Point2d q;
	q.x = oppositePoints.at<float>(si,0);
	q.y = oppositePoints.at<float>(si,1);

	cout << p <<q;
				cv::	circle(contourImage,p,3,Scalar(255,0,0),-1,8,0);
				
				cv::	circle(contourImage,q,3,Scalar(255,0,0),-1,8,0);

			cv::	line(contourImage, p,q,Scalar(255,120,0),1,8,0);
				namedWindow("image");
				imshow("image",contourImage);
	imwrite("grasppoint.jpg",contourImage);	
				waitKey(0);

	theta = atan( ( -oppositePoints.at<float>(si,1) + contourPoints.at<float>(si,1))/ ( +oppositePoints.at<float>(si,0) - contourPoints.at<float>(si,0)))	;
	// Move the right hand to the desired thumb point (translation and orientation)
	ALValue q1 = "RShoulderPitch";
	ALValue q2 = "RShoulderRoll";
	ALValue q3 = "RElbowRoll";
	ALValue q4 = "RElbowYaw";
	ALValue q5 = "RWristYaw";
	ALValue qe = "RHand";

	float l1 = 90;
	float l2 = 50.55;
	float l3 = 58;
	//float handOffset = 
	float x,y,z;

	int space = 0;
	int axisMask = 7;
	bool isAbsolute = true;

	string effector = "RArm";
	
	x = _3dCoords.at<float>(si,0)*0.001;
	if (x>=0.2)
		x = 0.195;
		y = _3dCoords.at<float>(si,1)*0.001;
if (y>=0.02)
		//y = 0.0;
	

	z = 0.065;

	cout << "\n" <<x;
	cout << "\n" <<y;
	ALValue path = AL::ALValue::array(x, y, z, 0.0f, 0.0f, 0.0f);
		ALValue path2 = AL::ALValue::array(0,0,0, 0.0f, 0.0f, theta);

	ALValue path1 = AL::ALValue::array(0.11,-0.11,0.17, 0.0f, 0.0f, 0.0f);

	

		motionProxy.stiffnessInterpolation("RArm", 1.0f, 1.0f);
			motionProxy.positionInterpolation(effector,space,path1,7,3.0f,isAbsolute);
		motionProxy.openHand(qe);	
	motionProxy.positionInterpolation(effector,space,path,axisMask,3.0f,isAbsolute);
//motionProxy.positionInterpolation(effector,space,path2,axisMask,3.0f,false);	
	//motionProxy.closeHand(qe);

	//motionProxy.positionInterpolation(effector,space,path1,axisMask,4.0f,isAbsolute);
	//motionProxy.stiffnessInterpolation("RArm", 0.0f, 2.0f);


	
	
  }
  catch (const AL::ALError& e)
  {
    cerr << "Caught exception " << e.what() << endl;
  }

      	return 0;
  
}
