

#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <alproxies/alvideodeviceproxy.h>
#include <alproxies/almotionproxy.h>
#include <alvision/alimage.h>
#include <alvision/alvisiondefinitions.h>
#include <alerror/alerror.h>

#include <string>
#include <vector>

using namespace std;
using namespace cv;
using namespace AL;


// Image rectification and 3D image production (NOT NEEDED, DIDNT WORK)
// Input: 2 RGB images, Rotation betweeen the cameras, translation between the cameras
// Output: 3D image
//
void rectifyImages(Mat image1RGB, Mat image2RGB,Mat R,Mat T,Mat& _3DImage)
{

	// Camera matrix, rotation matrix, translation matrix and distortion coefficients from calibration
	double a = 180/CV_PI;
	Mat cameraMatrix = (Mat_<double>(3, 3) << 755.8969, 0, 319.5, 0, 758.8535, 239.5, 0,0,1);
//	Mat R = (Mat_<double>(3, 3) << cos(5*a),0,-sin(5*a),0,1,0,-sin(5*a),0,cos(5*a));
//	Mat T = (Mat_<double>(3, 1) << 53.9*sin(5*a),0,53.9-53.9*cos(5*a));
	Mat distCoeffs = (Mat_<double>(5, 1) << 0.3862,-2.1405,0.001909,0.00310899,3.25182);

	// Image converting
	cvtColor(image1RGB, image1RGB, CV_RGB2GRAY);
	cvtColor(image2RGB, image2RGB, CV_RGB2GRAY);

	// Declares
	Size imageSize = image1RGB.size();
	Mat R1;
	Mat R2;
	Mat P1;
	Mat P2;
	Mat Q;
	Mat undistMap1, undistMap2, rectMap1, rectMap2;
	Mat T1;
	Mat T2;
	Mat C1,C2,rot1,rot2,tra1,tra2;
	Mat undistImage1,undistImage2,rectImage1,rectImage2;
	Mat disp,disp8;
	Mat _3Dimage;
	StereoSGBM sgbm;

	
	// Optimize the camera matrix
	cameraMatrix = getOptimalNewCameraMatrix(cameraMatrix,distCoeffs, imageSize, -1,imageSize);


	// Compute the new projection matrices (rectified)
	stereoRectify(cameraMatrix,distCoeffs,cameraMatrix,distCoeffs,imageSize,R,T,R1,R2,P1,P2,Q,CALIB_ZERO_DISPARITY,0,imageSize);


	// Decomposing the new projection matrices
	decomposeProjectionMatrix(P1,T1,rot1,tra1);
	decomposeProjectionMatrix(P2,T2,rot2,tra2);


	// Compute the rectification and undistortion maps
	initUndistortRectifyMap(cameraMatrix,distCoeffs,rot1,cameraMatrix, imageSize, CV_32FC1, undistMap1, rectMap1);
	initUndistortRectifyMap(cameraMatrix,distCoeffs,rot2,cameraMatrix, imageSize, CV_32FC1, undistMap2, rectMap2);


	// Undistort the initial images
	undistort(image1RGB,undistImage1,cameraMatrix,distCoeffs);
	undistort(image2RGB,undistImage2,cameraMatrix,distCoeffs);

	// Apply the rectification map to the undistorted images
	remap(undistImage1,rectImage1,undistMap1,rectMap1,INTER_LINEAR);
	remap(undistImage2,rectImage2,undistMap2,rectMap2,INTER_LINEAR);


//undistImage1.copyTo(rectImage1);
//undistImage2.copyTo(rectImage2);

	// Stereo matchin parameters' initialization
	sgbm.preFilterCap = 6;
	sgbm.SADWindowSize = 5;
	sgbm.numberOfDisparities = 32;
	sgbm.P1 = 600;
	sgbm.P2 = 1800;
//	sgbm.preFilterSize = 155;
//	sgbm.textureThreshold = 414;
	sgbm.uniquenessRatio = 0;
	sgbm.speckleWindowSize = 0;
	sgbm.speckleRange = 0;
	sgbm.disp12MaxDiff = -1;
	sgbm.minDisparity = 0;

	// Stereo matching
     	sgbm(rectImage2, rectImage1,disp);
	normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);
	
	// 3D image computing
	Mat _3DImage1;
	reprojectImageTo3D(disp8,_3DImage1,Q,true);
	_3DImage = _3DImage1.clone();
	 


    	// Displaying the disparity and depth images
	namedWindow("3d");	
	namedWindow("disp");	
	imshow("disp", disp8);
	imshow("3d", _3DImage);


	// Display the initial images
	namedWindow("Image 1");
	imshow("Image 1",image1RGB);
	namedWindow("Image 2");
	imshow("Image 2",image2RGB);

	// Display the undistorted images
	namedWindow("Undistorted Image 1");
	imshow("Undistorted Image 1",undistImage1);
	namedWindow("Undistorted Image 2");
	imshow("Undistorted Image 2",undistImage2);

	// Display the rectified images
	namedWindow("Rectified Image 1");
	imshow("Rectified Image 1",rectImage1);
	namedWindow("Rectified Image 2");
	imshow("Rectified Image 2",rectImage2);



}


// Get a picture from the robot camera
// Input : Camera proxy
// Output: Image

void getPicture(ALVideoDeviceProxy camProxy, Mat& imgHeader1)
{
	// Subscribe a client image requiring 320*240 and BGR colorspace.
	const string clientName = camProxy.subscribe("test", kQVGA, kBGRColorSpace, 30);

	// Create an cv::Mat header to wrap into an opencv image.
	Mat imgHeader = Mat(Size(320, 240), CV_8UC3);

	  
	/* Retrieve an image from the camera.
	* The image is returned in the form of a container object, with the
	* following fields:
	* 0 = width
	* 1 = height
	* 2 = number of layers
	* 3 = colors space index (see alvisiondefinitions.h)
	* 4 = time stamp (seconds)
	* 5 = time stamp (micro seconds)
	* 6 = image buffer (size of width * height * number of layers)
	*/	
	ALValue img1 = camProxy.getImageRemote(clientName);

	/** Access the image buffer (6th field) and assign it to the opencv image
	* container. */
	imgHeader.data = (uchar*) img1[6].GetBinary();

	/** Tells to ALVideoDevice that it can give back the image buffer to the
	* driver. Optional after a getImageRemote but MANDATORY after a getImageLocal.*/

	
	camProxy.releaseImage(clientName);
		
	// Cleanup
	camProxy.unsubscribe(clientName);

	imgHeader1 = imgHeader.clone();


}



// Image segmentation (background subtraction)
// Input: backgound, foreground
// Output: Contour of the object
//
void segmentImage(Mat background, Mat foreground, Mat& contourImage)
{	// Declarations
	Mat dst,dst1;
	int i,j;

	// Background and foreground rgb to gray
	
	cvtColor(background, background, CV_RGB2GRAY);
	cvtColor(foreground, foreground, CV_RGB2GRAY);
	


	// Background and foreground median filtering
	int ksize=5;
	medianBlur(background,background,ksize);
	medianBlur(foreground,foreground,ksize);	

	// Absolute difference
	absdiff(foreground,background,dst);
//	GaussianBlur(foreground,dst,Size(3,3),2,0,BORDER_DEFAULT);
	// Thresholding
	double value,thresh;
	thresh = 40;
	value = threshold(foreground,dst1,thresh,255,THRESH_OTSU);
//	adaptiveThreshold(dst,dst1,255,ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY,19,0);
	Mat element;
       //element = getStructuringElement(MORPH_ELLIPSE, Size(5,5), Point(-1,-1));
      // morphologyEx(dst1, dst1, MORPH_CLOSE, element, Point(-1,-1), 1, BORDER_CONSTANT, morphologyDefaultBorderValue() );
      // morphologyEx(dst1, dst1, MORPH_OPEN, element, Point(-1,-1), 1, BORDER_CONSTANT, morphologyDefaultBorderValue() );
//morphologyEx(dst1, dst1, MORPH_OPEN, element, Point(-1,-1), 3, BORDER_CONSTANT, morphologyDefaultBorderValue() );
//	 erode(dst1,dst1, element, Point(-1,-1), 1, BORDER_CONSTANT, morphologyDefaultBorderValue() );

//	 cout << "erode";
	// Contour finding
	vector<vector<Point> > contours,contours1;
	Mat cont = Mat::zeros(background.size(), background.type());
	Mat cont1 = Mat::zeros(background.size(), background.type());
	findContours(dst1.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	drawContours(cont, contours, -1, Scalar::all(255), 1);
	findContours(cont.clone(), contours1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	drawContours(cont1, contours1, -1, Scalar::all(255), 1);

	namedWindow("Cont1");
		imshow("Cont1",cont1);
		waitKey(0);

	int maxSize = contours1[0].size();
	int maxPos = 0;
	for (i = 0; i<contours1.size(); i++)
		if ( contours1[i].size() > maxSize )
		{
			maxSize = contours1[0].size();
			maxPos = i;
		}
	
	vector<Point> largeCont;
	largeCont =  contours1[maxPos];

cout << "came here";
	
cout << "came there";


	Mat newCont =  Mat::zeros(cont.size(), cont.type());

	int a,b;
	for (i = 0; i<largeCont.size(); i++)
	{
		a = largeCont[i].x ;
		b = largeCont[i].y ;
		newCont.at<uchar>(b,a) = 255;
	}
		
//	cout <<  largeCont;
	// Show the results
	
	
	namedWindow("Background");
	namedWindow("Foreground");
	namedWindow("Abs");
	namedWindow("Segmented");
	namedWindow("Contour");

	imshow("Background",background);
	imshow("Foreground",foreground);
	imshow("Abs",dst);
	imshow("Segmented",dst1);
	imshow("Contour",newCont);
	waitKey(0);
	imwrite("contour.jpg",newCont);

	contourImage = cont.clone();


}



// Backprojection of a ray from the camera center to intersect a point in a plane
// Input: Image contour
// Output: 3Dcoordinates of the point
// Redefinition of the image plane points and the 3Dworld plane points is needed according to your problem. Also different cameraMatrix
//
void get3dCoordinates(Mat contourPoints, Mat& _3dCoords)
{
	int i;

	// Points for solving the PnP problem
	vector<Point2f> chessImage;
	chessImage.push_back(Point2f(108,29));
	chessImage.push_back(Point2f(238,38));
	chessImage.push_back(Point2f(250,77));
	chessImage.push_back(Point2f(94,78));

	vector<Point3f> chessWorld;
	chessWorld.push_back(Point3f(0,0,0));
	chessWorld.push_back(Point3f(0,6*17.5,0));
	chessWorld.push_back(Point3f(6*17.5,6*17.5,0));
	chessWorld.push_back(Point3f(6*17.5,0,0));	


	// Declarations of needed matrices
	Mat cameraMatrix = (Mat_<float>(3, 3) << 378.64049, 0, 159.5, 0, 380.07872, 119.5, 0,0,1);
	Mat distCoeffs = (Mat_<float>(5, 1) << 0.3786347,-1.9043234,0.00216862,0.00265294,1.899664);
	Mat rvec= Mat(3,1,CV_32F);
	Mat rmat= Mat(3,3,CV_32F);
	Mat tvec = Mat(3,1,CV_32F);

	// Solve the PnP problem and get the translation vector and rotation matrix
	solvePnP(chessWorld, chessImage, cameraMatrix, distCoeffs, rvec, tvec );
	Rodrigues(rvec, rmat);
	rmat.convertTo(rmat,CV_32F);
	tvec.convertTo(tvec,CV_32F);

	cout << "\n" <<tvec;

	
	// Backproject the chosen image points
	Mat uvpoint;
	Mat temp3d = Mat(3,contourPoints.rows,CV_32F);

	
	for (i=0; i<contourPoints.rows; i++)
	{
		Mat uvPoint = Mat(3,1,CV_32F);
		uvPoint.at<float>(0,0) = contourPoints.at<float>(i,0);
		uvPoint.at<float>(1,0) = contourPoints.at<float>(i,1);
		uvPoint.at<float>(2,0) = 1.0f;


		Mat tempMat,tempMat2;
		float s;

		tempMat = rmat.inv()*cameraMatrix.inv()*uvPoint;	
		tempMat2 = rmat.inv()*tvec;
		s = tempMat2.at<float>(2,0);	
		s /= tempMat.at<float>(2,0);
		temp3d.col(i) =  rmat.inv() * (s * cameraMatrix.inv() * uvPoint - tvec);
	//	temp3d.col(i) = rmat.inv()*(temp3d.col(i)-tvec*0.001);
	}		


	_3dCoords = temp3d.t();


}
