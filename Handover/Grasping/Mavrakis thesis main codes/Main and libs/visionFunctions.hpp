

#ifndef VISION_FUNCTIONS_HPP
#define VISION_FUNCTIONS_HPP
 
 
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace AL;
using namespace cv;


void rectifyImages(Mat left, Mat right, Mat R, Mat T,Mat& _3DImage);  	// Image rectification function, (Left and right images, known R,T)

void getPicture(ALVideoDeviceProxy camProxy, Mat& imgHeader); 	// Function for taking a picture with a proxy and returning the corresponding Mat 

void segmentImage(Mat background,Mat foreground, Mat& contourImage);		// Function for segmenting an image to get the different objects of interest, using backgound subtraction

void get3dCoordinates(Mat contourPoints, Mat& _3dCoords);// Function for computing the 3d coordinates of a point in plane Z=0
 
#endif
