

// Camera calibration:
// Input: -
// Output: cameraMatrix, distortion coefficients
// Needs 20 checkerboard images (imageX.jpg) to perform calibration of the camera (Zhang)
// Change the chessboard size and the 3D chessboard points

#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <vector>



using namespace cv;
using namespace std;

int main()

{
	bool patternFound;	// Variable for checking if the chessboard was found in the image

	Mat image1,image2,image3,image4;	// Image buffers

	Size patternSize(7,5);	// Size of the inner chessboard pattern                           <-------------------  CHANGE

	vector<Point2f> corners;	// Corner vectors


	vector<Point3f> chessPoints;
	Point3f p(0.f,0.f,0.f);

	vector<vector<Point3f>>	chessVector;

	vector<vector<Point2f>> imageVector;

	vector<Mat> image,cimage;

	int i;

	Mat cameraMatrix(Mat::eye(3, 3, CV_64F));
	Mat distCoeffs(Mat::zeros(8, 1, CV_64F));

	// Find calibration patterns (chessboards)
	cimage.push_back(imread("image1.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image2.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image3.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image4.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image5.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image6.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image7.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image8.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image9.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image10.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image11.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image12.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image13.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image14.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image15.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image16.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image17.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image18.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image19.jpg",CV_LOAD_IMAGE_COLOR));
	cimage.push_back(imread("image20.jpg",CV_LOAD_IMAGE_COLOR));


	image.push_back(imread("image1.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image2.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image3.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image4.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image5.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image6.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image7.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image8.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image9.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image10.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image11.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image12.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image13.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image14.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image15.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image16.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image17.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image18.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image19.jpg",CV_LOAD_IMAGE_GRAYSCALE));
	image.push_back(imread("image20.jpg",CV_LOAD_IMAGE_GRAYSCALE));

	for (i=0;i<20;i++)
	{
		patternFound = findChessboardCorners(image[i],patternSize,corners,CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE + CALIB_CB_FAST_CHECK); // Find corners

		if(!patternFound)
		{
			cout << "Didnt find pattern no "<< (i+1) <<",exiting..\n";
			return 0;
		}

		cornerSubPix(image[i], corners, Size(11,11), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));		// Refine corner positions

		drawChessboardCorners(cimage[i], patternSize, Mat(corners), patternFound);	// Draw the refined corners on the image


		// 2D image point vector for the calibrateCamera()	
		imageVector.push_back(corners);

	}


	


	// Create OpenCV windows to display the images
	 namedWindow("Image No 1");
	 namedWindow("Image No 2");
	 namedWindow("Image No 3");
	 namedWindow("Image No 4");
	 namedWindow("Image No 5");
	 namedWindow("Image No 6");
	 namedWindow("Image No 7");
	 namedWindow("Image No 8");
	 namedWindow("Image No 9");
	 namedWindow("Image No 10");
	 namedWindow("Image No 11");
	 namedWindow("Image No 12");
	 namedWindow("Image No 13");
	 namedWindow("Image No 14");
	 namedWindow("Image No 15");
	 namedWindow("Image No 16");
	 namedWindow("Image No 17");
	 namedWindow("Image No 18");
	 namedWindow("Image No 19");
	 namedWindow("Image No 20");
	
  
	  while ((char) cv::waitKey(30) != 27)
	  {
	   	//Display the images on screen (press Esc to exit)
	    	imshow("Image No 1", cimage[0]);
	    	imshow("Image No 2", cimage[1]);
	    	imshow("Image No 3", cimage[2]);		
	    	imshow("Image No 4", cimage[3]);
		imshow("Image No 5", cimage[4]);
	    	imshow("Image No 6", cimage[5]);
	    	imshow("Image No 7", cimage[6]);		
	    	imshow("Image No 8", cimage[7]);
		imshow("Image No 9", cimage[8]);
	    	imshow("Image No 10", cimage[9]);
		imshow("Image No 11", cimage[10]);
	    	imshow("Image No 12", cimage[11]);
	    	imshow("Image No 13", cimage[12]);		
	    	imshow("Image No 14", cimage[13]);
		imshow("Image No 15", cimage[14]);
	    	imshow("Image No 16", cimage[15]);
	    	imshow("Image No 17", cimage[16]);		
	    	imshow("Image No 18", cimage[17]);
		imshow("Image No 19", cimage[18]);
	    	imshow("Image No 20", cimage[19]);
	  }




	


	// Useful constants  
	int yStep = 4;
	int xStep = 1;


 	
	// 3D chessboard point creation.	NEEDS TO BE CHANGED ACCORDING TO YOUR CHESSBOARD PLANE
	for(i=0; i<35; i++)
	{
		if ((i==7) || (i==14) || (i==21) || (i==28))
		{
			yStep--;
			xStep = 1;
		}
		p.x = xStep*3.f;

		p.y = yStep*3.f+2.5f;
		p.z = 0.f;	
		chessPoints.push_back(p);
		xStep++;
	}

	// 3D chessboard point vector for the calibrateCamera()
	for (i=0;i<20;i++)
	{
		chessVector.push_back(chessPoints);
	}


	Size imageSize(image[1].size());

	double out;
	vector<Mat> rvecs, tvecs;
	out = calibrateCamera(chessVector,imageVector,imageSize,cameraMatrix,distCoeffs,rvecs,tvecs,CV_CALIB_FIX_PRINCIPAL_POINT);


	cout << "cameraMatrix = "<< endl << " "  << cameraMatrix << endl << endl;
	cout << "\n";
	cout << "distCoeffs = "<< endl << " "  << distCoeffs << endl << endl;


	


}
