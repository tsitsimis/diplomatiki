close all;

% read image
I = imread('contour.jpg');  % binary image
I = rgb2gray(I);
I = im2double(I);
[rows, cols] = size(I);

pos = find(I == 1);
cont = zeros(length(pos), 2);
for i = 1:length(pos)
    cont(i, :) = pos2point(pos(i), rows);
end
s = cont(:, 1) + 1j * cont(:, 2);
a = fft(s);
s_hat = ifft(a, 560);

smoothed = [real(s_hat), imag(s_hat)];

% show results
figure;
imshow(I);
hold on;
plot(smoothed(:, 1), smoothed(:, 2), 'r.');