function point = pos2point(pos, rows)
    pos = pos - 1;
    i = 1 + floor(pos / rows);
    j = 1 + mod(pos, rows);
    point = [i, j];
end