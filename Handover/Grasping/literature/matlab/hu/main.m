close all;

% read image
I1 = imread('tennis_ball.jpg');
I1 = rgb2gray(I1);
I1 = im2double(I1);

% transform image
phi = 30;
I2 = imrotate(I1, phi, 'crop');

% image moments
m11_1 = immoment(I1, 1, 1);
m11_2 = immoment(I2, 1, 1);
m20_1 = immoment(I1, 2, 0);
m02_1 = immoment(I1, 0, 2);
m20_2 = immoment(I2, 2, 0);
m02_2 = immoment(I2, 0, 2);

% Hu invariants
f1_1 = m20_1 + m02_1;
f1_2 = m20_2 + m02_2;

f2_1 = (m20_1 - m02_1)^2 + 4*m11_1^2;
f2_2 = (m20_2 - m02_2)^2 + 4*m11_2^2;

figure;
imshow(I1);

figure;
imshow(I2);