close all;

% read image
I = imread('circle.jpg');  % binary image
I = rgb2gray(I);
I = im2double(I);

% image moments
m00 = immoment(I, 0, 0);
m10 = immoment(I, 1, 0);
m01 = immoment(I, 0, 1);

% image centroid
ic = m10 / m00;
jc = m01 / m00;

% show results
figure;
imshow(I);
hold on;
plot(ic, jc, 'o')