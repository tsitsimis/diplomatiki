function mom = immoment(im, p, q)
    [rows, cols] = size(im);
    [X, Y] = meshgrid(1:cols, 1:rows);
    X = X(:);
    Y = Y(:);
    mom = sum(X.^p .* Y.^q .* im(:));
end