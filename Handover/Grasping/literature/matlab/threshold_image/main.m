close all;

I = imread('peppers.jpg');
I(:, :, [1,3]) = 0;
I = rgb2gray(I);
I = im2double(I);

data = I(:);
dataLen = length(data);
[N, edges] = histcounts(data, 0:0.01:1);

t1 = 0.28;
t2 = 0.3;
ind1 = find(edges == t1);
ind2 = find(edges == t2);
index = ind1:ind2;
thres = (I > t1) & (I < t2);

figure;
subplot(2, 2, 1:2);
hold on;
plot(edges(1:end-1), N);
h1 = fill([edges(ind1), edges(index), edges(ind2)], [0, N(index), 0], 'b', 'EdgeColor', 'none');

subplot(2, 2, 3);
imshow(I);

subplot(2, 2, 4);
imshow(thres);