close all;

% read image
img = imread('green_red.png');

% transform to Lab space
cform = makecform('srgb2lab');
lab_img = applycform(img, cform);

% take a and b values
ab = double(lab_img(:, :, 2:3));
nrows = size(ab, 1);
ncols = size(ab, 2);
ab = reshape(ab, nrows*ncols, 2);

% histogram
[counts, centers] = hist(ab(:, 1), 50);
figure;
plot(centers, counts, '-o');

[counts, centers] = hist(ab(:, 2), 50);
figure;
plot(centers, counts, '-o');



