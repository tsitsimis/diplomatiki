close all;

% read image
img = imread('frame.jpg');

% transform to Lab space
cform = makecform('srgb2lab');
lab_img = applycform(img, cform);

% take a and b values
ab = double(lab_img(:, :, 2:3));
nrows = size(ab, 1);
ncols = size(ab, 2);
ab = reshape(ab, nrows*ncols, 2);

% k-means
nColors = 3;
[cluster_idx, cluster_center] = kmeans(ab, nColors, 'distance', 'sqEuclidean', 'Replicates', 3);
pixel_labels = reshape(cluster_idx, nrows, ncols);

% detect red in RGB
red_img = (img(:, :, 1) > 100 & img(:, :, 2) < 50 & img(:, :, 3) < 50);

% mask with k-means regions


% plot results
figure;
imshow(pixel_labels, []);

figure;
imshow(red_img, []);

