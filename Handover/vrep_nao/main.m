close all;
addpath(genpath('vrep_api'));

vrep = remApi('remoteApi');
vrep.simxFinish(-1);
clientID = vrep.simxStart('127.0.0.1', 19999, true, true, 5000, 5);

% operation modes
osw = vrep.simx_opmode_oneshot_wait;
str = vrep.simx_opmode_streaming;
buf = vrep.simx_opmode_buffer;

if (clientID > -1)
    disp('Connected to remote API server');
    
    % object handles
    [~, nao] = vrep.simxGetObjectHandle(clientID, 'NAO', osw);
    [~, LShoulderPitch] = vrep.simxGetObjectHandle(clientID, 'LShoulderPitch', osw);
    [~, LShoulderRoll] = vrep.simxGetObjectHandle(clientID, 'LShoulderRoll', osw);
    [~, LElbowYaw] = vrep.simxGetObjectHandle(clientID, 'LElbowYaw', osw);
    [~, LWristYaw] = vrep.simxGetObjectHandle(clientID, 'LWristYaw', osw);
    [~, vSensor] = vrep.simxGetObjectHandle(clientID, 'NAO_vision2', osw);
    
    % read vision sensor
    cform = makecform('srgb2lab');
    
    figure;
    gray_code = 0;
    bytesPerPixel = 3;
    [code, res, img] = vrep.simxGetVisionSensorImage2(clientID, vSensor, gray_code, str);
    while 1
        [code, res, img] = vrep.simxGetVisionSensorImage2(clientID, vSensor, gray_code, buf);
        if code == vrep.simx_return_ok
            %% simple red color detection
            bin_img = (img(:, :, 1) > 100 & img(:, :, 2) < 50 & img(:, :, 3) < 50);
            bin_img = uint8(bin_img);
            
            cube = img .* repmat(bin_img, [1, 1, 3]);
            
            %% edge detection
            cube_edge = edge(cube(:, :, 1)) | edge(cube(:, :, 2)) | edge(cube(:, :, 3));
            
            %% image moments
            stats = regionprops(bin_img, 'centroid');
            centroids = cat(1, stats.Centroid);
            
            %% show results
            h1 = imshow(img, []);
            
            yellow = zeros(res(1), res(2), 3);
            yellow(:, :, 1:2) = 1;
            
            hold on;
            h2 = imshow(yellow);
            set(h2, 'AlphaData', cube_edge);
            
            plot(centroids(:, 1), centroids(:, 2), 'b*');
            
            pause(1);
        end
    end
    
    
    
else
    disp('Connection failed');
end

