from myfunctions import *


# children
# aggelos_data_path = "/media/theodore/My Passport/Tzafesta/Aggelos/2017_06_22-13_02_47"
# aggelos_start = '1498125879399'
# aggelos_stop = '1498125907698'

# ilektra_data_path = "/media/theodore/My Passport/Tzafesta/Ilektra/2017_06_22-11_42_29"
# ilektra_start = '1498121076432'
# ilektra_stop = '1498121115065'

aggelos_data_path = "/media/theodore/My Passport/Tzafesta/Ilektra/2017_06_22-11_42_29"
aggelos_start = '1498121076432'
aggelos_stop = '1498121115065'


data_path = aggelos_data_path
color_dir = "%s/Color" % data_path
color_file = "%s/Color/colorData.csv" % data_path
face_file = "%s/Face/face.csv" % data_path
skel_file = "%s/Skeleton/skeleton.csv" % data_path

# csv to numpy
color = np.loadtxt(color_file, delimiter=';', dtype=np.str)
face = np.loadtxt(face_file, delimiter=';', dtype=np.str)
skeleton = np.loadtxt(skel_file, delimiter=';', dtype=np.str)

# skeleton = skeleton[skeleton[:, 4] == "Tracked"]  # keep only tracked skeletons

ind_start_color = np.where(color[:, 2] == aggelos_start)[0][0]
ind_start_skel = np.where(skeleton[:, 2] == aggelos_start)[0][0]
ind_stop_color = np.where(color[:, 2] == aggelos_stop)[0][0]
ind_stop_skel = np.where(skeleton[:, 2] == aggelos_stop)[0][0]

color = color[ind_start_color:ind_stop_color, :]
skeleton = skeleton[ind_start_skel:ind_stop_skel, :]     # skeleton size is 6 x color size

# delete frames (rows) where the color-skeleton unix time is not the same
ind = np.where(color[:, 2] != np.unique(skeleton[:, 2]))
color2 = color
color2 = np.delete(color2, ind, axis=0)

ind_unix = np.in1d(skeleton[:, 2], color[ind, 2]).nonzero()[0]
skeleton2 = skeleton
skeleton2 = np.delete(skeleton2, ind_unix, axis=0)

# face2 = np.zeros(np.shape(skeleton2)[0], np.shape(face)[1])
face2 = face[ind_start_color:ind_stop_color, :]
face2 = np.delete(face2, ind, axis=0)

for i in range(0, len(color2) - 1):
        img = cv2.imread(color_dir + "/" + color2[i, 1] + "_" + color2[i, 0] + ".jpg")

        skel_idx = np.argwhere(
            skeleton2 == color2[i, 2])  # find the skeleton rows with the same unix timestamp with the color

        face_idx = np.argwhere(
            face == skeleton2[skel_idx[0][0], 1])  # find the face rows with the same relative time with the skeleton

        # draw the skeletons
        if len(skel_idx) > 0:
            for idx in skel_idx:
                skel_row = skeleton2[idx[0]]
                joints_array = np.reshape(skel_row[skel_start:skel_stop], (25, 13))
                img = draw_body(img, joints_array)

        # draw the faces
        if len(face_idx) > 0:
            for idx in face_idx:
                face_row = face[idx[0]]
                top_left = (int(int(face_row[bounding_left_color])), int(face_row[bounding_top_color]))
                bottom_right = (int(int(face_row[bounding_right_color])), int(face_row[bounding_bottom_color]))
                cv2.rectangle(img, (top_left), (bottom_right), (0, 255, 0), 3)

                cv2.circle(img, (int(round(float(face_row[mouth_left_color]))), int(round(float(face_row[mouth_left_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[mouth_right_color]))), int(round(float(face_row[mouth_right_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[nose_color]))), int(round(float(face_row[nose_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[eye_right_color]))), int(round(float(face_row[eye_right_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[eye_left_color]))), int(round(float(face_row[eye_left_color + 1])))),
                           5, (255, 0, 0), -5)

        img = cv2.resize(img, (800, 400))
        cv2.imshow("Faces found", img)
        cv2.waitKey(1)


