import numpy as np
import cv2
import os
import sys

# indices of color face bounding box
bounding_top_color = 5 + 2
bounding_left_color = 4 + 2
bounding_right_color = 6 + 2
bounding_bottom_color = 3 + 2

# indices of landmarks
eye_left_color = 28 + 2
eye_right_color = 31 + 2
nose_color = 34 + 2
mouth_left_color = 37 + 2
mouth_right_color = 40 + 2

# indices of skeleton
skel_start = 3
skel_stop = 13 * 25 + skel_start
in_joint_color = 7
in_joint_tracking = 1

# joint sequence
AnkleLeft = 14
AnkleRight = 18
ElbowLeft = 5
ElbowRight = 9
FootLeft = 15
FootRight = 19
HandLeft = 7
HandRight = 11
HandTipLeft = 21
HandTipRight = 23
Head = 3
HipLeft = 12
HipRight = 16
KneeLeft = 13
KneeRight = 17
Neck = 2
ShoulderLeft = 4
ShoulderRight = 8
SpineBase = 0
SpineMid = 1
SpineShoulder = 20
ThumbLeft = 22
ThumbRight = 24
WristLeft = 6
WristRight = 10


# draws a bone
def draw_bone(joints_array, img, joint_1, joint_2):
    # if both are tracked draw them both and a line
    if joints_array[joint_1, in_joint_tracking] == "Tracked" and joints_array[joint_2, in_joint_tracking] == "Tracked":
        point1 = (int(round(float(joints_array[joint_1, in_joint_color]))),
                  int(round(float(joints_array[joint_1, in_joint_color + 1]))))
        point2 = (int(round(float(joints_array[joint_2, in_joint_color]))),
                  int(round(float(joints_array[joint_2, in_joint_color + 1]))))
        cv2.circle(img, point1, 10, (0, 0, 0), -5)
        cv2.circle(img, point2, 10, (0, 0, 0), -5)
        cv2.line(img, point1, point2, (0, 0, 0), 10)
    return img


# draws a body
def draw_body(img, joints_array):
    # Torso
    img = draw_bone(joints_array, img, Head, Neck)
    img = draw_bone(joints_array, img, Neck, SpineShoulder)
    img = draw_bone(joints_array, img, SpineShoulder, SpineMid)
    img = draw_bone(joints_array, img, SpineMid, SpineBase)
    img = draw_bone(joints_array, img, SpineShoulder, ShoulderRight)
    img = draw_bone(joints_array, img, SpineShoulder, ShoulderLeft)
    img = draw_bone(joints_array, img, SpineBase, HipRight)
    img = draw_bone(joints_array, img, SpineBase, HipLeft)

    # Right Arm
    img = draw_bone(joints_array, img, ShoulderRight, ElbowRight)
    img = draw_bone(joints_array, img, ElbowRight, WristRight)
    img = draw_bone(joints_array, img, WristRight, HandRight)
    img = draw_bone(joints_array, img, HandRight, HandTipRight)
    img = draw_bone(joints_array, img, WristRight, ThumbRight)

    # Left Arm
    img = draw_bone(joints_array, img, ShoulderLeft, ElbowLeft)
    img = draw_bone(joints_array, img, ElbowLeft, WristLeft)
    img = draw_bone(joints_array, img, WristLeft, HandLeft)
    img = draw_bone(joints_array, img, HandLeft, HandTipLeft)
    img = draw_bone(joints_array, img, WristLeft, ThumbLeft)

    # Right Leg
    img = draw_bone(joints_array, img, HipRight, KneeRight)
    img = draw_bone(joints_array, img, KneeRight, AnkleRight)
    img = draw_bone(joints_array, img, AnkleRight, FootRight)

    # Left Leg
    img = draw_bone(joints_array, img, HipLeft, KneeLeft)
    img = draw_bone(joints_array, img, KneeLeft, AnkleLeft)
    img = draw_bone(joints_array, img, AnkleLeft, FootLeft)

    return img


def cmp_by_nano(a, b):
    a = int(a.split("_")[0])
    b = int(b.split("_")[0])
    if a > b:
        return 1
    elif a == b:
        return 0
    else:
        return -1


def get_text(x):
    x = int(x)
    return {
        3: 'Yes',
        2: 'Maybe',
        1: 'No',
        0: 'Unknown'
    }[x]


def main(DIR):
    # read face color and skeleton
    face_file = "%s/Face/face.csv" % DIR
    color_file = "%s/Color/colorData.csv" % DIR
    skel_file = "%s/Skeleton/skeleton.csv" % DIR

    color_dir = "%s/Color" % DIR

    face = np.loadtxt(face_file, delimiter=';', dtype=np.str)
    skel = np.loadtxt(skel_file, delimiter=';', dtype=np.str)
    color_list = np.loadtxt(color_file, delimiter=';', dtype=np.str)

    print "%d frames found" % len(color_list)

    for i in range(0, len(color_list) - 1):
        img = cv2.imread(color_dir + "/" + color_list[i, 1] + "_" + color_list[i, 0] + ".jpg")

        skel_idx = np.argwhere(
            skel == color_list[i, 2])  # find the skeleton rows with the same unix timestamp with the color
        # print skel_idx
        # print skel_idx[0]
        # print skel_idx[0][0]

        face_idx = np.argwhere(
            face == skel[skel_idx[0][0], 1])  # find the face rows with the same relative time with the skeleton

        # draw the skeletons
        if len(skel_idx) > 0:
            for idx in skel_idx:
                skel_row = skel[idx[0]]
                joints_array = np.reshape(skel_row[skel_start:skel_stop], (25, 13))
                img = draw_body(img, joints_array)

        # draw the faces
        if len(face_idx) > 0:
            for idx in face_idx:
                face_row = face[idx[0]]
                top_left = (int(int(face_row[bounding_left_color])), int(face_row[bounding_top_color]))
                bottom_right = (int(int(face_row[bounding_right_color])), int(face_row[bounding_bottom_color]))
                cv2.rectangle(img, (top_left), (bottom_right), (0, 255, 0), 3)

                cv2.circle(img, (int(round(float(face_row[mouth_left_color]))), int(round(float(face_row[mouth_left_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[mouth_right_color]))), int(round(float(face_row[mouth_right_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[nose_color]))), int(round(float(face_row[nose_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[eye_right_color]))), int(round(float(face_row[eye_right_color + 1])))),
                           5, (255, 0, 0), -5)
                cv2.circle(img, (int(round(float(face_row[eye_left_color]))), int(round(float(face_row[eye_left_color + 1])))),
                           5, (255, 0, 0), -5)

        img = cv2.resize(img, (800, 400))
        cv2.imshow("Faces found", img)
        cv2.waitKey(1)


if __name__ == '__main__':
    parent_dir = sys.argv[1]
    main(parent_dir)
