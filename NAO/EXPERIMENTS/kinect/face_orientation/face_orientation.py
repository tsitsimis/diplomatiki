from myfunctions import *

# children
aggelos_data_path = "/media/theodore/My Passport/Tzafesta/Aggelos/2017_06_22-13_02_47"

data_path = aggelos_data_path
color_dir = "%s/Color" % data_path
color_file = "%s/Color/colorData.csv" % data_path
face_file = "%s/Face/face.csv" % data_path
skel_file = "%s/Skeleton/skeleton.csv" % data_path

color_list = np.loadtxt(color_file, delimiter=';', dtype=np.str)
face = np.loadtxt(face_file, delimiter=';', dtype=np.str)
skel = np.loadtxt(skel_file, delimiter=';', dtype=np.str)

n_frames = len(color_list) - 1

# head
head_angles = face[:, [yaw_index, pitch_index, roll_index]]
head_angles = head_angles.astype(np.float)

# torso
torso_angles = skel
torso_angles = torso_angles[2::6, :]

# torso_angles = torso_angles[torso_angles[:, 4] == 'Tracked']
# unixtime_col = torso_angles[:, 2]
# _, ind = np.unique(unixtime_col, return_index=True)
# torso_angles = torso_angles[ind, :]

q0 = torso_angles[:, spine_shoulder].astype(np.float)
q1 = torso_angles[:, spine_shoulder + 1].astype(np.float)
q2 = torso_angles[:, spine_shoulder + 2].astype(np.float)
q3 = torso_angles[:, spine_shoulder + 3].astype(np.float)

torso_yaw, torso_pitch, torso_roll = quat2euler(q0, q1, q2, q3)
# np.savetxt("skeleton_reduced2.csv", torso_angles, delimiter=';', fmt='%s')

# plot
plt.subplot(2, 1, 1)
plt.plot(head_angles[:, 0])

plt.subplot(2, 1, 2)
plt.plot(torso_yaw)
plt.show()



