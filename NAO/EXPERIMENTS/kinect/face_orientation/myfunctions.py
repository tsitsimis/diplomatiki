import numpy as np
import cv2
import os
import sys
import matplotlib.pyplot as plt
import math

# indices of color face bounding box
bounding_top_color = 5 + 2
bounding_left_color = 4 + 2
bounding_right_color = 6 + 2
bounding_bottom_color = 3 + 2

# indices of landmarks
eye_left_color = 28 + 2
eye_right_color = 31 + 2
nose_color = 34 + 2
mouth_left_color = 37 + 2
mouth_right_color = 40 + 2

# indices of face yaw, pitch, roll
yaw_index = 63
pitch_index = 64
roll_index = 65

# indices of skeleton
spine_shoulder = 272

# indices of skeleton
skel_start = 3
skel_stop = 13 * 25 + skel_start
in_joint_color = 7
in_joint_tracking = 1

# joint sequence
AnkleLeft = 14
AnkleRight = 18
ElbowLeft = 5
ElbowRight = 9
FootLeft = 15
FootRight = 19
HandLeft = 7
HandRight = 11
HandTipLeft = 21
HandTipRight = 23
Head = 3
HipLeft = 12
HipRight = 16
KneeLeft = 13
KneeRight = 17
Neck = 2
ShoulderLeft = 4
ShoulderRight = 8
SpineBase = 0
SpineMid = 1
SpineShoulder = 20
ThumbLeft = 22
ThumbRight = 24
WristLeft = 6
WristRight = 10


def draw_face(img, face_row):
    cv2.circle(img, (int(round(float(face_row[mouth_left_color]))),
                     int(round(float(face_row[mouth_left_color + 1])))), 5, (255, 0, 0), -5)
    cv2.circle(img, (int(round(float(face_row[mouth_right_color]))),
                     int(round(float(face_row[mouth_right_color + 1])))), 5, (255, 0, 0), -5)
    cv2.circle(img, (int(round(float(face_row[nose_color]))),
                     int(round(float(face_row[nose_color + 1])))), 5, (255, 0, 0), -5)
    cv2.circle(img, (int(round(float(face_row[eye_right_color]))),
                     int(round(float(face_row[eye_right_color + 1])))), 5, (255, 0, 0), -5)
    cv2.circle(img, (int(round(float(face_row[eye_left_color]))),
                     int(round(float(face_row[eye_left_color + 1])))), 5, (255, 0, 0), -5)


# draws a bone
def draw_bone(joints_array, img, joint_1, joint_2):
    # if both are tracked draw them both and a line
    if joints_array[joint_1, in_joint_tracking] == "Tracked" and joints_array[joint_2, in_joint_tracking] == "Tracked":
        point1 = (int(round(float(joints_array[joint_1, in_joint_color]))),
                  int(round(float(joints_array[joint_1, in_joint_color + 1]))))
        point2 = (int(round(float(joints_array[joint_2, in_joint_color]))),
                  int(round(float(joints_array[joint_2, in_joint_color + 1]))))
        cv2.circle(img, point1, 10, (0, 0, 0), -5)
        cv2.circle(img, point2, 10, (0, 0, 0), -5)
        cv2.line(img, point1, point2, (0, 0, 0), 10)
    return img


# draws a body
def draw_body(img, joints_array):
    # Torso
    img = draw_bone(joints_array, img, Head, Neck)
    img = draw_bone(joints_array, img, Neck, SpineShoulder)
    img = draw_bone(joints_array, img, SpineShoulder, SpineMid)
    img = draw_bone(joints_array, img, SpineMid, SpineBase)
    img = draw_bone(joints_array, img, SpineShoulder, ShoulderRight)
    img = draw_bone(joints_array, img, SpineShoulder, ShoulderLeft)
    img = draw_bone(joints_array, img, SpineBase, HipRight)
    img = draw_bone(joints_array, img, SpineBase, HipLeft)

    # Right Arm
    img = draw_bone(joints_array, img, ShoulderRight, ElbowRight)
    img = draw_bone(joints_array, img, ElbowRight, WristRight)
    img = draw_bone(joints_array, img, WristRight, HandRight)
    img = draw_bone(joints_array, img, HandRight, HandTipRight)
    img = draw_bone(joints_array, img, WristRight, ThumbRight)

    # Left Arm
    img = draw_bone(joints_array, img, ShoulderLeft, ElbowLeft)
    img = draw_bone(joints_array, img, ElbowLeft, WristLeft)
    img = draw_bone(joints_array, img, WristLeft, HandLeft)
    img = draw_bone(joints_array, img, HandLeft, HandTipLeft)
    img = draw_bone(joints_array, img, WristLeft, ThumbLeft)

    # Right Leg
    img = draw_bone(joints_array, img, HipRight, KneeRight)
    img = draw_bone(joints_array, img, KneeRight, AnkleRight)
    img = draw_bone(joints_array, img, AnkleRight, FootRight)

    # Left Leg
    img = draw_bone(joints_array, img, HipLeft, KneeLeft)
    img = draw_bone(joints_array, img, KneeLeft, AnkleLeft)
    img = draw_bone(joints_array, img, AnkleLeft, FootLeft)

    return img


def quat2euler(x, y, z, w):
    t0 = 2.0 * (w * x + y * z)
    t1 = 1.0 - 2.0 * (x * x + y * y)
    roll = np.degrees(np.arctan2(t0, t1))

    t2 = 2.0 * (w * y - z * x)
    # t2 = 1 if t2 > 1 else t2
    # t2 = -1 if t2 < -1 else t2
    t2[t2 > 1] = 1
    t2[t2 < -1] = -1
    pitch = np.degrees(np.arcsin(t2))

    t3 = 2.0 * (w * z + x * y)
    t4 = 1.0 - 2.0 * (y * y + z * z)
    yaw = np.degrees(np.arctan2(t3, t4))

    return yaw, pitch, roll


