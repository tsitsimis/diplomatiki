from naoqi import ALProxy
import vision_definitions as vd
import almath
import cv2
from my_utils import *

IP = "192.168.0.107"
PORT = 9559

PROCESS_NAME = "VisualCompassTest"

# joint names
headYawName = "HeadYaw"
headPitchName = "HeadPitch"
RShoulderRollName = "RShoulderRoll"
RShoulderPitchName = "RShoulderPitch"
RElbowRollName = "RElbowRoll"
RElbowYawName = "RElbowYaw"
RWristYawName = "RWristYaw"
RHandName = "RHand"

compass_proxy = ALProxy("ALVisualCompass", IP, PORT)
memory_proxy = ALProxy("ALMemory", IP, PORT)
motion_proxy = ALProxy("ALMotion", IP, PORT)

# set reference image
compass_proxy.enableReferenceRefresh(True)
compass_proxy.setResolution(1)

compass_proxy.subscribe(PROCESS_NAME)
ref_image = get_ref_image(compass_proxy)
cv2.imshow("Reference Image", ref_image)

dist = 0
while dist <= 0.4:
    current_image = get_current_image(compass_proxy)

    # Get the deviation information from the ALMemory event.
    try:
        deviation = memory_proxy.getData("VisualCompass/Deviation")
        match_info = memory_proxy.getData("VisualCompass/Match")
    except:
        continue

    wy = deviation[0][0]
    wz = deviation[0][1]
    wz_deg = wz * 180.0 / np.pi
    wy_deg = wy * 180.0 / np.pi

    # walk
    dx = 0.1
    motion_proxy.moveTo(dx, 0.0, -wz)
    dist += dx

    # show current image
    cv2.imshow("Current Image", current_image)

    # press q to exit
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break


# un-subscribe
compass_proxy.unsubscribe(PROCESS_NAME)

