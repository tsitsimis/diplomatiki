from naoqi import ALProxy
import vision_definitions as vd
import almath
import cv2
import Image
import numpy as np


def get_ref_image(compass_proxy):
    nao_image = compass_proxy.getReferenceImage()
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]

    # convert to numpy array
    frame = Image.frombytes("L", (image_width, image_height), array)
    frame = np.array(frame)
    return frame


def get_current_image(compass_proxy):
    nao_image = compass_proxy.getCurrentImage()
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]

    # convert to numpy array
    frame = Image.frombytes("L", (image_width, image_height), array)
    frame = np.array(frame)
    return frame


def nao_walk(ip, port, dist, theta):
    motion_proxy = ALProxy("ALMotion", ip, port)
    motion_proxy.moveTo(dist, 0.0, theta)  # blocking call, moveTo(x, y, theta)
