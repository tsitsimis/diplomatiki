close all;

%% open camera
vid = videoinput('linuxvideo', 1);
start(vid);
preview(vid);

%% get snapshot
closepreview(vid);
img = getsnapshot(vid);
stop(vid);
delete(vid);

%% convert RGB to HSV
[rows, cols, ~] = size(img);
img = im2double(img);
img_hsv = rgb2hsv(img);

figure;
hold all;
imshowpair(img, img_hsv, 'Montage');
title('RGB to HSV');

%% Integral image
imgH = img_hsv(:, :, 1); % Hue channel
intImg = cumsum(cumsum(imgH, 1), 2);

figure;
hold all;
imshowpair(imgH, intImg, 'Montage');
title('Integral Image');

%% Adaptive thresholding
img_bin = zeros(rows, cols);
s = cols/ 8;
t = 0.15;
for i = 1:rows
    for j = 1:cols
        
        % set the sxs region
        i1 = i - floor(s / 2);
        j1 = j - floor(s / 2);
        i2 = i + floor(s / 2);
        j2 = j + floor(s / 2);
        
        % check borders
        if i1 < 1
            i1 = 1;
        end
        if j1 < 1
            j1 = 1;
        end
        if i2 > rows
            i2 = rows;
        end
        if j2 > cols
            j2 = cols;
        end
        
        count = (i2 - i1) * (j2 - j1);
        sum = intImg(i2, j2) - intImg(i1, j2) - intImg(i2, j1) + intImg(i1, j1);
        if imgH(i, j) * count <= sum * (1 - t)
            img_bin(i, j) = 0;
        else
            img_bin(i, j) = 1;
        end
    end
end

figure;
imshow(img_bin);

%% morphological opening
%B1 = strel('disk', 7);
%img_open = imopen(img_bin, B1);

%% morphological closing
%B2 = strel('disk', 10);
%img_openclose = imclose(img_open, B2);

%figure;
%imshowpair(img_open, img_openclose, 'Montage');
%title('open-closing');

%% fill holes, clear border
%imgClean = imfill(imgClean, 'holes');
%imgClean = imclearborder(imgClean);

%figure;
%imshow(imgClean);






