close all;

%% read image
img = imread('red_1.jpg');

%% convert RGB to L*a*b*
[nrows, ncols, ~] = size(img);
cform = makecform('srgb2lab');
img_lab = applycform(img, cform);

figure;
imshowpair(img, img_lab, 'Montage');

%% take a and b channels
img_ab = double(img_lab(:, :, 2:3));
img_ab = reshape(img_ab, nrows * ncols, 2);

%% k-means segmentation
nColors = 3;
[cluster_idx, cluster_center] = kmeans(img_ab, nColors, 'distance', 'sqEuclidean', 'Replicates', 3);

pixel_labels = reshape(cluster_idx, nrows, ncols);

figure;
imshow(pixel_labels, []);

%% extract regions
% region 1
reg1 = cluster_idx == 1;
reg1 = reshape(reg1, nrows, ncols);
reg1 = uint8(reg1);
img_reg1 = (img) .* repmat(reg1, [1, 1, 3]);

% region 2
reg2 = cluster_idx == 2;
reg2 = reshape(reg2, nrows, ncols);
reg2 = uint8(reg2);
img_reg2 = (img) .* repmat(reg2, [1, 1, 3]);

% region 3
reg3 = cluster_idx == 3;
reg3 = reshape(reg3, nrows, ncols);
reg3 = uint8(reg3);
img_reg3 = (img) .* repmat(reg3, [1, 1, 3]);

%% show regions
img_regions(:, :, :, 1) = img_reg1;
img_regions(:, :, :, 2) = img_reg2;
img_regions(:, :, :, 3) = img_reg3;

figure;
montage(img_regions, 'Size', [1, 3]);