close all;
clear cam;

%% read reference image
referenceImage = imread('student_id.jpg');
refImgGray = rgb2gray(referenceImage);

referencePts = detectSURFFeatures(refImgGray);
referenceFeatures = extractFeatures(refImgGray, referencePts);

% figure;
% imshow(referenceImage); hold on;
% plot(referencePts.selectStrongest(50));

%% camera
cam = webcam();

cameraFrame = snapshot(cam);
frameSize = size(cameraFrame);

%videoPlayer = vision.VideoPlayer('Position', [100 100 [frameSize(2), frameSize(1)]+30]);

runLoop = true;
numPts = 0;
frameCount = 0;

%while runLoop && frameCount < 400

    % Get the next frame.
    cameraFrame = snapshot(cam);
    cameraFrameGray = rgb2gray(cameraFrame);
    frameCount = frameCount + 1;

    cameraPts = detectSURFFeatures(cameraFrameGray);
    cameraFeatures = extractFeatures(cameraFrameGray, cameraPts);
    idxPairs = matchFeatures(cameraFeatures, referenceFeatures);
    
    matchedCameraPts = cameraPts(idxPairs(:, 1));
    matchedReferencePts = referencePts(idxPairs(:, 2));
    
    %figure;
    %showMatchedFeatures(cameraFrame, referenceImage, ...
    %    matchedCameraPts, matchedReferencePts, 'Montage');
    
    %% geometric transformation
    [referenceTransform, inlierReferencePts, inlierCameraPts] = ...
        estimateGeometricTransform(matchedReferencePts, matchedCameraPts, 'Similarity');
    
    figure;
    showMatchedFeatures(cameraFrame, referenceImage, ...
        inlierCameraPts, inlierReferencePts, 'Montage');
    
    box = imread('box.png');
    repDims = size(box);
    refDims = size(referenceImage);
    sx = refDims(2) / repDims(2);
    sy = refDims(1) / repDims(1);
    
    scaleTransform = affine2d([sx, 0, 0; 0, sy, 0; 0, 0, 1]);
    outputView = imref2d(size(referenceImage));
    boxScaled = imwarp(box, scaleTransform, 'OutputView', outputView);
    
%     figure;
%     imshowpair(referenceImage, boxScaled, 'Montage');
    
    %% apply geometric transform to box
    outputView = imref2d(size(cameraFrame));
    boxTransformed = imwarp(boxScaled, referenceTransform, 'OutputView', outputView);
    boxTransformed = rgb2gray(boxTransformed);
    boxTransformed = im2double(boxTransformed);
    
    %figure;
    %imshowpair(cameraFrame, boxTransformed, 'Montage');
    
    figure;
    redBorder = zeros(size(cameraFrame, 1), size(cameraFrame, 2), 3);
    redBorder(:, :, 1) = 1;
    imshow(cameraFrame), hold on;
    h2 = imshow(redBorder);
    set(h2, 'AlphaData', boxTransformed);
    

    %step(videoPlayer, cameraFrame);
    %runLoop = isOpen(videoPlayer);
%end


%% close camera
clear cam;