function [num, score, features] = get_features(select)

%close all;

%% get samples filenames
if select == 1
    root = '/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots4/blue/';
elseif select == 2
    root = '/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots4/yellow/';
end

files = dir(root);
files = files(3:end);  % remove . and .. folders
n_samples = size(files, 1);
n_features = 17;
features = zeros(n_samples, n_features);
score = zeros(n_samples, 1);
num = zeros(n_samples, 1);

for i = 1:n_samples
    % read image
    img_name = files(i).name;
    [score(i), num(i)] = get_score(img_name);
    filename = strcat(root , img_name);
    img = imread(filename);
    
    % extract features
    features(i, :) = image_features(img, 0);
end

% feaures
% 1 centroid x / centroid y / 3 width / 4 height / 5 area*
% 6 major axis / 7 minor axis / 8 perimeter / 9 orientation / 10 m11 / 11 solidity
% 12 m20 / 13 m02 / 14 m21 / 15 m12 / 16 phi1
% 17 phi2