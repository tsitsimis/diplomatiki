function [score, num] = get_score(imgName)
    components = strsplit(imgName, '_');  % {'green', num, score.jpg}
    
    % get img number
    num = components{2};
    num = str2double(num);
    
    % get img score
    score = components{3};
    score = strsplit(score, '.');
    score = score(1);
    score = str2double(score) / 100;
end