function [train_ind, test_ind] = get_train_test_sets(features, p)
    n_samples = length(features);
    n_train = ceil(p * n_samples);
    
    train_ind = randsample(n_samples, n_train)'; % Select randomly train samples
    test_ind = 1:n_samples;
    test_ind(train_ind) = [];                  % The rest are the test samples
end