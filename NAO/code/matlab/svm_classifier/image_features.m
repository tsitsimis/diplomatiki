function x = image_features(img, showImage)
    %close all;
    %img = imread('/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots4/blue/blue_54_100.jpg');
    img = im2double(img);
    
    %% RGB to HSV
    img_hsv = rgb2hsv(img);

    %% Hue channel
    img_hue = img_hsv(:, :, 1);
    
    %% Binary image
    %img_bin = (img_hue < 0.4);  % yellow
    img_bin = (img_hue > 0.6 & img_hue < 0.8);  % blue
    
    %% morphological opening
    B1 = strel('disk', 10);
    img_open = imopen(img_bin, B1);

    %% morphological closing
    B2 = strel('disk', 10);
    img_openclose = imclose(img_open, B2);

    final_img = img_openclose;

    %% keep largest area
    obj1 = bwareafilt(final_img, 1);
    obj2 = bwareafilt(final_img, 2);
    rp1 = regionprops(obj1, 'Area');
    rp2 = regionprops(obj2, 'Area');
    area1 = rp1.Area;
    if length(rp2) == 2
        area2 = rp2(1).Area + rp2(2).Area;
    else
        area2 = rp2(1).Area;
    end
    
    
    if area2-area1 > 3000
        final_img = obj2;
    else
        final_img = obj1;
    end
    
    final_img = obj1;
    
    %% convex hull
    img_ch = bwconvhull(final_img);
    %finalImg = img_ch;
    
    %% extract features
    rp = regionprops(final_img, 'Centroid', 'BoundingBox', 'Area', 'Perimeter', 'Orientation',...
        'MajorAxisLength', 'MinorAxisLength', 'Solidity');
    
    centroid = rp.Centroid;
    boundingBoxWidth = rp.BoundingBox(3);
    boundingBoxHeight = rp.BoundingBox(4);
    area = rp.Area;
    majorAxis = rp.MajorAxisLength;
    minorAxis = rp.MinorAxisLength;
    perimeter = rp.Perimeter;
    orientation = rp.Orientation;
    solidity = rp.Solidity;

    % image moments
    M10 = immoment(final_img, 1, 0);
    M01 = immoment(final_img, 0, 1);
    M00 = immoment(final_img, 0, 0);
    M11 = immoment(final_img, 1, 1);
    M20 = immoment(final_img, 2, 0);
    M02 = immoment(final_img, 0, 2);
    M12 = immoment(final_img, 1, 2);
    M21 = immoment(final_img, 2, 1);
    
    x_bar = M10 / M00;
    y_bar = M01 / M00;
    m11 = M11 - y_bar * M10;
    m20 = M20 - x_bar * M10;
    m02 = M02 - y_bar * M01;
    m21 = M21 - 2*x_bar*M11 - y_bar*M20 + 2*x_bar^2*M01;
    m12 = M12 - 2*y_bar*M11 - x_bar*M02 + 2*y_bar^2*M10;
    
    phi1 = m20 + m02;
    phi2 = (m20 - m02)^2 + 4*m11^2;

    %% feature vector
    x = [centroid(1),...
         centroid(2),...
         boundingBoxWidth,...
         boundingBoxHeight,...
         area,...
         majorAxis,...
         minorAxis,...
         perimeter,...
         abs(orientation),...
         m11,...
         solidity,...
         m20,...
         m02,...
         m21,...
         m12,...
         phi1,...
         phi2
         ];
     
     %% show image
     if showImage
%         figure;
%         imshowpair(img, img_hsv, 'Montage');
% 
%         figure;
%         imshowpair(img_hue, img_bin, 'Montage');
%         
%         figure;
%         imshowpair(img_open, img_openclose, 'Montage');
%         
%         figure;
%         imshowpair(obj1, obj2, 'Montage');

        figure;
        imshowpair(img, final_img, 'Montage');
     end

%end
