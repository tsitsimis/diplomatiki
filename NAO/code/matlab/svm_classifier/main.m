% close all;

%% get features for all images
disp('extracting features...');
[num1, Y1, X1] = get_features(1);

%% select features
f = [1, 9];
X1r = X1(:, f);

figure;
hold all;
gscatter(X1r(:, 1), X1r(:, 2), Y1, 'rgb', 'ooo');

%% cross-validation
folds = 10;
accuracy = zeros(1, folds);
for i = 1:folds
    %% create random train and test sets
    [train_ind, test_ind] = get_train_test_sets(X1r, 0.7);
    train_set = X1r(train_ind, :);
    test_set = X1r(test_ind, :);

    %% train classifier
    Mdl = fitcecoc(train_set, Y1(train_ind));

    %% test classifier
    predicted_labels = predict(Mdl, test_set);
    true_labels = Y1(test_ind);
    accuracy(i) = length(find(predicted_labels == true_labels)) / length(predicted_labels);
end

m_acc = mean(accuracy);

return;
%% decision boundaries
SVMModels = cell(3, 1);
classes = unique(Y1);
for j = 1:3;
    %indx = strcmp(Y, classes(j));
    indx = (Y1 == classes(j));
    SVMModels{j} = fitcsvm(X1r, indx, 'ClassNames', [false true], 'Standardize', true,...
        'KernelFunction', 'rbf', 'BoxConstraint', 1);
end

d = 0.5;
[x1Grid, x2Grid] = meshgrid(min(X1r(:, 1)):d:max(X1r(:, 1)), min(X1r(:, 2)):d:max(X1r(:, 2)));
xGrid = [x1Grid(:), x2Grid(:)];
N = size(xGrid, 1);
Scores = zeros(N, numel(classes));

for j = 1:numel(classes);
    [~, score] = predict(SVMModels{j}, xGrid);
    Scores(:, j) = score(:, 2);
end

[~, maxScore] = max(Scores, [], 2);

figure;
h(1:3) = gscatter(xGrid(:, 1), xGrid(:, 2), maxScore, [0.1 0.5 0.5; 0.5 0.1 0.5; 0.5 0.5 0.1]);
hold on;
h(4:6) = gscatter(X1r(:, 1), X1r(:, 2), Y1);

