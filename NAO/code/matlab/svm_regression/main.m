close all;

load carsmall;
rng 'default';

X = [Horsepower,Weight];
Y = MPG;

%% train SVM model
Mdl = fitrsvm(X,Y);
Mdl.ConvergenceInfo.Converged