import cv2
import numpy as np
from PolygonDrawer import PolygonDrawer
from sklearn.mixture import GaussianMixture
from scipy.stats import multivariate_normal
from imutils import build_montages


def collect_samples(n, save=False, filename="samples.txt"):
    color_points = np.zeros((0, 3))

    for i in range(n):
        # take sample image
        camera = cv2.VideoCapture(0)
        while True:
            # grab the current frame
            (grabbed, frame) = camera.read()

            # show the frame to our screen
            cv2.imshow("Frame", frame)

            key = cv2.waitKey(1) & 0xFF
            if key == ord("o"):
                img = frame
                break

        camera.release()
        cv2.destroyAllWindows()

        # select color region
        pd = PolygonDrawer(img.copy())
        mask = pd.get_mask()

        # get pixel values
        loc = np.where(mask != 0)
        cp = img[loc[0], loc[1], :]
        color_points = np.concatenate((color_points, cp), axis=0)
        if save:
            np.savetxt(filename, color_points)

    return color_points


def load_samples(filename):
    color_points = np.loadtxt(filename)
    return color_points


def fit_model(color_points, n):
    model = GaussianMixture(n).fit(color_points)
    mean = model.means_
    cov = model.covariances_
    return mean, cov


def adapt_model(mean_0, cov_0, mean_1, cov_1):
    pass


def eval_sample(img, mean, cov):
    proba_img = 0.5 * multivariate_normal.pdf(img, mean[0], cov[0]) + \
                0.5 * multivariate_normal.pdf(img, mean[1], cov[1])
    proba_img /= np.max(proba_img)
    proba_img *= 255
    return proba_img


def get_contour(proba_img, t=10):
    proba_img = proba_img
    thres = cv2.inRange(proba_img, t, 255)

    kernel = np.ones((3, 3), dtype=np.uint8)
    opening = cv2.morphologyEx(thres, cv2.MORPH_OPEN, kernel)

    kernel = np.ones((3, 3), dtype=np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    mask = closing
    # mask = np.dstack([mask] * 3)
    return mask


def show_results(frame, proba_img, mask):
    proba_img = np.dstack([proba_img]*3)
    mask = np.dstack([mask]*3)
    images_to_show = [frame, proba_img, mask]
    cv2.namedWindow("montage", cv2.WINDOW_NORMAL)
    montages = build_montages(images_to_show,  (640/2, 480/2), (len(images_to_show), 1))
    for montage in montages:
        cv2.imshow("montage", montage)

