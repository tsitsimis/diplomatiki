from myutils import *


# initialize NAO camera
# subscriber = init_cam(videoProxy)

blueLower = (88, 81, 0)
blueUpper = (255, 255, 155)
cyan_lower = (0, 129, 95)
cyan_upper = (255, 255, 255)
red_lower = (0, 146, 118)
red_upper = (255, 255, 255)

x = np.array([1, 1, 1, 1])
z = np.array([1, 1])
P = 0.01 * np.eye(4)
F = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]])
H = np.array([[1, 0, 0, 0], [0, 1, 0, 0]])
Q = 0.01 * np.eye(4)
R = 0.1 * np.eye(2)

camera = cv2.VideoCapture(1)
while True:
    # grab the current frame
    (grabbed, frame) = camera.read()
    # read image from NAO camera
    # frame = cam2numpy(videoProxy, subscriber)

    # resize
    frame = imutils.resize(frame, width=600)

    # convert to the HSV color space
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # threshold
    # mask_thres = cv2.inRange(hsv, blueLower, blueUpper)
    mask_thres = cv2.inRange(hsv, cyan_lower, cyan_upper)
    # mask_thres = cv2.inRange(hsv, red_lower, red_upper)

    # morphological filters
    kernel = np.ones((5, 5), np.uint8)
    mask_open = cv2.morphologyEx(mask_thres, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((10, 10), np.uint8)
    mask_close = cv2.morphologyEx(mask_open, cv2.MORPH_CLOSE, kernel)

    mask = mask_close
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask
        c = max(cnts, key=cv2.contourArea)

        # compute min area rectangle
        rect = ((x_rect, y_rect), (width, height), angle) = cv2.minAreaRect(c)

        if width > height:
            orientation = np.abs(angle)
        else:
            orientation = (90 - np.abs(angle))

        box = cv2.boxPoints(rect)
        box = box.astype(int)

        # compute centroid
        M = cv2.moments(c)
        if M["m00"] != 0:
            (z1, z2) = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            z = np.array([z1, z2])

        # draw min area rectangle and centroid
        cv2.drawContours(frame, [box], 0, (0, 255, 255), 2)
        cv2.circle(frame, (z1, z2), 5, (0, 0, 255), -1)

    # Kalman filter
    # predict
    x = np.dot(F, x)
    P = np.dot(np.dot(F, P), F.T) + Q
    # update
    y = z - np.dot(H, x)
    S = R + np.dot(np.dot(H, P), H.T)
    K = np.dot(np.dot(P, H.T), np.linalg.inv(S))
    x = x + np.dot(K, y)
    P = np.dot((np.eye(4) - np.dot(K, H)), P)
    cv2.circle(frame, (int(x[0]), int(x[1])), 5, (255, 0, 0), -1)

    # show the frame to our screen
    cv2.imshow("Frame", frame)
    # cv2.imshow("Frame", np.concatenate((frame, np.dstack([hsv[:,:,0]] * 3)), axis=1))
    # cv2.imshow("Frame", np.concatenate((frame, hsv), axis=1))

    key = cv2.waitKey(1) & 0xFF
    # if the 'q' key is pressed, stop the loop
    if key == ord("q"):
        break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()