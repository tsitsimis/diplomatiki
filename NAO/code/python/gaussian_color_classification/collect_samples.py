import numpy as np
import cv2
from scipy.stats import multivariate_normal
from imutils import build_montages
from PolygonDrawer import PolygonDrawer
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


img = cv2.imread("/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/green_2_100.jpg")
# img = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
pd = PolygonDrawer(img.copy())
mask = pd.get_mask()

loc = np.where(mask != 0)
color_points = img[loc[0], loc[1], :]
# np.savetxt("8.txt", color_points)

fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
ax.scatter(color_points[:, 0], color_points[:, 1], color_points[:, 2])

ax.set_xlabel("R")
ax.set_ylabel("G")
ax.set_zlabel("B")
plt.show()
