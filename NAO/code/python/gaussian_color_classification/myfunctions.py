from naoqi import ALProxy
import vision_definitions as vd
import Image
import numpy as np
import cv2
from sklearn.mixture import GaussianMixture
from scipy.stats import multivariate_normal
import os
from imutils import build_montages


def init_cam(video_proxy):
    cam_bottom = 0
    fps = 12
    try:
        video_proxy.unsubscribe("demo")
    except:
        pass
    subscriber = video_proxy.subscribeCamera("demo", cam_bottom, vd.kVGA, vd.kRGBColorSpace, fps)
    return subscriber


def cam2numpy(video_proxy, subscriber):
    # read image from NAO camera
    nao_image = video_proxy.getImageRemote(subscriber)
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]
    # convert NAO image to numpy array
    frame = Image.frombytes("RGB", (image_width, image_height), array)
    frame = np.array(frame)
    return frame


def train_color_model(my_dir):
    # my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/code/python/nao_handover/color_samples/txt/"
    files = os.listdir(my_dir)
    n_samples = len(files)

    samples = np.zeros((0, 3))

    for i in range(0, n_samples):
        full_path = my_dir + files[i]
        color_points = np.loadtxt(full_path)
        samples = np.concatenate((samples, color_points), axis=0)

    # mean = np.mean(samples, axis=0)
    # cov = np.cov(samples, rowvar=0)
    model = GaussianMixture(2).fit(color_points)
    mean = model.means_
    cov = model.covariances_

    return mean, cov


def load_samples(filename):
    color_points = np.loadtxt(filename)
    return color_points


def fit_model(color_points, n):
    model = GaussianMixture(n).fit(color_points)
    mean = model.means_
    cov = model.covariances_
    return mean, cov


def proba_image(img, mean, cov, d3=True):
    proba_img = multivariate_normal.pdf(img, mean, cov)
    proba_img /= np.max(proba_img)
    proba_img *= 255

    if d3:
        proba_img = np.dstack([proba_img] * 3)

    return proba_img


def get_bin_image(proba_img):
    proba_img = proba_img[:, :, 0]
    thres = cv2.inRange(proba_img, 1, 255)
    return thres


def get_contour(proba_img, t):
    proba_img = proba_img[:, :, 0]
    thres = cv2.inRange(proba_img, t, 255)

    kernel = np.ones((3, 3), dtype=np.uint8)
    opening = cv2.morphologyEx(thres, cv2.MORPH_OPEN, kernel)

    kernel = np.ones((10, 10), dtype=np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    mask = closing
    mask = np.dstack([mask] * 3)
    return mask


def get_contour2(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # threshold
    blue_lower = (88, 66, 0)
    blue_upper = (255, 255, 255)
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    thres = cv2.inRange(hsv, blue_lower, blue_upper)

    kernel = np.ones((10, 10), dtype=np.uint8)
    opening = cv2.morphologyEx(thres, cv2.MORPH_OPEN, kernel)

    kernel = np.ones((10, 10), dtype=np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    mask = closing
    mask = np.dstack([mask] * 3)
    return mask

