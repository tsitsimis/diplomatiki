from myfunctions import *


my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/"

# get features for all images
X, Y, img_num = get_train_features(my_dir)

# train with only a few samples
n_samples = np.array([80, 100, 120, 140, 160, 179])
n_samples_classify(X, Y, n_samples)
exit()
# test various parameter values
# c = np.array([1e-1, 5e-1, 1e0, 5e0, 1e1, 5e1, 1e2, 5e2, 1e3])
# gamma = np.array([1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2])
# accuracy_heatmap(X, Y, c, gamma)

# SVM
clf_svm = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
cross_val_svm = model_selection.cross_val_score(clf_svm, X, Y, cv=10)
print "SVM: %.1f" % round(100 * np.mean(cross_val_svm))

exit()
# Logistic Regression
clf_logreg = linear_model.LogisticRegression(C=1e5)
cross_val_logreg = model_selection.cross_val_score(clf_logreg, X, Y, cv=10)
print "Logreg: %.1f" % round(100 * np.mean(cross_val_logreg))

# Neural Network
clf_nn = neural_network.MLPClassifier(hidden_layer_sizes=(100), activation='logistic')
cross_val_nn = model_selection.cross_val_score(clf_nn, X, Y, cv=10)
print "NN: %.1f" % round(100 * np.mean(cross_val_nn))

# Tree
clf_tree = tree.DecisionTreeClassifier()
cross_val_tree = model_selection.cross_val_score(clf_tree, X, Y, cv=10)
print "Tree: %.1f" % round(100 * np.mean(cross_val_tree))

# Naive bayes
clf_naive = naive_bayes.GaussianNB()
cross_val_naive = model_selection.cross_val_score(clf_naive, X, Y, cv=10)
print "Naive Bayes: %.1f" % round(100 * np.mean(cross_val_naive))






