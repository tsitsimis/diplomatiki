from myfunctions import *

# Connect to NAO
# IP = "169.254.28.162"
IP = "192.168.0.108"
PORT = 9559

videoProxy = ALProxy("ALVideoDevice", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
init_joints(motionProxy, speed)

######## 2nd configuration ########
nao_move_shoulder(IP, PORT, 5)
###################################

# initialize camera
subscriber = init_cam(videoProxy)

# save images to this directory
sub_folder = "green"
image_prefix = "/green_"
my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots11_2classes/" + sub_folder + "/"

cnt = get_counter(my_dir)

# camera = cv2.VideoCapture(1)
while True:
    # read image from camera
    # (grabbed, frame) = camera.read()            # webcam
    frame = cam2numpy(videoProxy, subscriber)   # NAO

    # # object contour
    # found, contour = get_object_contour(frame)
    #
    # if found:
    #     box = contour2box(contour)
    #     cv2.drawContours(frame, [box], 0, (0, 0, 255), 2)

    # show frame
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    cv2.imshow("Frame", frame)

    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break
    elif key == ord("1"):
        quality = 0
        img = Image.fromarray(frame)
        img.save(my_dir + "/green_" + str(cnt) + "_" + str(quality) + ".jpg")
        cnt += 1

    elif key == ord("2"):
        quality = 50
        img = Image.fromarray(frame)
        img.save(my_dir + image_prefix + str(cnt) + "_" + str(quality) + ".jpg")
        cnt += 1

        # motionProxy.setAngles(RHandName, 0.0, speed)
        # time.sleep(2)
        # motionProxy.setAngles(RHandName, 1.0, speed)

    elif key == ord("3"):
        quality = 100
        img = Image.fromarray(frame)
        img.save(my_dir + image_prefix + str(cnt) + "_" + str(quality) + ".jpg")
        cnt += 1


videoProxy.unsubscribe(subscriber)
