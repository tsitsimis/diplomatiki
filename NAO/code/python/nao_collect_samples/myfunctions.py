from naoqi import ALProxy
import vision_definitions as vd
import almath
import Image
import numpy as np
import cv2
import time
import os


# joint names
headYawName = "HeadYaw"
headPitchName = "HeadPitch"
RShoulderRollName = "RShoulderRoll"
RShoulderPitchName = "RShoulderPitch"
RElbowRollName = "RElbowRoll"
RElbowYawName = "RElbowYaw"
RWristYawName = "RWristYaw"
RHandName = "RHand"


def init_joints(motion_proxy, speed):
    motion_proxy.setStiffnesses(["RArm", "Head"], 1.0)
    motion_proxy.setAngles(RElbowRollName, 39.3 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderRollName, 9.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderPitchName, 10 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RElbowYawName, 62.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RWristYawName, 0.0 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RHandName, 1.0, speed)
    motion_proxy.setAngles(headPitchName, 20 * almath.TO_RAD, speed)


def nao_move_shoulder(ip, port, delta_theta):
    motion_proxy = ALProxy("ALMotion", ip, port)

    theta0 = motion_proxy.getAngles(RShoulderRollName, True)[0]
    theta1 = theta0 + delta_theta
    motion_proxy.setAngles(RShoulderRollName, theta1, 0.03)


def nao_stiffness(ip, port, v):
    motion_proxy = ALProxy("ALMotion", ip, port)
    motion_proxy.setStiffnesses(["Body"], v)


def init_cam(video_proxy):
    cam_bottom = 0
    fps = 1
    try:
        video_proxy.unsubscribe("demo")
    except:
        pass
    subscriber = video_proxy.subscribeCamera("demo", cam_bottom, vd.kVGA, vd.kRGBColorSpace, fps)
    return subscriber


def cam2numpy(video_proxy, subscriber):
    # read image from NAO camera
    naoImage = video_proxy.getImageRemote(subscriber)
    imageWidth = naoImage[0]
    imageHeight = naoImage[1]
    array = naoImage[6]
    # convert NAO' s image to numpy array
    frame = Image.frombytes("RGB", (imageWidth, imageHeight), array)
    frame = np.array(frame)
    return frame


def get_counter(directory):
    files = os.listdir(directory)
    n_samples = len(files)

    if n_samples > 0:
        img_num = np.zeros(n_samples)

        for i in range(0, n_samples):
            img_name = files[i]
            _, img_num[i] = parse_filename(img_name)

        return int(np.max(img_num) + 1)

    return 1

def parse_filename(img_name):
    components = img_name.split("_")

    # get img number
    num = components[1]
    num = int(num)

    # get img score
    score = components[2]
    score = score.split('.')[0]
    # score = float(score) / 100
    score = np.int(score)

    return score, num


def get_object_contour(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # threshold
    blueLower = (88, 66, 0)
    blueUpper = (255, 255, 255)
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    cyan_lower = (0, 129, 95)
    cyan_upper = (255, 255, 255)
    red_lower = (0, 146, 118)
    red_upper = (255, 255, 255)

    # thresh = cv2.inRange(hsv, green_lower, green_upper)
    thresh = cv2.inRange(hsv, red_lower, red_upper)

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box