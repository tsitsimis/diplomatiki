from myfunctions import *

# my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/"
# my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots9/red2/"
dir1 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots10_2classes/green/"
dir2 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots11_2classes/green/"

# get features for all images
X1, Y1, img_num1 = get_train_features(dir1)
X2, Y2, img_num2 = get_train_features(dir2)

# sort according to label
# ind = np.argsort(Y)
# Y = Y[ind]
# X = X[ind, :]

# train with only a few samples
# k = 179
# ind = np.random.choice(X.shape[0], k, replace=False)
# X = X[ind, :]
# Y = Y[ind]

# select_features = [0, 1, 2, 3, 4]
# Xr = X[:, select_features]
# Xr = X

# best grasping
c1 = np.mean(X1[np.where(Y1 == 100), :], axis=1)
c2 = np.mean(X2[np.where(Y2 == 100), :], axis=1)

# train classifier 1
clf1 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf1, X1, Y1, cv=10))
clf1.fit(X1, Y1)

# train classifier 1
clf2 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf2, X2, Y2, cv=10))
clf2.fit(X2, Y2)

# connect to NAO
IP = "192.168.0.108"
# IP = "172.17.0.1"
PORT = 9559

ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
nao_posture(IP, PORT, "Crouch")
init_joints(motionProxy, speed)

# initialize camera
subscriber = init_cam(videoProxy)
# camera = cv2.VideoCapture(1)

# train color gaussian model
mean, cov = train_color_model()

# store object's last 5 positions
history = Queue.Queue(maxsize=5)
grasped_it = False
cnt_grasp = 0
cnt_bad_dim = 0
X = X1
Y = Y1
clf = clf1

while True:
    frame = cam2numpy(videoProxy, subscriber)  # read image from NAO camera
    # (grabbed, frame) = camera.read()

    found, contour = get_object_contour(frame)  # object contour

    if grasped_it and (not found):
        ttsProxy.say("Oops! Give it to me again!")
        grasped_it = False
        motionProxy.setAngles(RHandName, 1.0, 0.2)

    if found:
        box = contour2box(contour)  # contour bounding box

        features = cx, cy, orientation, width, height = contour_features(contour)  # contour features
        features = np.array(features)

        history, is_steady, sx, sy = is_object_steady(history, cx, cy)  # child holds the object still?

        if grasped_it:
            cnt_grasp += 1
            if cnt_grasp >= 5:
                if not is_steady:
                    ttsProxy.say("Oops! Give it to me again!")
                    grasped_it = False
                    cnt_grasp = 0
                    motionProxy.setAngles(RHandName, 1.0, 0.2)
                else:
                    ttsProxy.say("Thank you!")
                    exit()

        is_steady = True
        if is_steady:
            # x = [features[select_features]]
            x = features
            x = np.reshape(x, (1, -1))
            Y_pred = clf.predict(x)  # predict grasping quality

            bad_dim = np.argmax(np.abs(x - c1))
            # print nao_response(bad_dim)
            if bad_dim == 0:
                cnt_bad_dim += 1
                if cnt_bad_dim >= 10:
                    err = np.abs(x[0][0] - c1[0][0])
                    print "error: " + str(err)
                    # nao_move_shoulder(IP, PORT, 0.01 * err)
                    nao_move_shoulder(IP, PORT, 5)
                    clf = clf2
                    cnt_bad_dim = 0

            if Y_pred == 0:                 # no handover
                color = (0, 0, 255)
            elif Y_pred == 50:              # average handover
                color = (255, 255, 255)
            elif Y_pred == 100:             # good handover
                color = (255, 0, 0)
                motionProxy.setAngles(RHandName, 0.0, 0.2)
                grasped_it = True

        cv2.drawContours(frame, [box], 0, color, 2)  # draw detected object bounding box
        cv2.circle(frame, (cx, cy), 5, color, thickness=3)

            # show object in feature space
            # plt.scatter(x[0][0], x[0][1], s=50, c="green")
            # plt.draw()

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    cv2.imshow("Frame", frame)

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)

quit()
# go back to initial position
motionProxy.setAngles(RElbowRollName, 21 * almath.TO_RAD, speed)
motionProxy.setAngles(RShoulderRollName, -8 * almath.TO_RAD, speed)
motionProxy.setAngles(RShoulderPitchName, 86 * almath.TO_RAD, speed)
motionProxy.setAngles(RElbowYawName, 67 * almath.TO_RAD, speed)
motionProxy.setAngles(RWristYawName, 0.0 * almath.TO_RAD, speed)
motionProxy.setAngles(RHandName, 0.0, speed)