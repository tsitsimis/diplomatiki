import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os
from naoqi import ALProxy
import vision_definitions as vd
import Image
from sklearn import svm, metrics, model_selection, neighbors, tree
from scipy.stats import multivariate_normal
import almath
import time
import Queue

# joint names
headYawName = "HeadYaw"
headPitchName = "HeadPitch"
RShoulderRollName = "RShoulderRoll"
RShoulderPitchName = "RShoulderPitch"
RElbowRollName = "RElbowRoll"
RElbowYawName = "RElbowYaw"
RWristYawName = "RWristYaw"
RHandName = "RHand"


def init_joints(motion_proxy, speed):
    motion_proxy.setStiffnesses(["RArm", "Head"], 1.0)
    motion_proxy.setAngles(RElbowRollName, 39.3 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderRollName, 9.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderPitchName, 10 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RElbowYawName, 62.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RWristYawName, 0.0 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RHandName, 1.0, speed)
    motion_proxy.setAngles(headPitchName, 20 * almath.TO_RAD, speed)


def init_cam(video_proxy):
    cam_bottom = 0
    fps = 12
    try:
        video_proxy.unsubscribe("demo")
    except:
        pass
    subscriber = video_proxy.subscribeCamera("demo", cam_bottom, vd.kVGA, vd.kRGBColorSpace, fps)
    return subscriber


def cam2numpy(video_proxy, subscriber):
    # read image from NAO camera
    nao_image = video_proxy.getImageRemote(subscriber)
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]
    # convert NAO's image to numpy array
    frame = Image.frombytes("RGB", (image_width, image_height), array)
    frame = np.array(frame)
    return frame


def get_object_contour(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # threshold
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    cyan_lower = (0, 129, 95)
    cyan_upper = (255, 255, 255)
    red_lower = (0, 146, 118)
    red_upper = (255, 255, 255)

    thresh = cv2.inRange(hsv, green_lower, green_upper)
    # thresh = cv2.inRange(hsv, cyan_lower, cyan_upper)
    # thresh = cv2.inRange(hsv, red_lower, red_upper)

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def bin_image(proba_img, t=1):
    thresh = cv2.inRange(proba_img, t, 255)
    return thresh


def get_object_contour2(bin_img):
    thresh = bin_img

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box


def contour_features(contour):
    # fit rectangle
    ((_, _), (width, height), angle) = cv2.minAreaRect(contour)  # rectangle

    # centroid
    mom = cv2.moments(contour)
    cx = int(mom['m10'] / mom['m00'])
    cy = int(mom['m01'] / mom['m00'])

    # orientation
    if width > height:
        orientation = np.abs(angle)
    else:
        orientation = (90 - np.abs(angle))

    # rectangle corners
    nw_x = cx - width / 2
    nw_y = cy - height / 2

    ne_x = cx + width / 2
    ne_y = cy - height / 2

    sw_x = cx - width / 2
    sw_y = cy + height / 2

    se_x = cx + width / 2
    se_y = cy + height / 2

    # features vector
    features = [cx, cy, orientation, width, height]
    # features = [cx, cy, nw_x, nw_y, ne_x, ne_y, sw_x, sw_y, se_x, se_y, orientation, width, height]
    return features


def get_train_features(directory):
    files = os.listdir(directory)
    n_samples = len(files)
    n_features = 5
    # n_features = 13

    X = np.zeros((n_samples, n_features))
    Y = np.zeros(n_samples, dtype=int)
    img_num = np.zeros(n_samples)

    for i in range(0, n_samples):
        img_name = files[i]
        Y[i], img_num[i] = parse_filename(img_name)

        full_path = directory + img_name
        img = cv2.imread(full_path)
        found, contour = get_object_contour(img)
        if found:
            X[i, :] = contour_features(contour)

    return X, Y, img_num


def parse_filename(img_name):
    components = img_name.split("_")

    # get img number
    num = components[1]
    num = int(num)

    # get img score
    score = components[2]
    score = score.split('.')[0]
    # score = float(score) / 100
    score = np.int(score)

    return score, num


def create_train_test(features, p):
    n_samples = np.shape(features)[0]
    all_ind = range(0, n_samples)

    # train set
    n_train = np.ceil(p * n_samples)
    train_ind = np.random.choice(all_ind, n_train, replace=False)
    train_set = features[train_ind, :]

    # test set
    test_ind = [x for x in all_ind if x not in train_ind]
    test_set = features[test_ind, :]

    return train_ind, train_set, test_ind, test_set


def plot_decision_boundary(clf, X, Y):
    h = 0.5
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.coolwarm)

    # plt.xlabel('centroid x')
    # plt.ylabel('orientation')
    # plt.title('decision boundaries')
    #
    # classes = ['good', 'medium', 'bad']
    # class_colours = ['red', 'white', 'blue']
    # recs = []
    # for i in range(0,len(class_colours)):
    #     recs.append(mpatches.Rectangle((0, 0), 1, 1, fc=class_colours[i]))
    # plt.legend(recs,classes,loc=4)


def is_object_steady(history, cx, cy):
    # add new position
    history.put([cx, cy])

    # convert to numpy array
    history_np = np.array(list(history.queue))

    # calc std of x, y coordinates
    sx = np.std(history_np[:, 0])
    sy = np.std(history_np[:, 1])

    # remove oldest position
    if history.full():
        history.get()

    # check if object is steady
    is_steady = (sx <= 30 and sy <= 30)

    return history, is_steady, sx, sy


def nao_response(bad_dim):
    if bad_dim == 0:
        return "Bring the object closer to the x axis"
    elif bad_dim == 1:
        return "Bring the object closer to the y axis"
    elif bad_dim == 2:
        return "Rotate the object"
    elif bad_dim == 3 or bad_dim == 4:
        return "Bring the object closer to the z axis"


def train_color_model():
    my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/code/python/nao_handover/color_samples/txt/"
    files = os.listdir(my_dir)
    n_samples = len(files)

    samples = np.zeros((0, 3))

    for i in range(0, n_samples):
        full_path = my_dir + files[i]
        color_points = np.loadtxt(full_path)
        samples = np.concatenate((samples, color_points), axis=0)

    mean = np.mean(samples, axis=0)
    cov = np.cov(samples, rowvar=0)

    return mean, cov


def proba_image(img, mean, cov, d3=True):
    proba_img = multivariate_normal.pdf(img, mean, cov)
    proba_img /= np.max(proba_img)
    proba_img *= 255

    if d3:
        proba_img = np.dstack([proba_img] * 3)

    return proba_img


def nao_stiffness(ip, port, v):
    motion_proxy = ALProxy("ALMotion", ip, port)
    motion_proxy.setStiffnesses(["Body"], v)


def nao_posture(ip, port, posture, speed=0.5):
    posture_proxy = ALProxy("ALRobotPosture", ip, port)
    posture_proxy.goToPosture(posture, speed)  # blocking call


def get_hand_tf(ip, port):
    motion_proxy = ALProxy("ALMotion", ip, port)
    name = 'RArm'
    space = 0
    use_sensor_values = False
    tf = motion_proxy.getTransform(name, space, use_sensor_values)

    r_shoulder_roll = motion_proxy.getAngles(RShoulderRollName, use_sensor_values)
    return r_shoulder_roll


def move_hand(ip, port, tf0):
    motion_proxy = ALProxy("ALMotion", ip, port)
    space = 0  # FRAME_TORSO
    axis_mask = 3
    is_absolute = False
    effector = "RArm"

    tf1 = tf0
    # tf1[7] += 0.02
    path = [tf1]
    path = [[1.0, 0.0, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.05,
             0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 1.0]]

    path = [0.0, 0.05, 0.0,
            0.0, 0.0, 0.0]

    duration = 2.0  # seconds
    # motion_proxy.transformInterpolation(effector, space, path, axis_mask, duration, is_absolute)
    # motion_proxy.positionInterpolation(effector, space, path, axis_mask, duration, is_absolute)
    motion_proxy.setAngles(RShoulderRollName, 9.8 * almath.TO_RAD, 0.3)


def nao_move_shoulder(ip, port, delta_theta):
    motion_proxy = ALProxy("ALMotion", ip, port)

    theta0 = motion_proxy.getAngles(RShoulderRollName, True)[0]
    theta1 = theta0 + delta_theta
    motion_proxy.setAngles(RShoulderRollName, theta1, 0.03)









