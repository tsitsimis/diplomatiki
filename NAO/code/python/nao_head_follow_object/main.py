from myfunctions import *

IP = "169.254.28.162"
PORT = 9559

ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)

# initialize joints
fractionMaxSpeed = 0.02
motionProxy.setStiffnesses("Head", 1.0)

# initialize camera
CAM_BOTTOM = 0
FPS = 1
try:
    videoProxy.unsubscribe("demo")
except:
    pass
subscriber = videoProxy.subscribeCamera("demo", CAM_BOTTOM, vd.kVGA, vd.kRGBColorSpace, FPS)

# initialize Right arm
# lift_hand(motionProxy, fractionMaxSpeed)

while True:
    # read image from NAO camera
    frame = cam2numpy(videoProxy, subscriber)

    # object contour
    contour = get_object_contour(frame)

    if len(contour) > 0:
        # contour bounding box
        box = contour2box(contour)

        # contour features
        cx, cy, orientation, width, height = contour_features(contour)

        # draw features
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        cv2.drawContours(frame, [box], 0, (0, 255, 255), 2)
        cv2.circle(frame, (cx, cy), 5, color=(0, 0, 255), thickness=3)

        # turn NAO' s head to follow the detected object
        # turn_nao_head(motionProxy, fractionMaxSpeed, headYawName, headPitchName, width, height, cx, cy)

    cv2.imshow("Frame", frame)

    # press q to exit
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)
