import numpy as np
import cv2
import matplotlib.pyplot as plt
from naoqi import ALProxy
import vision_definitions as vd
import almath
import Image

# joint names
headYawName = "HeadYaw"
headPitchName = "HeadPitch"
RShoulderRollName = "RShoulderRoll"
RShoulderPitchName = "RShoulderPitch"
RElbowRollName = "RElbowRoll"
RElbowYawName = "RElbowYaw"
RWristYawName = "RWristYaw"
RHandName = "RHand"


def cam2numpy(videoProxy, subscriber):
    # read image from NAO camera
    naoImage = videoProxy.getImageRemote(subscriber)
    imageWidth = naoImage[0]
    imageHeight = naoImage[1]
    array = naoImage[6]
    # convert NAO's image to numpy array
    frame = Image.frombytes("RGB", (imageWidth, imageHeight), array)
    frame = np.array(frame)
    return frame

def get_object_contour(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # threshold
    blueLower = (88, 66, 0)
    blueUpper = (255, 255, 255)
    thresh = cv2.inRange(hsv, blueLower, blueUpper)

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    c = []
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)

    return c


def contour_features(contour):
    # fit rectangle
    rect = ((x, y), (width, height), angle) = cv2.minAreaRect(contour)

    # centroid
    M = cv2.moments(contour)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    # orientation
    if width > height:
        orientation = np.abs(angle)
    else:
        orientation = (90 - np.abs(angle))

    # features vector
    features = [cx, cy, orientation, width, height]
    return np.array(features)


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box


def turn_nao_head(motionProxy, fractionMaxSpeed, headYawName, headPitchName, imageWidth, imageHeight, cx, cy):
    dx = (imageWidth / 2.0 - cx) / (imageWidth / 2)
    if np.abs(dx) >= 0.2:
        K = 10
        deltaYaw = K * dx
        headYaw = motionProxy.getAngles(headYawName, False)[0] * almath.TO_DEG
        headYaw += deltaYaw

        headYaw = np.min([headYaw, 70])
        headYaw = np.max([headYaw, -70])

        headYawRad = headYaw * almath.TO_RAD
        motionProxy.setAngles(headYawName, headYawRad, fractionMaxSpeed)

    dy = (imageHeight / 2.0 - cy) / (imageHeight / 2)
    if np.abs(dy) >= 0.2:
        K = 10
        deltaPicth = K * dy
        headPitch = motionProxy.getAngles(headPitchName, False)[0] * almath.TO_DEG
        headPitch -= deltaPicth

        headPitch = np.min([headPitch, 20])
        headPitch = np.max([headPitch, -30])

        headPitchRad = headPitch * almath.TO_RAD
        motionProxy.setAngles(headPitchName, headPitchRad, fractionMaxSpeed)


def lift_hand(motionProxy, fractionMaxSpeed):
    motionProxy.setStiffnesses("Body", 1.0)
    motionProxy.setAngles(RElbowRollName, 39.3 * almath.TO_RAD, fractionMaxSpeed)
    motionProxy.setAngles(RShoulderRollName, 6.7 * almath.TO_RAD, fractionMaxSpeed)
    motionProxy.setAngles(RShoulderPitchName, -6.9 * almath.TO_RAD, fractionMaxSpeed)
    motionProxy.setAngles(RElbowYawName, 62.8 * almath.TO_RAD, fractionMaxSpeed)
    motionProxy.setAngles(RWristYawName, 0.0 * almath.TO_RAD, fractionMaxSpeed)
    motionProxy.setAngles(RHandName, 1.0, fractionMaxSpeed)