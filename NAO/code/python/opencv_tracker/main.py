from myutils import *


# read camera
camera = cv2.VideoCapture(0)

frame_width = camera.get(cv2.CAP_PROP_FRAME_WIDTH)
frame_height = camera.get(cv2.CAP_PROP_FRAME_HEIGHT)

# Read first frame.
plt.figure()
plt.axis([0, frame_width, frame_height, 0])
plt.ion()

while True:
    # Read a new frame
    ok, frame = camera.read()

    detected, features = color_based_detection(frame)
    if detected:
        cx, cy, box = features
        cv2.drawContours(frame, [box], 0, (0, 255, 255), 2)
        cv2.circle(frame, (cx, cy), 5, (0, 0, 255), -1)

        plt.scatter(cx, cy, s=50, c="green")
        plt.draw()

    # Display result
    cv2.imshow("Tracking", frame)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27:
        break

