import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt


blueLower = (88, 81, 0)
blueUpper = (255, 255, 155)


def color_based_detection(frame):
    # bgr to hsv
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # threshold
    mask_thres = cv2.inRange(hsv, blueLower, blueUpper)

    # morphological filters
    kernel = np.ones((5, 5), np.uint8)
    mask_open = cv2.morphologyEx(mask_thres, cv2.MORPH_OPEN, kernel)
    kernel = np.ones((10, 10), np.uint8)
    mask_close = cv2.morphologyEx(mask_open, cv2.MORPH_CLOSE, kernel)
    mask = mask_close
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask
        c = max(cnts, key=cv2.contourArea)

        # compute min area rectangle
        rect = ((x, y), (width, height), angle) = cv2.minAreaRect(c)

        # check if the detected area is large enough to correspond to the object
        if width * height > 1000:

            if width > height:
                orientation = np.abs(angle)
            else:
                orientation = (90 - np.abs(angle))

            box = cv2.boxPoints(rect)
            box = box.astype(int)

            # compute centroid
            M = cv2.moments(c)
            cx, cy = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            return True, [cx, cy, box]

        return False, []
    return False, []

