from my_utils import *


color_points = load_samples("samples_lab.txt")
mean_old, cov_old = fit_model(color_points)

img = cv2.imread("/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/green_1_100.jpg")

proba_img = eval_sample(img, mean_old, cov_old)
mask = get_contour(proba_img, t=10)

img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
segmented_img = region_growing(img_gray, mask)

show_results([img, proba_img, mask, segmented_img])

