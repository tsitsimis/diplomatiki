import vrep
import matplotlib.pyplot as plt


vrep.simxFinish(-1)
clientID = vrep.simxStart('127.0.0.1', 19999, True, True, 5000, 5)
if clientID > -1:
    print "Connection successful"

    _, rgb_handle = vrep.simxGetObjectHandle(clientID, 'NAO_vision1', vrep.simx_opmode_oneshot_wait)
    code1, rgb_res, rgb_image = \
        vrep.simxGetVisionSensorImage(clientID, rgb_handle, 0, vrep.simx_opmode_streaming)
    if vrep.simxGetConnectionId(clientID) != -1:
        code2, rgb_res, rgb_image = \
            vrep.simxGetVisionSensorImage(clientID, rgb_handle, 0, vrep.simx_opmode_buffer)

        print rgb_image
        # plt.imshow(rgb_image)
        # pause(0.1);

else:
    print "Failed connecting to remote API server"


