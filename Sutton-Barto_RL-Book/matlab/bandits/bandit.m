close all;

nB = 1000;
nA = 10;
nP = 200;

%epsilon = 0.1;
epsilon_array = [0, 0.01, 0.1, 0.3];
len_e = length(epsilon_array);

% true rewards
q_star = mvnrnd(zeros(nB, nA), eye(nA));

avg_reward_array = zeros(len_e, nP);
percent_optimal_action = zeros(len_e, nP);

for ei = 1:len_e
    epsilon = epsilon_array(ei);

    % initializations
    q_t = zeros(nB, nA); %estimated reward after pulling every arm
    q_n = zeros(nB, nA); %number of times every arm was pulled
    q_s = zeros(nB, nA); %sum of rewards from every arm
    all_rewards = zeros(nB, nP);
    %pickedMaxAction = zeros(nB,nP);

    for bi = 1:nB %repeat experiment for every bandit

        for pi = 1:nP
            if epsilon > rand % exploratory move
               arm = randi([1 10]);
            else % greedy move
                [~, arm] = max(q_t(bi, :));
            end
            reward = q_star(bi, arm);% + 1*randn;
            all_rewards(bi, pi) = reward;
            
%             [~, bestArm] = max( q_star(bi,:) ); 
%             if( arm == bestArm )
%                 pickedMaxAction(bi,pi) = 1; 
%             end

            q_n(bi, arm) = q_n(bi, arm) +1;
            %q_s(bi, arm) = q_s(bi, arm) + reward;
            %q_t(bi, arm) = q_s(bi, arm)/q_n(bi, arm);
            step = 1/q_n(bi, arm);
            q_t(bi, arm) = q_t(bi, arm) + step*(reward - q_t(bi, arm));
        end

    end

    avg_reward_array(ei, :) = mean(all_rewards, 1);
    %percent_optimal_action(ei, :) = mean(pickedMaxAction, 1);
end

figure;
hold all;
for i = 1:(len_e)
    plot(avg_reward_array(i, :), 'linewidth', 3);
end
legend('ε=0', 'ε=0.01', 'ε=0.1', 'ε=0.3');
xlabel('trials');
ylabel('reward');
ylim([0, 1.4])

% figure;
% plot(percent_optimal_action(1, :))
% hold on
% for i = 2:(len_e)
%     plot(percent_optimal_action(i, :))
% end
% legend('0', '0.01', '0.1')