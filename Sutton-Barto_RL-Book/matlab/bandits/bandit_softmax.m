close all;

nB = 1000;
nA = 10;
nP = 200;

%epsilon = 0.1;
tau_array = [0.01, 0.1, 0.5, 1];
len_tau = length(tau_array);

% true rewards
q_star = mvnrnd(zeros(nB, nA), eye(nA));

avg_reward_array = zeros(len_tau, nP);
percent_optimal_action = zeros(len_tau, nP);

for ti = 1:len_tau
    tau = tau_array(ti);

    % initializations
    q_t = zeros(nB, nA); %estimated reward after pulling every arm
    q_n = zeros(nB, nA); %number of times every arm was pulled
    q_s = zeros(nB, nA); %sum of rewards from every arm
    all_rewards = zeros(nB, nP);
    %pickedMaxAction = zeros(nB,nP);

    for bi = 1:nB %repeat experiment for every bandit

        for pi = 1:nP
%             if epsilon > rand % exploratory move
%                arm = randi([1 10]);
%             else % greedy move
%                 [~, arm] = max(q_t(bi, :));
%             end
            
            prob = exp(q_t(bi, :) / tau) / sum(exp(q_t(bi, :) / tau));
            arm = drand01(prob);
            
            reward = q_star(bi, arm);% + 1*randn;
            all_rewards(bi, pi) = reward;
            
%             [~, bestArm] = max( q_star(bi,:) ); 
%             if( arm == bestArm )
%                 pickedMaxAction(bi,pi) = 1; 
%             end

            q_n(bi, arm) = q_n(bi, arm) + 1;
            %q_s(bi, arm) = q_s(bi, arm) + reward;
            %q_t(bi, arm) = q_s(bi, arm)/q_n(bi, arm);
            step = 1 / q_n(bi, arm);
            q_t(bi, arm) = q_t(bi, arm) + step*(reward - q_t(bi, arm));
        end

    end

    avg_reward_array(ti, :) = mean(all_rewards, 1);
    %percent_optimal_action(ei, :) = mean(pickedMaxAction, 1);
end

figure;
hold all;
for i = 1:(len_tau)
    plot(avg_reward_array(i, :), 'linewidth', 3);
end
legend('τ=0.01', 'τ=0.1', 'τ=0.5', 'τ=1');
xlabel('trials');
ylabel('reward');
ylim([0, 1.4])

% figure;
% plot(percent_optimal_action(1, :))
% hold on
% for i = 2:(len_e)
%     plot(percent_optimal_action(i, :))
% end
% legend('0', '0.01', '0.1')