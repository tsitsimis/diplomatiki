function a = epsilon_greedy(Q, s, epsilon)
    
    actionsSize = size(Q, 2);

    if rand < epsilon % exploration
        a = randi(actionsSize, 1);
    else % exploitation
        [~, inds] = max(Q(s, :));
        rndInd = randi(length(inds), 1);
        a = inds(rndInd);
        %[~, a] = max(Q(s, :));
    end
end