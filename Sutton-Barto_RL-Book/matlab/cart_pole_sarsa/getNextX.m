function x_prime = getNextX(x, action)
    dt = 0.01;
    
    g = 9.81;
    m = 0.1;
    M = 1;
    l = 1;

    k = 0;
    c = 0;
    
    F = 10 * action;
    
    x_prime = zeros(1, 4);
    x_prime(1) = x(1) + dt*x(2);
    x_prime(2) = x(2) + dt*(((c*x(4)/l - m*g*sin(x(3)))*cos(x(3)) + F - k*x(2) + m*l*sin(x(3))*(x(4))^2) / (M + m*(1 - (cos(x(3)))^2)));
    x_prime(3) = x(3) + dt*x(4);
    x_prime(4) = x(4) + dt*(((M + m)*(-c*x(4) + m*g*l*sin(x(3))) - (F - k*x(2) + m*l*sin(x(3))*(x(4))^2 )*m*l*cos(x(3))) / (m * l^2 * (M + m*(1 - (cos(x(3)))^2))));
end