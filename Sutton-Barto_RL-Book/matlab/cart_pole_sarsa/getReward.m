function [r, f] = getReward(x)
    p = x(1);
    theta = x(3);
    theta_dot = x(4);
    
    r = 10 - 5*abs(p) - 100*abs(theta)^2 - 10*theta_dot;
    f = false;
    
    if p < -4 || p > 4 || theta < deg2rad(-45) ||theta > deg2rad(45)
        r = -10000 - 50*abs(p) - 100*abs(theta);
        f = true; 
    end
    
end