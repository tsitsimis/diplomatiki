function states = getStates()
    x1 = [-1, 1];
    x2 = [-1, 0, 1];
    
    theta_max = 12;
    
    theta_step = (deg2rad(theta_max) - deg2rad(-theta_max)) / 20;
    x3 = deg2rad(-theta_max):theta_step:deg2rad(theta_max);
    
    x4 = [-1, 1];
    
    l1 = length(x1);
    l2 = length(x2);
    l3 = length(x3);
    l4 = length(x4);
    
    states = zeros(l1*l2*l3*l4, 4);
    i = 1;
    for i_x1 = 1:l1
        for i_x2 = 1:l2
            for i_x3 = 1:l3
                for i_x4 = 1:l4
                    states(i, 1) = x1(i_x1);
                    states(i, 2) = x2(i_x2);
                    states(i, 3) = x3(i_x3);
                    states(i, 4) = x4(i_x4);
                    i = i + 1;
                end
            end
        end
    end
end