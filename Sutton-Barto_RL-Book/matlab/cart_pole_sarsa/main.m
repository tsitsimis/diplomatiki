close all;

numOfEpisodes = 200;
numOfSteps = 1000;

alpha = 1;
gamma = 1.0;
epsilon = 0.001;

states = getStates();
numOfStates = size(states, 1);

actions = -1 : 0.1 : 1;
numOfActions = length(actions);

% Q = zeros(numOfStates, numOfActions);
load('Q');

%%%%%%%%%%%%%% init GUI %%%%%%%%%%%%%%
figure;
axis([-5, 5 -0.5 1.5]);
axis on;
axis equal;
hold on;
cartL = 2;
l = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ei = 1:numOfEpisodes
    
    % generate an episode
    theta = zeros(1, numOfSteps);
    cart_x = zeros(1, numOfSteps);
    
    steps = 0;
    total_reward = 0;

    x = [0, 0, pi/40, 0];
    s = x2s(x, states);
    a = epsilon_greedy(Q, s, epsilon);
    for si = 1:numOfSteps
        action = actions(a);
        x_prime = getNextX(x, action);
        [r, f] = getReward(x_prime);
        s_prime = x2s(x_prime, states);
        a_prime = epsilon_greedy(Q, s_prime, epsilon);
        
        Q(s, a) = Q(s, a) + alpha * (r + gamma * Q(s_prime, a_prime) - Q(s, a));
        
        x = x_prime;
        s = s_prime;
        a = a_prime;
        
        steps = steps + 1;
        total_reward = total_reward + r;
        
        if f == true
            break;
        end

        %%%%%%%%%%%%%%%%%%%%%%% plot %%%%%%%%%%%%%%%%%%%%%%%
        if ei == numOfEpisodes
            cla;
            plot([-5, 5], [0, 0], '-');

            cart_x(si) = x_prime(1);
            theta(si) = x_prime(3);

            % draw cart
            plot([cart_x(si) - cartL/2, cart_x(si) + cartL/2], [0, 0], 'r');     % ____
            plot([cart_x(si) - cartL/2, cart_x(si) + cartL/2], [0.2, 0.2], 'r'); % ----
            plot([cart_x(si) - cartL/2, cart_x(si) - cartL/2], [0, 0.2], 'r');   % |
            plot([cart_x(si) + cartL/2, cart_x(si) + cartL/2], [0, 0.2], 'r');   %     |
            
            % draw force
            if action > 0
                text(cart_x(si) + cartL/2 + 0.1, 0.1, ['\rightarrow ', num2str(10*action)]);
            else
                text(cart_x(si) - cartL/2 - 0.1, 0.1, [num2str(-10*action), ' \leftarrow'], 'HorizontalAlignment','right');
            end

            % draw pendulum
            plot(cart_x(si), 0.2, 'o');
            plot([cart_x(si), cart_x(si) + l*sin(theta(si))], [0.2, l*cos(theta(si))]);

            plot([cart_x(si), cart_x(si) + l*sin(deg2rad(45))], [0.2, l*cos(deg2rad(45))], '--b');
            plot([cart_x(si), cart_x(si) + l*sin(deg2rad(-45))], [0.2, l*cos(deg2rad(-45))], '--b');

            pause(0.0001);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    disp(['Episode: ', num2str(ei), ', Steps: ', num2str(steps), ', Total Reward: ', num2str(total_reward)]);
    
    epsilon = epsilon * 0.99;
    
end

save Q Q;