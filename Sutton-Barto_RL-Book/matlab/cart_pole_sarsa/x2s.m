function s = x2s(x, states)
    
    x(1) = sign(x(1));
    x(2) = sign(x(2));
    x(4) = sign(x(4));

    N1 = size(states, 1);
    norm = zeros(1, N1);
    for i = 1:N1
        states(i, :) = states(i, :) - x;
        norm(i) = sum(states(i, :) .^ 2);
    end
    
    [~, s] = min(norm);
end