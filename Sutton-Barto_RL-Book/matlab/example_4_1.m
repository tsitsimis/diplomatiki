pol = 1/4; % policy
gamma = 1;
r = -1; % reward

states = 0:15;
actions = ['u'; 'r'; 'd'; 'l']; % up, right, down, left

v = zeros(1, 16); % states-values
v_p = zeros(1, 16);

delta = 1e10;
epsilon = 0.01;

% while delta > epsilon
for k = 1:200
    %delta = 0;
    
    for si = 1:14 % for every state
        v_ = v_p(si+1);
        v_tmp = 0;
        for ai = 1:4
            action = actions(ai);
            sum = 0;
            for sj = 0:15
                sum = sum + p(sj, si, action) * (r + gamma * v_p(sj+1));
            end
            v_tmp = v_tmp + pol * sum;
        end
       
        v(si+1) = v_tmp;
        %delta = max([delta, abs(v_ - v(si))]);
    end
    v_p = v;
end

v = reshape(v, [4,4])';
v = round(v);
disp(v);