function states = getAccessibleStates(s_ball)
    states = zeros(1, 3);
    for i = 1:3
        states(i) = getState(i, s_ball);
    end
end