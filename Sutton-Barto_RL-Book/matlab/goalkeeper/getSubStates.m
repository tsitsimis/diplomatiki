function [s_keeper, s_ball] = getSubStates(s)
    if mod(s, 3) == 0
        s_ball = 3;
        s_keeper = fix(s / 3);
    else
        s_ball = mod(s, 3);
        s_keeper = fix(s / 3) + 1;
    end
end