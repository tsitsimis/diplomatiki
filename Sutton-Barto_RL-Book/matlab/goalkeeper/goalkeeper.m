close all;

keeperH = 3;
keeperW = 0.5;

arenaSize = 9;


% policy. Initially uniform
policy = (1/3) * ones(9, 3);
% policy = zeros(9, 3);
% for i = 1:3
%     policy(i, :) = [0, 1/2, 1/2];
% end
% for i = 4:6
%     policy(i, :) = [1/3, 1/3, 1/3];
% end
% for i = 7:9
%     policy(i, :) = [1/2, 0, 1/2];
% end

Q = zeros(9, 3);
Returns = zeros(36, 8);
epsilon = 0.1;

for k = 1:1000

% episode. terminate when saves = 20
saves = 0;
goals = 0;
    
s_keeper = 1;
state_action_pairs = [];
while saves < 20
    s_ball = randi(3, 1);
    s = getState(s_keeper, s_ball);

    accessibleStates = getAccessibleStates(s_ball);
    ind = sample_discrete([policy(s, 1), policy(s, 2), policy(s, 3)], 1);
    s = accessibleStates(ind);
    [s_keeper, s_ball] = getSubStates(s);
    
    state_action_pairs = [state_action_pairs; [s, ind]];
    
    if s_keeper == s_ball
        saves = saves + 1;
    else
        goals = goals + 1;
    end
end

disp(['saves: ', num2str(saves), ', goals: ', num2str(goals)]);

% calculate Q(s,a)
lenSteps = length(state_action_pairs);
for s = 1:9
    for a = 1:3
        ind = find(state_action_pairs(:,1) == s & state_action_pairs(:,2) == a);
        if ~isempty(ind)
            firstOccurence = ind(1); % first-visit MC
            steps = lenSteps - firstOccurence;
            
            r = -1;
            Returns(s, a) = steps * r;
            Returns(s, 4 + a) = Returns(s, 4 + a) + 1;
            Q(s, a) = Returns(s, a) / Returns(s, 4 + a);
        end
    end
end

% calculate policy
sizeA = 3;
for s = 1:36
    ind = find(state_action_pairs(:,1) == s);
    if ~isempty(ind)
        q_array = [];
        for i = 1:length(ind)
            a = state_action_pairs(ind(i), 2);
            q_array = [q_array, Q(s, a)];
        end
        [max_q, i_max] = max(q_array);
        a_star = state_action_pairs(ind(i_max), 2);

        for i = 1:3
            if i == a_star
                policy(s, i) = 1 - epsilon + epsilon/sizeA;
            else
                policy(s, i) = epsilon/sizeA;
            end
        end
    end
end

end

% plot
% figHandle = figure;
% hold on;
% axis on;
% axis equal;
% axis([0, arenaSize, 0, arenaSize]);
% 
% pos = [0, 3, 6];
% 
% for i = 1:100
%     cla;
%     
%     ind = randi(3, 1);
%     j = pos(ind);
%     plotKeeper(figHandle, j, keeperW, keeperH);
%     pause(0.5);
% end