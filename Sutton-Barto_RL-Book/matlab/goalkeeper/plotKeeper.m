function plotKeeper(figHandle, j, keeperW, keeperH)
    figure(figHandle);
    
    plot([0, 0], [j, j + keeperH], 'k');
    plot([keeperW, keeperW], [j, j + keeperH], 'k');
    plot([0, keeperW], [j, j], 'k');
    plot([0, keeperW], [j + keeperH, j + keeperH], 'k');
end