function [i, j] = num2point(num, N)
    if mod(num, N) == 0
        i = fix(num / N);
        j = N;
    else
        i = fix(num / N) + 1;
        j = mod(num, N);
    end
end