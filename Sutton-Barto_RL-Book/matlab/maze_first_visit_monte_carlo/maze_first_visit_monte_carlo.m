close all;

% v_walls = [3, 7, 8, 9, 10, 16, 19, 25, 28, 34];
% h_walls = [2, 5, 6, 9, 11, 13, 14, 15, 17, 21, 22, 26, 28];
v_walls = [3, 7, 8, 9, 10, 16, 19, 21, 22, 25, 28, 34];
h_walls = [2, 5, 9, 11, 13, 15, 17, 21, 26, 28];

Q = zeros(36, 4);
Returns = zeros(36, 8); % 4 cells for action-values and 4 for number of action occurencies

epsilon = 0.1;

% initialization to uniform policy
policy = zeros(36, 4);
for si = 1:36
    [actions, ~] = neighbours2(si, 6, h_walls, v_walls);
    sizeA = length(actions);
    for ai = 1:sizeA
        action = actions(ai);
        policy(si, action) = 1/sizeA;
    end
end

numOfEpisodes = 15;
for k = 1:numOfEpisodes

    % (a) generate an episode
    s = 1;
    state_action_pairs = [];
    while s ~= 36
        [actions, accessibleStates] = neighbours2(s, 6, h_walls, v_walls);
        actionPicked = sample_discrete([policy(s, 1), policy(s, 2), policy(s, 3), policy(s, 4)], 1);
        ind = find(actions == actionPicked);

        state_action_pairs = [state_action_pairs; [s, actionPicked]];

        s = accessibleStates(ind);
    end
    
    disp(['episode: ', num2str(k)]);

    % (b) calculate Q(s,a)
    lenSteps = length(state_action_pairs);
    for s = 1:36
        for a = 1:4
            ind = find(state_action_pairs(:,1) == s & state_action_pairs(:,2) == a);
            if ~isempty(ind)
                firstOccurence = ind(1); % first-visit MC
                steps = lenSteps - firstOccurence;

                r = getReturn(s, a);
                Returns(s, a) = steps * r;
                Returns(s, 4 + a) = Returns(s, 4 + a) + 1;
                Q(s, a) = Returns(s, a) / Returns(s, 4 + a);
            end
        end
    end

    % (c) calculate policy
    for s = 1:36
        ind = find(state_action_pairs(:,1) == s);
        if ~isempty(ind)
            q_array = [];
            for i = 1:length(ind)
                a = state_action_pairs(ind(i), 2);
                q_array = [q_array, Q(s, a)];
            end
            [max_q, i_max] = max(q_array);
            a_star = state_action_pairs(ind(i_max), 2);

            [actions, ~] = neighbours2(s, 6, h_walls, v_walls);
            sizeA = length(actions);
            for i = 1:4
                if policy(s, i) ~= 0
                    if i == a_star
                        policy(s, i) = 1 - epsilon + epsilon/sizeA;
                    else
                        policy(s, i) = epsilon/sizeA;
                    end
                end
            end
        end
    end


end

% init graphics
figHandler = figure;
axis on;
axis equal;
hold on;
axis([0, 6, 0, 6]);

% visualize the episode
for si = 1:size(state_action_pairs, 1)
    figure(figHandler);
    cla;
    initGraphics(figHandler, h_walls, v_walls);
    
    s = state_action_pairs(si, 1);
    [ii, jj] = num2point(s, 6);
    drawBall(figHandler, ii, jj);
    pause(0.5);
end