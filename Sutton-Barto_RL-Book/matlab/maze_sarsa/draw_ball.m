function draw_ball(figHandler, i, j)
    r = 0.2;

    figure(figHandler);
    
    x = j - 1 + 0.5 - r;
    y = 6 - i + 0.5 - r;

    rectangle('Position', [x y 2*r 2*r], ...
              'Curvature', [1,1], ...
              'EdgeColor', 'b', ...
              'FaceColor', 'b');
end