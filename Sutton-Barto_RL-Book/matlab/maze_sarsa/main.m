close all;

% maze
v_walls = [3, 7, 8, 9, 10, 16, 19, 21, 22, 25, 28, 34];
h_walls = [2, 5, 9, 11, 13, 15, 17, 21, 26, 28];

maze.v_walls = v_walls;
maze.h_walls = h_walls;

% SARSA parameters
epsilon = 0.1;
alpha = 1;
gamma = 1;

nStates = 36;
Start = 1;
Goal = 36;

% action-value function
Q = zeros(nStates, 4);

nEpisodes = 200;
route = []; % final route

h = waitbar(0, 'Running SARSA...');
for k = 1:nEpisodes
    % generate an episode
    S = Start;
    A = take_action(S, epsilon, Q, maze);
    
    while S ~= Goal
        if k == nEpisodes
            route = [route; S];
        end
        
        % Take action A, observe R, S'
        S_prime = next_state(S, A, maze);
        R = get_reward(S_prime);
        
        % Choose A' from S' using policy derived from Q (e.g., e-greedy)
        A_prime = take_action(S_prime, epsilon, Q, maze);
        
        % Update action-value function
        Q(S, A) = Q(S, A) + alpha * (R + gamma * Q(S_prime, A_prime) - Q(S, A));
        
        S = S_prime;
        A = A_prime;
    end
    waitbar(k / nEpisodes, h);
end
close(h);

route = [route; S];

fprintf('%d\n', length(route));

% plot maze and solution!
fig = figure;
axis off;
axis equal;
hold on;
axis([0, 6, 0, 6]);

for i = 1:size(route, 1)
    figure(fig);
    cla;
    plot_maze(fig, h_walls, v_walls);

    S = route(i);
    [ii, jj] = num2point(S, 6);
    draw_ball(fig, ii, jj);
    pause(0.2);
end
