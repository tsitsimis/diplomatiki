function [actions, n] = neighbours(k, sizeM, maze)
    if mod(k, sizeM) == 0
        i = fix(k / sizeM);
        j = sizeM;
    else
        i = fix(k / sizeM) + 1;
        j = mod(k, sizeM);
    end
    
    h_walls = maze.h_walls;
    v_walls = maze.v_walls;
    
    up_n = [i-1; j];
    right_n = [i; j+1];
    down_n = [i+1; j];
    left_n = [i; j-1];
    
    n_ = [];
    actions = [];
    if i > 1
        num = point2num(up_n, sizeM);
        if size(find(h_walls == num), 2) == 0
            n_ = [n_, up_n];
            actions = [actions; 1];
        end
    end
    if i < sizeM
        num = point2num([i,j], sizeM);
        if size(find(h_walls == num), 2) == 0
            n_ = [n_, down_n];
            actions = [actions; 3];
        end
    end
    if j > 1
        num = point2num(left_n, sizeM);
        if size(find(v_walls == num), 2) == 0
            n_ = [n_, left_n];
            actions = [actions; 4];
        end
    end
    if j < sizeM
        num = point2num([i,j], sizeM);
        if size(find(v_walls == num), 2) == 0
            n_ = [n_, right_n];
            actions = [actions; 2];
        end
    end
    
    n = zeros(1, size(n_, 2));
    for ii = 1:size(n_, 2)
        n(ii) = sizeM*(n_(1,ii) - 1) + n_(2,ii);
    end
    
end