function S_prime = next_state(S, A, maze)
    [actions, accessibleStates] = neighbours(S, 6, maze);
    S_prime = accessibleStates(actions == A);
end