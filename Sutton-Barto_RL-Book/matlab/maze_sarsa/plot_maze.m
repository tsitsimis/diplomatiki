function plot_maze(figHandler, h_walls, v_walls)
    figure(figHandler);
    
    % draw grid
    for i = 0:6
        plot([0, 6], [i, i], 'k'); % horizontal lines
        plot([i, i], [0, 6], 'k'); % vertical lines
    end
    
    % draw walls
    wallWidth = 3;
    for wi = 1:length(h_walls)
        wall = h_walls(wi);
        [i, j] = num2point(wall, 6);
        i = 6 - i;
        j = j - 1;
        plot([j, j + 1], [i, i], 'r', 'LineWidth', wallWidth);
    end
    
    for wi = 1:length(v_walls)
        wall = v_walls(wi);
        [i, j] = num2point(wall, 6);
        i = 6 - i;
        plot([j, j], [i, i + 1], 'r', 'LineWidth', wallWidth);
    end
end