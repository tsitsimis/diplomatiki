function action = take_action(S, epsilon, Q, maze)
    h_walls = maze.h_walls;
    v_walls = maze.v_walls;
    
    [actions, ~] = neighbours(S, 6, maze);
    if rand < epsilon   % exploration
        a_ind = randi(length(actions), 1);
    else                % exploitation
        q_list = zeros(1, length(actions));
        for i = 1:length(actions)
            q_list(i) = Q(S, actions(i));
        end
        inds = find(q_list == max(q_list));
        ind = randi(length(inds), 1);
        a_ind = inds(ind);
    end
    
    action = actions(a_ind);
end