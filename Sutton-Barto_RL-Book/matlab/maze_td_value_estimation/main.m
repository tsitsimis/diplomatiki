close all;

v_walls = [3, 7, 8, 9, 10, 16, 19, 21, 22, 25, 28, 34];
h_walls = [2, 5, 9, 11, 13, 15, 17, 21, 26, 28];

epsilon = 0.1;
alpha = 1;
gamma = 1;

startState = 1;
finalState = 36;

V = zeros(1, 36);

numOfEpisodes = 1000;
for k = 1:numOfEpisodes
    
    % generate an episode
    s = startState;
    state_sequence = [s];
    while s ~= finalState
        [actions, accessibleStates] = neighbours2(s, 6, h_walls, v_walls);

        if rand < epsilon % exploration
            a_ind = randi(length(actions), 1);
            a = actions(a_ind);
        else % exploitation
            v_list = zeros(1, length(actions));
            for vi = 1:length(actions)
                v_list(vi) = V(accessibleStates(vi));
            end
            inds = find(v_list == max(v_list));
            ind = randi(length(inds), 1);
            a_ind = inds(ind);
            a = actions(a_ind);
        end

        s_prime = accessibleStates(a_ind);
        R = getStateReward(s_prime);
        
        V(s) = V(s) + alpha * (R + gamma * V(s_prime) - V(s));
        
        s = s_prime;
        state_sequence = [state_sequence, s];
    end
    
    disp(['episode: ', num2str(k)]);
    
end

V_table = reshape(V, [6, 6])';
disp(round(V_table));

% init graphics
figHandler = figure;
axis on;
axis equal;
hold on;
axis([0, 6, 0, 6]);

% visualize the last episode
for si = 1:length(state_sequence)
    figure(figHandler);
    cla;
    initGraphics(figHandler, h_walls, v_walls);

    s = state_sequence(si);
    [ii, jj] = num2point(s, 6);
    drawBall(figHandler, ii, jj)
    pause(0.5);
end
