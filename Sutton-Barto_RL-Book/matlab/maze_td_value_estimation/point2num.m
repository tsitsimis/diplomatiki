function p = point2num(point, N)
    i = point(1);
    j = point(2);
    p = N*(i-1) + j;
end