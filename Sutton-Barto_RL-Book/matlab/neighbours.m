function n = neighbours(i,j, rows, cols)
    up_n = [i-1; j];
    right_n = [i; j+1];
    down_n = [i+1; j];
    left_n = [i; j-1];
    
    n = [];
    if i > 1 
        n = [n, up_n];
    end
    if i < rows
        n = [n, down_n];
    end
    if j > 1
        n = [n, left_n];
    end
    if j < cols
        n = [n, right_n];
    end
end