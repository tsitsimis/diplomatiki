function prob = p(s_prime, s, action)
    if strcmp(action, 'u')
        prob = (s_prime == s - 4);
        prob = prob || ((s_prime == s) && (s == 1 || s == 2 || s == 3));
    elseif strcmp(action, 'r')
        prob = (s_prime == s + 1) && (s ~= 3 && s ~= 7 && s ~= 11);
        prob = prob || ((s_prime == s) && (s == 3 || s == 7 || s == 11));
    elseif strcmp(action, 'd')
        prob = (s_prime == s + 4);
        prob = prob || ((s_prime == s) && (s == 12 || s == 13 || s == 14));
    elseif strcmp(action, 'l')
        prob = (s_prime == s - 1) && (s ~= 4 && s ~= 8 && s ~= 12);
        prob = prob || ((s_prime == s) && (s == 4 || s == 8 || s == 12));
    end
end