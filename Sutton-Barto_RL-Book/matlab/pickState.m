% epsilon-greedy policy

function s_prime = pickState(neighbours, v)
    epsilon = 0.1;
    
    sizeN = length(neighbours);
    
    vals = zeros(1, sizeN);
    for i = 1:sizeN
        vals(i) = v(neighbours(i));
    end
    
    if rand < epsilon % exploration
        randInd = randi(sizeN, 1);
        s_prime = neighbours(randInd);
    else % exploitation
        maxInds = find(vals == max(vals));
        randInd = randi(length(maxInds), 1);
        maxInd = maxInds(randInd);
        s_prime = neighbours(maxInd);
    end
end