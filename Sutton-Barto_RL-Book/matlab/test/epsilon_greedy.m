function a = epsilon_greedy(Q, s, epsilon)
    
    n_actions = size(Q, 2);

    if rand < epsilon % exploration
        a = randi(n_actions, 1);
    else % exploitation
        [~, inds] = max(Q(s, :));
        rndInd = randi(length(inds), 1);
        a = inds(rndInd);
        %[~, a] = max(Q(s, :));
    end
end