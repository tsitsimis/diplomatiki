function s_prime = getNextS(s, a, states, theta_max)    
    state = states(s, :);
    x = state(1);
    theta = state(2);
    
    if (theta == theta_max && a == 1) || (theta == -theta_max && a == -1)
        s_prime = s;
    else
        %state_prime = [x, theta + a];
        s_prime = find(states(:, 1) == x & states(:, 2) == theta + a);
    end
end