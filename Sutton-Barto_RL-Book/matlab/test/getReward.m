function r = getReward(s, H, states)
    state = states(s, :);
    x = state(1);
    theta = state(2);
    
    human_cart_theta = atan2(x, H);
    
    r = -abs(theta - human_cart_theta);
end