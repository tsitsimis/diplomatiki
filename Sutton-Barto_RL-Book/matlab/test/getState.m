function s = getState(x, theta, states)
    s = find(states(:, 1) == x & states(:, 2) == theta);
end