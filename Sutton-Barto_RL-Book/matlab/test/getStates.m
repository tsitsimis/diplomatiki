function states = getStates(L, x_step, theta_max, theta_step)
    x = -L : x_step : L;
    x_size = length(x);
    
    theta = -theta_max : theta_step : theta_max;
    theta_size = length(theta);

    states = zeros(x_size * theta_size, 2);
    si = 1;
    for xi = 1:x_size
        for ti = 1:theta_size
            states(si, 1) = x(xi);
            states(si, 2) = theta(ti);
            si = si + 1;
        end
    end
end