close all;

L = 5;
H = 4;
x_step = 0.5;
theta_max = 45;
theta_step = 1;

states = getStates(L, x_step, theta_max, theta_step);
n_states = size(states, 1);

actions = [-1, 1];
n_actions = length(actions);

alpha = 1;
gamma = 1.0;
epsilon = 0.001;

episodeSteps = 100;
numOfEpisodes = 300;

Q = zeros(n_states, n_actions);

figure;
axis([-L, L -(H + 3) 2]);
axis on;
axis equal;
hold on;

for ei = 1:numOfEpisodes

    % generate an episode
    total_reward = 0;
    
    x = 0;
    theta = -20;
    s = getState(x, theta, states);
    a = epsilon_greedy(Q, s, epsilon);

    goingRight = 1;
    for i = 1:episodeSteps
        if x < L && goingRight == 1
            x_prime = x + x_step;
        elseif x == L && goingRight == 1
            goingRight = 0;
            x_prime = x - x_step;
        elseif x > -L && goingRight == 0
            x_prime = x - x_step;
        elseif x == -L && goingRight == 0
            goingRight = 1;
            x_prime = x + x_step;
        end

        s_prime = getNextS(s, a, states, theta_max);
%         aa = states(s_prime, :);
%         disp([num2str(aa(2))]);
        r = getReward(s_prime, H, states);
        a_prime = epsilon_greedy(Q, s_prime, epsilon);

        Q(s, a) = Q(s, a) + alpha * (r + gamma * Q(s_prime, a_prime) - Q(s, a));

        x = x_prime;
        s = s_prime;
        a = a_prime;
        
        total_reward = total_reward + r;

        if ei == numOfEpisodes
            cla;
            plot([-L, L], [0, 0], '-');

            %plot cart
            plot(x, 0, 'ro');

            %plot human
            plot(0, -H, 'bo');
            current_state = states(s, :);
            theta_human = current_state(2);
            theta_human = deg2rad(theta_human);
            human = 1;
            plot([0 human*sin(theta_human)], [-H -H + human*cos(theta_human)], 'b');
        end

        pause(0.1);
    end

    disp(['Episode: ', num2str(ei), ', Total Reward: ', num2str(total_reward)]);
    epsilon = epsilon * 0.99;

end

