/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.zenor25;

import java.util.Map;
import org.robokind.api.animation.messaging.RemoteAnimationPlayerClient;
import org.robokind.api.common.position.NormalizedDouble;
import org.robokind.api.motion.Joint;
import org.robokind.api.motion.Robot.JointId;
import org.robokind.api.motion.Robot.RobotPositionHashMap;
import org.robokind.api.motion.Robot.RobotPositionMap;
import org.robokind.api.motion.messaging.RemoteRobot;
import org.robokind.api.speech.messaging.RemoteSpeechServiceClient;
import org.robokind.client.basic.Robokind;
import org.robokind.client.basic.RobotJoints;
import org.robokind.client.basic.UserSettings;

/**
 *
 * @author Theodore
 */
public class Demo {
    private static final String ROBOT_IP = "192.168.1.13";
    private static final String ROBOT_ID = "myRobot";

    private static RemoteRobot myRobot;
    private static RemoteAnimationPlayerClient myPlayer;
    private static RemoteSpeechServiceClient mySpeaker;
    private static RobotPositionMap goalPositions;
    
    public static void main(String[] args) {
        // Connect to robot/avatar
        
        // Uncomment to connect to real robot
        //SetSettings(ROBOT_ID, ROBOT_IP);
        
        myRobot = Robokind.connectRobot();
        myPlayer = Robokind.connectAnimationPlayer();
        mySpeaker = Robokind.connectSpeechService();
        
        // Define body parts
        JointId leftShoulderRoll = getJoint(RobotJoints.LEFT_SHOULDER_ROLL);
        JointId leftShoulderPitch = getJoint(RobotJoints.LEFT_SHOULDER_PITCH);
        JointId leftHandGrasp = getJoint(RobotJoints.LEFT_HAND_GRASP);
        JointId leftWristYaw = getJoint(RobotJoints.LEFT_WRIST_YAW);
        JointId leftKneePitch = getJoint(RobotJoints.LEFT_KNEE_PITCH);
        JointId rightAnklePitch = getJoint(RobotJoints.RIGHT_ANKLE_PITCH);
        JointId neckYaw = getJoint(RobotJoints.NECK_YAW);
        JointId waist = getJoint(RobotJoints.WAIST);

        // Move to default joints positions
        goalPositions = myRobot.getDefaultPositions();
        for(Map.Entry<JointId, NormalizedDouble> entry : goalPositions.entrySet()) {
            // print default joit values
            System.out.println(entry.getKey().toString() + ": " + entry.getValue().toString());
        }
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(2000);
        
        // Move robot
        // left shoulder
        goalPositions = new RobotPositionHashMap();
        goalPositions.put(leftShoulderPitch, new NormalizedDouble(0.5));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // left wrist
        goalPositions.clear();
        goalPositions.put(leftWristYaw, new NormalizedDouble(1.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // grasp -- NOT WORKING
        goalPositions.clear();
        goalPositions.put(leftHandGrasp, new NormalizedDouble(1.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // left leg -- NOT WORKING
        goalPositions.clear();
        goalPositions.put(leftKneePitch, new NormalizedDouble(1.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // Face expressions
        Expression expression = new Expression();
        expression.setExpressionJoints(myRobot);
        expression.gentleLookDown(400, myRobot);
        Robokind.sleep(1000);
        expression.smile(200, myRobot);
        Robokind.sleep(1000);
        
        // turn waist
        goalPositions.clear();
        goalPositions.put(waist, new NormalizedDouble(1.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // turn neck
        goalPositions.clear();
        goalPositions.put(neckYaw, new NormalizedDouble(0.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        goalPositions.clear();
        goalPositions.put(neckYaw, new NormalizedDouble(1.0));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        goalPositions.clear();
        goalPositions.put(neckYaw, new NormalizedDouble(0.5));
        myRobot.move(goalPositions, 1000);
        Robokind.sleep(1000);
        
        // Move to default joints positions
        goalPositions = myRobot.getDefaultPositions();
        myRobot.move(goalPositions, 1000);
        
        // Disconnect
        Robokind.disconnect();
        System.exit(0);
    }
    
    private static void SetSettings(String robotID, String robotIP) {
        UserSettings.setRobotId(robotID);
        UserSettings.setRobotAddress(robotIP);
        UserSettings.setAnimationAddress(robotIP);
        UserSettings.setSpeechAddress(robotIP);
        UserSettings.setSensorAddress(robotIP);
        UserSettings.setAccelerometerAddress(robotIP);
        UserSettings.setGyroscopeAddress(robotIP);
        UserSettings.setCompassAddress(robotIP);
        UserSettings.setCameraAddress(robotIP);
        UserSettings.setImageRegionAddress(robotIP);
        UserSettings.setImageRegionId("0");
    }
    
    private static JointId getJoint(int id) {
        return new JointId(myRobot.getRobotId(), new Joint.Id(id));
    }
    
}
