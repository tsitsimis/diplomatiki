@echo OFF

set INSTALL_DIR=%~dp0
set APPDATA_DIR=%LOCALAPPDATA%\RoboKind\R25Avatar\1.0.0\
set CONFIG_PATH=%INSTALL_DIR%config.ini
for /f "usebackq tokens=1,2 delims==" %%a in ( "%CONFIG_PATH%" ) do (
	if %%a==JAVA_EXEC set JAVA_EXEC=%%b
)

set RUN_PROPS=file:%INSTALL_DIR%run.properties
set LOG4J_PROPS=file:%INSTALL_DIR%log4j.properties
set LOG4J_OUTPUT_PATH=%APPDATA_DIR%rkbots_opengl_R25.log
set BROKER_HOME=%INSTALL_DIR%qpid_broker_home\\
set BROKER_WORK=%APPDATA_DIR%qpid_broker_work\\
set BROKER_ICNF=rk_qbrk\\rkq-init-conf.json
set FELIX_JAR=%INSTALL_DIR%felix.jar
set FELIX_CACHE=%INSTALL_DIR%felix-cache
set SAPI_DIR=%INSTALL_DIR%mssapi_qpid-0.34_20160928

cd "%SAPI_DIR%"
start "SAPIServer" /B SAPIServer.exe

cd "%INSTALL_DIR%

"%JAVA_EXEC%\\bin\\java" -splash:rwflip.png -Dfelix.config.properties="%RUN_PROPS%" -Dlog4j.configuration="%LOG4J_PROPS%" -Drkbots.logfile.path="%LOG4J_OUTPUT_PATH%" -Dqpid.broker.home="%BROKER_HOME%" -Dqpid.broker.work="%BROKER_WORK%" -Dqpid.broker.icnf="%BROKER_ICNF%" -jar "%FELIX_JAR%" "%FELIX_CACHE%"