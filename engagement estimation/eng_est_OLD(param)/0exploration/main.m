%% main

%clear all
close all
%clc

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 200;
vertiBars = false;

optimalParam1 = 5;
optimalParam2 = 10;

DATA2 = [];
for ee = 1:1
    
% define Task
BBT = BBsetTask();
BBT.engMu = optimalParam1;

% define baby robot
BBR = BBrobot(BBT);

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
LOG_FILES = [s a.action a.param 0 s a.action a.param BBT.cENG BBR.delta BBR.VC BBR.ACT BBR.PA...
    zeros(1, BBR.nA) zeros(1, BBR.nA) 0 BBR.varDelta BBR.sigma BBR.varACT BBR.kalmanV...
    diag(BBR.kalmanCOV)' BBR.sigma2Q(1) 0 0 0 ...
    BBT.cEST_ENG];

%% RUN TASK
for iii=1:episodeLength-1
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
        logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
        logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
        logs.estEngagement]];
end

BBT.engMu = optimalParam2;
for iii=1:episodeLength
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
        logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
        logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
        logs.estEngagement]];
end

DATA2 = [DATA2; LOG_FILES(:, 16)'];
end

% LOG_FILES
% 1-5 s / 6 old action / 7 old param / 8 r / 9-13 y / 14 action / 15 param
% 16 eng / 17 delta / 18 V / 19-24 ACT / 25-30 PA / 31-36 deltaACT
% 37-42 Q / 43 RPEQ / 44 varCriticDelta / 45 sigma / 46-51 varwA
% 52-57 kalmanV / 58-63 kalmanCOV / 64 sigma2Q / 65-66 star-ltar / 67 meta
% 68 est eng

%% plots
if true
figEng = figure;
hold all;
box on;
h1 = plot(LOG_FILES(:, 16), 'linewidth', 1, 'LineStyle', '-', 'color', 'black');
h2 = plot(LOG_FILES(:, 68), 'linewidth', 1, 'LineStyle', '--', 'color', 'black');
legend([h1, h2], {'real', 'estimated'});
xlabel('trials');
title('engagement', 'fontWeight','normal');

figParam = figure;
hold all;
box on;
% executed param
h1 = plot(LOG_FILES(:, 15), 'LineWidth', 1, 'LineStyle', '-', 'color', 'black');
% optimal param
%h2 = plot([1, size(LOG_FILES, 1)], [BBT.engMu, BBT.engMu], '--',...
%    'LineWidth', 1, 'LineStyle', '--', 'color', 'red');
h2 = plot(1:size(LOG_FILES, 1), [ones(1, episodeLength)*optimalParam1, ones(1, episodeLength)*optimalParam2],...
   '--', 'LineWidth', 1, 'LineStyle', '--', 'color', 'red');
axis([1, size(LOG_FILES, 1), min(BBT.intervalle)-1, max(BBT.intervalle) + 1]);
xlabel('trials');
title('action parameter', 'fontWeight','normal');
legend([h1, h2], {'executed param', 'optimal param'});

figure;
hold all;
box on;
h1 = plot(LOG_FILES(:, 45), 'linewidth', 2);
xlabel('trials');
title('exploration \sigma', 'fontWeight','normal');

else
gray = [173, 175, 178]/255;
mData = mean(DATA2, 1);
filtSize = 10;
mData = medfilt1(mData, filtSize);
n = size(DATA2, 2);
stdUpData = zeros(1, n);
for i = 1:n
    ind = find(DATA2(:, i) >= mData(i));
    stdUpData(i) = sqrt(sum((DATA2(ind, i) - mData(i)) .^ 2) / n);
end
stdUpData = medfilt1(stdUpData, filtSize);

stdDownData = zeros(1, n);
for i = 1:n
    ind = find(DATA2(:, i) < mData(i));
    stdDownData(i) = sqrt(sum((DATA2(ind, i) - mData(i)) .^ 2) / n);
end
stdDownData = medfilt1(stdDownData, filtSize);

up = mData + stdUpData;
down = mData - stdDownData;
t = 1:size(LOG_FILES, 1);
X = [t, fliplr(t)];
Y = [up, fliplr(down)];

figure;
hold all;
grid on;
box on;
h_fill = fill(X, Y, gray);
h_mean = plot(mData, 'LineWidth', 2, 'LineStyle', '-', 'color', 'blue');
plot(up, 'LineWidth', 1, 'LineStyle', '-', 'color', 'k');
plot(down, 'LineWidth', 1, 'LineStyle', '-', 'color', 'k');
legend([h_mean, h_fill], {'mean engagement', 'variance'});
xlabel('trials');
title('engagement', 'fontWeight','normal'); 
end


