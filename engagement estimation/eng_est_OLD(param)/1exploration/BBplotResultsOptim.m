RES_DIR = 'resultsOptiSummer2016/';
%RES_DIR = 'resultsPostEWRL2016/';

whichModel = 3;
nbPaquet = 9;
whichCol = 13;

%% paquets par modeles
% model 1; 3 paquets (5 with 1 sample)
% model 2; 5 paquets
% model 3; 9 paquets (11 with 1 sample)
% model 4; 1 paquets
% model 5; 1 paquet: ter (OLD, preEWRL, 3 paquets: 0, bis, nov)
% model 6; 3 paquets

%% REDO
% ALREADY LAUNCHED: model 1; alphaQ impair et gamma impair (1qua) ou gamma pair (1qui)
% ALREADY LAUNCHED: model 2; varObs 0.02 (sep); gainSigma 850 (six)
% ALREADY LAUNCHED: model 3dix / 3onz with gainSigma 25K and 30K
% ALREADY LAUNCHED: model 6; mu et rateSigma impair (6qua et 6qui)

%% parametres a chercher
% model 3; � faire gainSigma > 20000 et exploreBonus > 20 (is 25 enough?)
% model 4: gainSigma around 20; tau2 around 10; tau1 < 10
% model 5; �ventuellement (?) : tau1 5; tau2 5; mu 0.05; beta 0.5; 

if ((whichModel == 1)||(whichModel == 3))
    load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) '_nbBlock-5_episodeLength-1000_gridSearch.mat'])
    tabResults2 = tabResults;
    iii = 2;
    while (iii <= nbPaquet)
        switch(iii)
            case 2
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'bis_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 3
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'ter_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 4
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'qua_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 5
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'qui_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 6
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'six_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 7
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'sep_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 8
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'oct_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 9
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'nov_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 10
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'dix_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            case 11
                load([RES_DIR 'BBopti_mixedRwd_model-' num2str(whichModel) 'onz_nbBlock-5_episodeLength-1000_gridSearch.mat'])
        end
        tabResults2 = [tabResults2 ; tabResults];
        iii = iii + 1;
    end
else % models 2, 4, 5, 6
    tabResults2 = [];
    for alphaA = 0.1:0.1:0.6
        for alphaC = 0.1:0.1:0.5
            %load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaA) '_alphaC-' num2str(alphaC) '_model-' num2str(whichModel) '_nbBlock-5_episodeLength-1000_gridSearch.mat'])
            %tabResults2 = tabResults;
            iii = 1;
            while (iii <= nbPaquet)
                switch(iii)
                    case 1
                        switch(whichModel)
                            case 6
                                load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) '_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                            otherwise
                                load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) 'ter_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                        end
                    case 2
                        load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) 'bis_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                    case 3
                        switch(whichModel)
                            case 2
                                load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) 'nov_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                            otherwise
                                load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaA) '_alphaC-' num2str(alphaC) '_model-' num2str(whichModel) 'nov_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                        end
                    case 4
                        load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) 'qua_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                    case 5
                        load([RES_DIR 'BBopti_mixedRwd_alphaA-' num2str(alphaC) '_alphaC-' num2str(alphaA) '_model-' num2str(whichModel) 'qui_nbBlock-5_episodeLength-1000_gridSearch.mat'])
                end
                tabResults2 = [tabResults2 ; tabResults];
                iii = iii + 1;
            end
        end
    end
end
tabResults = tabResults2;
clear tabResults2;

%%%%%%%%%%%%%%%%%%%%%%%
%% BEST PERFORMANCE
%%%%%%%%%%%%%%%%%%%%%%%
theBestI = argmax(tabResults(:,whichCol));
theBest = tabResults(theBestI,:);
num2str(theBest)

%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURE
%%%%%%%%%%%%%%%%%%%%%%%
switch (whichModel)
    case {1,3}
        nbLigne = 3;
    otherwise
        nbLigne = 4;
end

close all
figure

%% BETA x GAMMA
switch (whichModel)
    case {4}
        indexB = 9;
    otherwise
        indexB = 1;
end
lesBetas = unique(tabResults(:,indexB))';
lesGammas = unique(tabResults(:,2))';
mabg = zeros(length(lesBetas),length(lesGammas));
mabgstd = zeros(length(lesBetas),length(lesGammas));
for b=1:length(lesBetas)
    for g=1:length(lesGammas)
        [boubou, indeks] = max(tabResults((tabResults(:,indexB)==lesBetas(b))&(tabResults(:,2)==lesGammas(g)),whichCol));
        mabg(b,g) = boubou;
        mabgstd(b,g) = tabResults(indeks,whichCol+1);
    end
end

subplot(nbLigne,2,1)
switch (whichModel)
    case {4}
        imagesc(lesBetas,lesGammas,mabg')
        xlabel('mu')
        ylabel('gamma')
    otherwise
        imagesc(1:length(lesBetas),lesGammas,mabg')
        xlabel('beta')
        ylabel('gamma')
end
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
title('mean reward')
subplot(nbLigne,2,2)
switch (whichModel)
    case {4}
        imagesc(lesBetas,lesGammas,mabgstd')
        xlabel('mu')
    otherwise
        imagesc(1:length(lesBetas),lesGammas,mabgstd')
        xlabel('beta')
end
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
title('std reward')

%% ALPHAC x ALPHAA
lesAC = unique(tabResults(:,3))';
lesAA = unique(tabResults(:,4))';
mabg = zeros(length(lesAC),length(lesAA));
mabgstd = zeros(length(lesAC),length(lesAA));
for b=1:length(lesAC)
    for g=1:length(lesAA)
        [boubou, indeks] = max(tabResults((tabResults(:,3)==lesAC(b))&(tabResults(:,4)==lesAA(g)),whichCol));
        mabg(b,g) = boubou;
        mabgstd(b,g) = tabResults(indeks,whichCol+1);
    end
end

subplot(nbLigne,2,3)
imagesc(lesAC,lesAA,mabg')
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
xlabel('alphaC')
ylabel('alphaA')
subplot(nbLigne,2,4)
imagesc(lesAC,lesAA,mabgstd')
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
xlabel('alphaC')

%% SIGMA x ALPHAQ
switch (whichModel)
    case {5,6}
        indexAQ = 8;
    otherwise
        indexAQ = 6;
end
lesSI = unique(tabResults(:,5))';
lesAQ = unique(tabResults(:,indexAQ))';
mabg = zeros(length(lesSI),length(lesAQ));
mabgstd = zeros(length(lesSI),length(lesAQ));
for b=1:length(lesSI)
    for g=1:length(lesAQ)
        [boubou, indeks] = max(tabResults((tabResults(:,5)==lesSI(b))&(tabResults(:,indexAQ)==lesAQ(g)),whichCol));
        mabg(b,g) = boubou;
        mabgstd(b,g) = tabResults(indeks,whichCol+1);
    end
end

subplot(nbLigne,2,5)
imagesc(lesSI,lesAQ,mabg')
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
switch (whichModel)
    case 1
        xlabel('sigma')
        ylabel('alphaQ')
    case {2,3,4}
        xlabel('gainSigma')
        ylabel('explor bonus')
    case {5,6}
        xlabel('rateSigma')
        ylabel('mu')
end
subplot(nbLigne,2,6)
imagesc(lesSI,lesAQ,mabgstd')
axis square
set(gca,'YDir','normal') % ,'XTick',[]
colorbar
switch (whichModel)
    case 1
        xlabel('sigma')
    case {2,3,4}
        xlabel('gainSigma')
    case {5,6}
        xlabel('rateSigma')
end

%% TAU1 x TAU2
if ((whichModel == 2)||(whichModel >= 4))
    switch (whichModel)
        case {5,6}
            indexT1 = 6;
            indexT2 = 7;
        case 4
            indexT1 = 7;
            indexT2 = 8;
        otherwise
            indexT1 = 9;
            indexT2 = 7;
    end
    lesT1 = unique(tabResults(:,indexT1))';
    lesT2 = unique(tabResults(:,indexT2))';
    mabg = zeros(length(lesT1),length(lesT2));
    mabgstd = zeros(length(lesT1),length(lesT2));
    for b=1:length(lesT1)
        for g=1:length(lesT2)
            [boubou, indeks] = max(tabResults((tabResults(:,indexT1)==lesT1(b))&(tabResults(:,indexT2)==lesT2(g)),whichCol));
            mabg(b,g) = boubou;
            mabgstd(b,g) = tabResults(indeks,whichCol+1);
        end
    end

    subplot(nbLigne,2,7)
    imagesc(lesT1,lesT2,mabg')
    axis square
    set(gca,'YDir','normal') % ,'XTick',[]
    colorbar
    switch (whichModel)
        case 2
            xlabel('varObs')
            ylabel('eta')
        case {4,5,6}
            xlabel('tau1')
            ylabel('tau2')
    end
    subplot(nbLigne,2,8)
    imagesc(lesT1,lesT2,mabgstd')
    axis square
    set(gca,'YDir','normal') % ,'XTick',[]
    colorbar
    switch (whichModel)
        case 2
            xlabel('varObs')
        case {4,5,6}
            xlabel('tau1')
    end
end

% %%%%%%%%%%%
% %% FIGURE 2 : child engagement
% 
% figure
% verti = 1000;
% while (verti <= 4999)
%     plot([verti verti], [0 10], '--k', 'LineWidth', 2)
%     hold on
%     verti = verti + episodeLength;
% end
% 
% 
% switch (whichModel)
%     case 1
%         listeData = [44 55:63];
%     case 2
%         listeData = 95:104; %70:79;
%     case 5
%         listeData = 45:54;
%     otherwise
%         listeData = [];
% end
% 
% engagement = zeros(5001,size(listeData,2));
% compteur = 1;
% for iii = listeData
%     iii
%     load(['postOptimResults_sept2016/experiment' num2str(iii) '_model' num2str(whichModel) '.mat'])
%     engagement(:,compteur) = LOG_FILES(:,16);
%     compteur = compteur + 1;
% end
% 
% mean(mean(engagement))
% 
% %errorfill(1:5001,smooth(mean(engagement'),'sgolay')',smooth(std(engagement')./sqrt(10),'sgolay')','b')
% errorfill(1:5001,mean(engagement'),std(engagement')./sqrt(size(engagement,2)),'r')
% 
% axis([0 5001 0 10.2])
% xlabel('blue: model 1; green: model 2; red: model 5')
% ylabel('simulated engagement')
% alpha 0.5
