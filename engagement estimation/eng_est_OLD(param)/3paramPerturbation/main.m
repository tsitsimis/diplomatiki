%% main

%clear all
%close all
%clc

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 100;
perturbationLength = 1;%10;
vertiBars = false;

ENG_DATA = [];
PARAM_DATA = [];
for ee = 1:50

% define Task
BBT = BBsetTask();

% define baby robot
BBR = BBrobot(BBT);

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
LOG_FILES = [s a.action a.param 0 s a.action a.param BBT.cENG BBR.delta BBR.VC BBR.ACT BBR.PA...
    zeros(1, BBR.nA) zeros(1, BBR.nA) 0 BBR.varDelta BBR.sigma BBR.varACT BBR.kalmanV...
    diag(BBR.kalmanCOV)' BBR.sigma2Q(1) 0 0 0 ...
    BBT.cEST_ENG 0 0];

%% RUN TASK
for iii=1:episodeLength-1
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
        logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
        logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
        logs.estEngagement logs.estSigma logs.yawSigma]];
end

BBT.engMu = BBT.optimalParam2;
for iii=1:perturbationLength
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
        logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
        logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
        logs.estEngagement logs.estSigma logs.yawSigma]];
end

BBT.engMu = BBT.optimalParam1;
for iii=1:(2*episodeLength - perturbationLength)
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
        logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
        logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
        logs.estEngagement logs.estSigma logs.yawSigma]];
end

ENG_DATA = [ENG_DATA; LOG_FILES(:, 16)'];
PARAM_DATA = [PARAM_DATA; LOG_FILES(:, 15)'];
end

%% statistics
engWindow = LOG_FILES(100:150, 16);
engMAD = sum(abs(engWindow - BBT.maxENG)) / length(engWindow);

paramWindow = LOG_FILES(100:150, 15);
paramMAD = sum(abs(paramWindow - BBT.optimalParam1)) / length(paramWindow);

maxEngAD = max(abs(engWindow - BBT.maxENG));

numTrials90 = find((engWindow - 0.9*BBT.maxENG) <= 0.1);
if length(numTrials90) >= 1
    numTrials90 = numTrials90(end);
else
    numTrials90 = 0;
end

stats = [engMAD, paramMAD, maxEngAD, numTrials90];

% break;

% LOG_FILES
% 1-5 s / 6 old action / 7 old param / 8 r / 9-13 y / 14 action / 15 param
% 16 eng / 17 delta / 18 V / 19-24 ACT / 25-30 PA / 31-36 deltaACT
% 37-42 Q / 43 RPEQ / 44 varCriticDelta / 45 sigma / 46-51 varwA
% 52-57 kalmanV / 58-63 kalmanCOV / 64 sigma2Q / 65-66 star-ltar / 67 meta
% 68 est eng / 69 est gaze sigma / 70 real gaze sigma

%% plots
if false
    figEng = figure;
    hold all;
    box on;
    h1 = plot(LOG_FILES(:, 16), 'linewidth', 1, 'LineStyle', '-', 'color', 'black');
    %h2 = plot(LOG_FILES(:, 68), 'linewidth', 1, 'LineStyle', '--', 'color', 'black');
    %legend([h1, h2], {'real', 'estimated'});
    xlabel('trials');
    title('engagement', 'fontWeight','normal');

    % figMSD = figure;
    % hold all;
    % box on;
    % h1 = plot(LOG_FILES(:, 70), 'linewidth', 1, 'LineStyle', '-', 'color', 'black', 'marker', 'none');
    % h2 = plot(LOG_FILES(:, 69), 'linewidth', 1, 'LineStyle', '--', 'color', 'black', 'marker', 'none');
    % xlabel('trials');
    % title('gaze std', 'fontWeight','normal');
    % legend([h1, h2], {'real', 'estimated'});

    figParam = figure;
    hold all;
    box on;
    % executed param
    h1 = plot(LOG_FILES(:, 15), 'LineWidth', 1, 'LineStyle', '-', 'color', 'black');
    % optimal param
    h2 = plot(1:size(LOG_FILES, 1), [ones(1, episodeLength)*BBT.optimalParam1,...
        ones(1, perturbationLength)*BBT.optimalParam2, ones(1, episodeLength)*BBT.optimalParam1],...
        '--', 'LineWidth', 1, 'LineStyle', '--', 'color', 'red');
    axis([1, size(LOG_FILES, 1), min(BBT.intervalle)-1, max(BBT.intervalle) + 1]);
    xlabel('trials');
    title('action parameter', 'fontWeight','normal');
    legend([h1, h2], {'executed param', 'optimal param'});

    % figure;
    % hold all;
    % box on;
    % h1 = plot(LOG_FILES(:, 45), 'linewidth', 2);
    % xlabel('trials');
    % title('exploration \sigma');

else
    gray = [173, 175, 178]/255;

    % avg engagement
    engData = mean(ENG_DATA, 1);
    n = size(ENG_DATA, 2);
    stdUpData = zeros(1, n);
    for i = 1:n
        ind = find(ENG_DATA(:, i) >= engData(i));
        stdUpData(i) = sqrt(sum((ENG_DATA(ind, i) - engData(i)) .^ 2) / n);
    end

    stdDownData = zeros(1, n);
    for i = 1:n
        ind = find(ENG_DATA(:, i) < engData(i));
        stdDownData(i) = sqrt(sum((ENG_DATA(ind, i) - engData(i)) .^ 2) / n);
    end

    up = engData + stdUpData;
    down = engData - stdDownData;
    t = 1:size(LOG_FILES, 1);
    X = [t, fliplr(t)];
    Y = [up, fliplr(down)];

    engFig = figure;
    hold all;
    %grid on;
    box on;
    h_fill = fill(X, Y, gray);
    h_mean = plot(engData, 'LineWidth', 2, 'LineStyle', '-', 'color', 'blue');
    %leg1 = legend([h_mean, h_fill], {'engagement', 'variance'});
    %set(leg1, 'Orientation', 'horizontal', 'Location', 'northoutside');
    xlim([1, size(LOG_FILES, 1)]);
    xlabel('number of trials');
    ylabel('engagement');
    title(['perturbation duration = ', num2str(perturbationLength), ' trials'],...
        'fontWeight','normal');
    
%     axes('Parent', engFig, 'XTickLabel', {'98','100','102','104','106'},...
%         'YTick', [9, 9.5, 10], 'YTickLabel', {'9','9.5','10'},...
%         'Position', [0.45 0.2 0.4 0.4]);
%     hold all;
%     box on;
%     t_zoom = 1:10;
%     X_zoom = [t_zoom, fliplr(t_zoom)];
%     Y_zoom = [up(98:107), fliplr(down(98:107))];
%     fill(X_zoom, Y_zoom, gray);
%     plot(engData(98:107), 'LineWidth', 2, 'Marker', 'o', 'LineStyle', '-', 'color', 'blue');
%     xlim([min(t_zoom), max(t_zoom)]);
%     ylim([min(engData(98:107))-0.2, 10.2]);
    

    % avg param
    paramData = mean(PARAM_DATA, 1);
    stdData = std(PARAM_DATA);
    up = paramData + stdData/2;
    down = paramData - stdData/2;
    Y = [up, fliplr(down)];

    paramFig = figure;
    hold all;
    box on;
    xlim([0, size(LOG_FILES, 1)]);
    h_fill = fill(X, Y, gray);
    h_mean = plot(paramData, 'LineWidth', 2, 'LineStyle', '-', 'color', 'red');
    xlim([1, size(LOG_FILES, 1)]);
    xlabel('number of trials');
    ylabel('action parameter');
    title(['perturbation duration = ', num2str(perturbationLength), ' trials'], 'fontWeight','normal');
    
    
%     axes('Parent', paramFig, 'Position', [0.45 0.2 0.4 0.4]);
%     hold all;
%     box on;
%     t_zoom = 1:20;
%     X_zoom = [t_zoom, fliplr(t_zoom)];
%     Y_zoom = [up(95:114), fliplr(down(95:114))];
%     fill(X_zoom, Y_zoom, gray);
%     plot(paramData(95:114), 'LineWidth', 2, 'Marker', 'o', 'LineStyle', '-', 'color', 'red');
%     xlim([min(t_zoom), max(t_zoom)]);
    %ylim([min(engData(80:119))-0.2, 10.2]);
end


