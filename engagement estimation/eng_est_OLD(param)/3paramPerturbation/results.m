close all;

%% increase noise variance
perLen = 1:10;
engMAD = [0.0564, 0.1412, 0.2572, 0.2855, 0.3724, 0.5153, 0.5968];
paramMAD = [];


figure;
plot(x, y, 'marker', 'o', 'color', 'k');
xlabel('noise \sigma');
ylabel('MAD');

%% decrease gaze samples
x = 10:-1:1;
y = [0.2187, 0.3235, 0.1671, 0.1817, 0.2521, 0.2871, 0.2878, 0.2131, 0.3123, 0.3123];

figure;
plot(x, y, 'marker', 'o', 'color', 'k');
xlabel('noise \sigma');
ylabel('MAD');

