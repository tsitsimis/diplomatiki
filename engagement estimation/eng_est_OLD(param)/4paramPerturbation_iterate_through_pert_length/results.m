close all;
load hyper_stats6;

myLineStyle = 'none';

%% increase of perturbation length
%% eng MAD - param MAD
figure;
subplot(2, 1, 1);
errorbar(hyper_stats(:, 1), hyper_stats(:, 2),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
ylabel('engagement MAD');

subplot(2, 1, 2);
errorbar(hyper_stats(:, 3), hyper_stats(:, 4),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
xlabel('perturbation duration (number of trials)');
%ylabel(['action parameter'; '      MAD       ']);
ylabel('action parameter MAD');

%% eng max AD - num of trials to 90%
figure;
subplot(2, 1, 1);
errorbar(hyper_stats(:, 5), hyper_stats(:, 6),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
ylabel([' engagement  '; 'max deviation']);

subplot(2, 1, 2);
errorbar(hyper_stats(:, 7), hyper_stats(:, 8),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
xlabel('perturbation duration (number of trials)');
ylabel('trials to 90%');

%% print table
% printmat(hyper_stats, 'stats',...
%     'DURATION1 DURATION2 DURATION3 DURATION4 DURATION5 DURATION6 DURATION7 DURATION8 DURATION9 DURATION10',...
%     'ENG_MAD ENG_STD PARAM_MAD PARAM_STD MAX MAX_STD #TRIALS #TRIALS_STD');

