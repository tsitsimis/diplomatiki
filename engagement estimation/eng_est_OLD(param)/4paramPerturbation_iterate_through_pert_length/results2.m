close all;
load stats3;

myLineStyle = 'none';

m = zeros(10, 4);
s = zeros(10, 4);
q25 = zeros(10, 4);
q75 = zeros(10, 4);

%% eng MAD - param MAD
figure;
subplot(1, 2, 1);
statsPert = reshape(stats(:, 1, :), 50, 10);
boxplot(statsPert, 'Symbol', '');
xlabel('perturbation duration (number of trials)');
ylabel('engagement');
ylim([0.2, 3]);
%title('MAD values', 'FontWeight', 'normal');
suptitle('MAD values');

m(:, 1) = mean(statsPert);
s(:, 1) = std(statsPert);
q25(:, 1) = quantile(statsPert, 0.25);
q75(:, 1) = quantile(statsPert, 0.75);

subplot(1, 2, 2);
statsPert = reshape(stats(:, 2, :), 50, 10);
boxplot(statsPert, 'Symbol', '');
xlabel('perturbation duration (number of trials)');
ylabel('action parameter');

m(:, 2) = mean(statsPert);
s(:, 2) = std(statsPert);
q25(:, 2) = quantile(statsPert, 0.25);
q75(:, 2) = quantile(statsPert, 0.75);


%% eng max AD - num of trials to 90%
figure;
subplot(1, 2, 1);
statsPert = reshape(stats(:, 3, :), 50, 10);
boxplot(statsPert, 'Symbol', '');
xlabel('perturbation duration (number of trials)');
ylabel([' engagement  '; 'max deviation']);

m(:, 3) = mean(statsPert);
s(:, 3) = std(statsPert);
q25(:, 3) = quantile(statsPert, 0.25);
q75(:, 3) = quantile(statsPert, 0.75);

subplot(1, 2, 2);
statsPert = reshape(stats(:, 4, :), 50, 10);
boxplot(statsPert, 'Symbol', '');
xlabel('perturbation duration (number of trials)');
ylabel('#trials to 90%');

m(:, 4) = mean(statsPert);
s(:, 4) = std(statsPert);
q25(:, 4) = quantile(statsPert, 0.25);
q75(:, 4) = quantile(statsPert, 0.75);


