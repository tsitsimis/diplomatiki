close all;
load hyper_stats;

myLineStyle = 'none';

%% increase of perturbation length
%% eng MAD - param MAD
figure;
subplot(2, 1, 1);
errorbar(hyper_stats(:, 1), hyper_stats(:, 2),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
ylabel(['engagement'; 'deviation ']);

subplot(2, 1, 2);
errorbar(hyper_stats(:, 3), hyper_stats(:, 4),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
xlabel('perturbation length');
ylabel(['parameter'; 'deviation']);

%% eng max AD - num of trials to 90%
figure;
subplot(2, 1, 1);
errorbar(hyper_stats(:, 5), hyper_stats(:, 6),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
ylabel([' engagement  '; 'max deviation']);

subplot(2, 1, 2);
errorbar(hyper_stats(:, 7), hyper_stats(:, 8),...
    'Marker', 'x', 'MarkerSize', 10, 'markerEdgeColor', 'red',...
    'LineStyle', myLineStyle, 'Color', 'black', 'LineWidth', 1);

xlim([0.5, 10.5]);
xlabel('perturbation length');
ylabel('trials to 90%');