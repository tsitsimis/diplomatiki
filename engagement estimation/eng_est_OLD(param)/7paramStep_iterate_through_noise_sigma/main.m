%% main
%close all;

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 100;
perturbationLength = 1;
vertiBars = false;

noiseArray = 0:0.2:2;
noiseLen = length(noiseArray);
%hyper_stats = zeros(noiseLen, 8);
nExperiments = 20;%50;
stats = zeros(nExperiments, 4, noiseLen);

for noise_i = 1:noiseLen
    disp(noise_i);

    for ee = 1:nExperiments
        % define Task
        BBT = BBsetTask();
        BBT.sigma_v = noiseArray(noise_i);

        % define baby robot
        BBR = BBrobot(BBT);

        % initialize model optimized parameters
        if (runOptimizedModels)
            load('bestModelsOptiSummer2016.mat')
            BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
        end

        % initial state
        s = BBT.P0;

        % initial action by the robot
        [BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

        % init LOGS
        LOG_FILES = [s a.action a.param 0 s a.action a.param BBT.cENG BBR.delta BBR.VC BBR.ACT BBR.PA...
            zeros(1, BBR.nA) zeros(1, BBR.nA) 0 BBR.varDelta BBR.sigma BBR.varACT BBR.kalmanV...
            diag(BBR.kalmanCOV)' BBR.sigma2Q(1) 0 0 0 ...
            BBT.cEST_ENG 0 0];

        %% RUN TASK
        for iii=1:episodeLength-1
            [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
            LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
                logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
                logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
                logs.estEngagement logs.estSigma logs.yawSigma]];
        end

        BBT.engMu = BBT.optimalParam2;
        for iii=1:perturbationLength
            [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
            LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
                logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
                logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
                logs.estEngagement logs.estSigma logs.yawSigma]];
        end
        
        BBT.engMu = BBT.optimalParam1;
        for iii=1:episodeLength
            [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
            LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
                logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
                logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
                logs.estEngagement logs.estSigma logs.yawSigma]];
        end

        %% statistics
        eng = LOG_FILES(101:200, 16);
        trial100 = find((eng >= 9.5) & (eng <= 9.99));
        if length(trial100) >= 1
            trial100 = 100 + trial100(end);
        else
            trial100 = 200;
        end
        
        %engWindow = LOG_FILES(:, 16);
        engWindow = LOG_FILES(101:trial100, 16);
        engMAD = sum(abs(engWindow - BBT.maxENG)) / length(engWindow);

        %paramWindow = LOG_FILES(:, 15);
        paramWindow = LOG_FILES(101:trial100, 15);
        paramMAD = sum(abs(paramWindow - BBT.optimalParam1)) / length(paramWindow);

        EngMaxAD = max(abs(engWindow - BBT.maxENG));

        numTrials90 = find(abs(LOG_FILES(100:end, 16) - 9) <= 0.3);
        if length(numTrials90) >= 1
            numTrials90 = numTrials90(end);
        else
            numTrials90 = 0;
        end

        stats(ee, :, noise_i) = [engMAD, paramMAD, EngMaxAD, numTrials90];
    end


    %% hyper-stats
%     meanEngMAD = mean(stats(:, 1));
%     stdEngMAD = std(stats(:, 1));
% 
%     meanParamMAD = mean(stats(:, 2));
%     stdParamMAD = std(stats(:, 2));
% 
%     meanEngMaxAD = mean(stats(:, 3));
%     stdEngMaxAD = std(stats(:, 3));
% 
%     meanNumTrials90 =  mean(stats(:, 4));
%     stdNumTrials90 =  std(stats(:, 4));
% 
%     hyper_stats(noise_i, :) = [meanEngMAD, stdEngMAD,...
%                               meanParamMAD, stdParamMAD,...
%                               meanEngMaxAD, stdEngMaxAD,...
%                               meanNumTrials90, stdNumTrials90];

end

% LOG_FILES
% 1-5 s / 6 old action / 7 old param / 8 r / 9-13 y / 14 action / 15 param
% 16 eng / 17 delta / 18 V / 19-24 ACT / 25-30 PA / 31-36 deltaACT
% 37-42 Q / 43 RPEQ / 44 varCriticDelta / 45 sigma / 46-51 varwA
% 52-57 kalmanV / 58-63 kalmanCOV / 64 sigma2Q / 65-66 star-ltar / 67 meta
% 68 est eng / 69 est gaze sigma / 70 real gaze sigma