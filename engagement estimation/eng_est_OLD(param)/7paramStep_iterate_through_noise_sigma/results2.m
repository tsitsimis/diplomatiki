close all;
load stats0_02_2;

myLineStyle = 'none';
noiseArray = 0:0.2:2;
noiseLen = length(noiseArray);

m = zeros(noiseLen, 4);
s = zeros(noiseLen, 4);
q25 = zeros(noiseLen, 4);
q75 = zeros(noiseLen, 4);

%% eng MAD - param MAD
figure;
subplot(1, 2, 1);
statsPert = reshape(stats(:, 1, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', '');
xlabel('measument noise \sigma', 'Interpreter', 'Tex');
ylabel('engagement');
%ylim([0, 1]);
suptitle('MAD values');

m(:, 1) = mean(statsPert);
s(:, 1) = std(statsPert);
q25(:, 1) = quantile(statsPert, 0.25);
q75(:, 1) = quantile(statsPert, 0.75);

%figure;
subplot(1, 2, 2);
statsPert = reshape(stats(:, 2, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', '');
xlabel('measument noise \sigma', 'Interpreter', 'Tex');
ylabel('action parameter');
%ylim([-0.2, 2.5]);

m(:, 2) = mean(statsPert);
s(:, 2) = std(statsPert);
q25(:, 2) = quantile(statsPert, 0.25);
q75(:, 2) = quantile(statsPert, 0.75);


%% eng max AD - num of trials to 90%
figure;
%subplot(1, 2, 1);
statsPert = reshape(stats(:, 3, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', '');
xlabel('measument noise \sigma', 'Interpreter', 'Tex');
ylabel([' engagement  '; 'max deviation']);
%ylim([0.5, 2]);

m(:, 3) = mean(statsPert);
s(:, 3) = std(statsPert);
q25(:, 3) = quantile(statsPert, 0.25);
q75(:, 3) = quantile(statsPert, 0.75);

figure;
%subplot(1, 2, 2);
statsPert = reshape(stats(:, 4, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', '');

xlabel('measument noise \sigma', 'Interpreter', 'Tex');
ylabel('#trials to 90%');
%ylim([-0.2, 10]);

m(:, 4) = mean(statsPert);
s(:, 4) = std(statsPert);
q25(:, 4) = quantile(statsPert, 0.25);
q75(:, 4) = quantile(statsPert, 0.75);

