Here is the source code that I am using for the model presented in the paper that Costas previously sent you.

The code includes different parallel learning models (kalman, CACLA, schweighoferDoya2003, etc.) because we are comparing these different models. The one that works better and that interests us for the moment is the fifth model. Thus you have to set the variable "whichModel" in "main.m" to 5.

There are two ways to launch the scripts. One is through main.m, which launches an experiment of 5 blocks of 1000 iterations, and then plots a figure. This is the method that you want to use for the moment, to see how the algorithm works.

There is another method which consists in launching BBoptimizeParameters() which searches for the best parameterset that gives the best performance to each possible model in our task. This is how I launch the code on a cluster of computers which searches for days and days the best parameter sets... :-)

You don't need this for the moment.

What you need to know for the moment is that BBsetTask.m creates a structure called BBT which contains the task parameters. BBrobot.m creates a structure called BBR which contains the robot. The runTrial.m function is probably the one you will have to modify, which determines how the child engagement varies, then compute a reward signal r based on this variation, and then asks the robot to learn based on this and to make a decision of what action to perform next. Finally, this runTrial.m function stores log of the current trial. This is what you will need to adapt to your V/REP simulations I think, as well as parameters of BBrobot.m so that the speed of learning changes in your V/REP simulations. If you want to use the parameter values determined in BBrobot.m rather than those obtained through optimization, then you need to set the variable "runOptimizedModels" to 0 in main.m

If you want to simplify your code in order to optimize computation time, you can of course comment all parts of BBrobot related to other models than model 5.