function neckAngle = humanGaze(action, exec_param, optimal_param)
    %% gaussian pdf
    mu = action;
    sigma = eng2sigma(exec_param, optimal_param);
    
    neckAngle = normrnd(mu, sigma);
    while abs(neckAngle) > pi/3 % human neck can only move between -pi/3 and pi/3
        neckAngle = normrnd(mu, sigma);
    end
    
    %% add gaussian noise
    noiseSigma = 0.005;
    neckAngle = neckAngle + normrnd(0, noiseSigma);
end

% function neckAngle = humanGaze(action, eng)
%     %% gaussian pdf
%     mu = action;
%     sigma = eng2sigma(eng);
%     
%     neckAngle = normrnd(mu, sigma);
%     while abs(neckAngle) > pi/3 % human neck can only move between -pi/3 and pi/3
%         neckAngle = normrnd(mu, sigma);
%     end
%     
%     %% add gaussian noise
%     noiseSigma = pi / 100;
%     neckAngle = neckAngle + normrnd(0, noiseSigma);
% end