function eng = sigma2eng(sigma, sigmaMin, sigmaMax, engThres, engMax)
    eng = engThres + (sigmaMax - sigma) * (engMax - engThres) / (sigmaMax - sigmaMin);
end