%% main

close all;
addpath(genpath('v-rep connection'));
addpath(genpath('v-rep matlab functions'));

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 50;
vertiBars = false;

% define Task
BBT = BBsetTask();

% define baby robot
BBR = BBrobot(BBT);

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
% 1 eng / 2 simulated eng / 3 gaze var / 4 action / 5 param
my_logs = [BBT.cENG,  BBT.cEST_ENG,  BBT.cMSD,              a.action, a.param,...
           BBR.sigma, BBR.metaparam, BBR.sigma2Q(a.action), BBR.Q];

%% RUN TASK
for iii = 1:episodeLength
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q];
end

BBT.engMu = BBT.optimalParam2;
for iii = 1:episodeLength
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q];
end


%% plot figures
figEng = figure;
hold all;
box on;
h1 = plot(my_logs(:, 1), 'linewidth', 1, 'LineStyle', '-', 'color', 'k');
h2 = plot(my_logs(:, 2), 'linewidth', 1, 'LineStyle', '--', 'color', 'k');
axis([1, length(my_logs), 0, 10.5]);
legend([h1, h2], {'real', 'estimated'});
xlabel('timesteps');
title('engagement', 'fontWeight','normal');

% figMSD = figure;
% hold all;
% box on;
% plot(my_logs(:, 3), 'linewidth', 1, 'LineStyle', '-', 'color', 'black', 'marker', 'none');
% axis([1, length(my_logs), 0, pi/9]);
% yValues = [0, pi/100, pi/18, pi/9];
% set(gca, 'YTick', yValues);
% set(gca, 'YTickLabel', {'0', '\pi/100', '\pi/18', '\pi/9'});
% xlabel('timesteps');
% title('mean square deviation', 'fontWeight','normal');

figParam = figure;
hold all;
box on;
% executed param
h1 = plot(my_logs(:, 5), 'LineWidth', 1, 'LineStyle', '-', 'color', 'black'); %[0,0.45, 0.74]
% optimal param
h2 = plot(2:length(my_logs), [BBT.optimalParam1*ones(1, episodeLength), ...
    BBT.optimalParam2*ones(1, episodeLength)], '--', 'LineWidth', 1, 'LineStyle', '-.', 'color', 'red');
axis([1, length(my_logs), min(BBT.intervalle)-1, max(BBT.intervalle) + 1]);
xlabel('timesteps');
title('action parameter', 'fontWeight','normal');
legend([h1, h2], {'executed param', 'optimal param'});


figure;
hold all;
box on;
h1 = plot(my_logs(:, 6), 'linewidth', 2);
xlabel('trials');
title('exploration \sigma');
% 
% figure;
% hold all;
% box on;
% h1 = plot(my_logs(:, 8), 'linewidth', 2);
% xlabel('trials');
% title('\sigma 2Q');
% 
% figure;
% hold all;
% box on;
% h1 = plot(my_logs(:, 7), 'linewidth', 2);
% xlabel('trials');
% title('metaparam');