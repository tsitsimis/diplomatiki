function world = makeAction(world, cube)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    
    opMode = world.oneshot_wait;
    
    if cube == 1 || cube == 2 || cube == 3
        if NAO.LHand == 1
            world = putNAOhandDown(world, 'left');
        end
        
        if NAO.RHand == 0
            world = putNAOhandUp(world, 'right');
        end
    else
        if NAO.RHand == 1
            world = putNAOhandDown(world, 'right');
        end
        
        if NAO.LHand == 0
            world = putNAOhandUp(world, 'left');
        end
    end
    
    NAO = world.NAO;
    
    switch cube
        case 1
            vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, deg2rad(-5), opMode);
        case 2
            vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, deg2rad(6), opMode);
        case 3
            vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, deg2rad(18), opMode);
        case 4
            vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(-18), opMode);
        case 5
            vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(-8), opMode);
        case 6
            vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(5), opMode);
    end
    
    world.NAO = NAO;
end