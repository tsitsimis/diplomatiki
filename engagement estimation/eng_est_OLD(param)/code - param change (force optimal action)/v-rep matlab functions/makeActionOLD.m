function makeActionOLD(world, a)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    angles = NAO.angles;
    
    opMode = vrep.simx_opmode_oneshot_wait;
    
    vrep.simxSetJointTargetPosition(clientID, NAO.HeadYawHandle, angles(a), opMode);
    if a >= 4 %angles(a) <= 0
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, pi/2, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, 0, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, 0, opMode);
    
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, angles(a), opMode);
    else
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, pi/2, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, 0, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, 0, opMode);
    
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, angles(a), opMode);
    end
end
