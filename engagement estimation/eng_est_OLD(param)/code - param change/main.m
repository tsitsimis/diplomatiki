%% main

close all;
addpath(genpath('v-rep connection'));
addpath(genpath('v-rep matlab functions'));

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 1000;
vertiBars = false;
    
% define Task
BBT = BBsetTask();

% define baby robot
BBR = BBrobot(BBT);

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
% 1 eng / 2 simulated eng / 3 gaze var / 4 action / 5 param
my_logs = [BBT.cENG,  BBT.cEST_ENG,  BBT.cMSD,              a.action, a.param,...
           BBR.sigma, BBR.metaparam, BBR.sigma2Q(a.action), BBR.Q,    BBR.star,...
           BBR.mtar];

%% RUN TASK
for iii = 1:episodeLength
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q,      logs.star,...
                        logs.mtar];
end

BBT.engMu = BBT.optimalParam2;
for iii = 1:episodeLength
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q,      logs.star,...
                        logs.mtar];
end

%% plot figures
if true
    
figEng = figure;
hold all;
box on;
h1 = plot(my_logs(:, 1), 'linewidth', 1, 'LineStyle', '-', 'color', 'black', 'marker', 'none');
h2 = plot(my_logs(:, 2), 'linewidth', 1, 'LineStyle', '--', 'color', 'black', 'marker', 'none');
axis([1, length(my_logs), 0, 10.5]);
legend([h1, h2], {['real',sprintf('\n'),'engagement'], ['estimated',sprintf('\n'),'engagement']});
xlabel('timesteps');
title('engagement', 'fontWeight','normal');

figMSD = figure;
hold all;
box on;
plot(my_logs(:, 3), 'linewidth', 1, 'LineStyle', '-', 'color', 'black', 'marker', 'none');
axis([1, length(my_logs), 0, pi/9]);
yValues = [0, pi/50, pi/18, pi/9];
set(gca, 'YTick', yValues);
set(gca, 'YTickLabel', {'0', '\pi/50', '\pi/18', '\pi/9'});
xlabel('timesteps');
title('mean square deviation', 'fontWeight','normal');

figParam = figure;
hold all;
box on;
%param zone
zoneColor = [0.9765, 0.8980, 0.9176];
x = 1:length(my_logs);
f1 = fill([x, flip(x)], [max(BBT.zone1)*ones(size(x)), min(BBT.zone1)*ones(size(x))], zoneColor, 'LineStyle','none');
f2 = fill([x, flip(x)], [max(BBT.zone2)*ones(size(x)), min(BBT.zone2)*ones(size(x))], zoneColor, 'LineStyle','none');
% executed param
h1 = plot(my_logs(:, 5), 'LineWidth', 1, 'LineStyle', '-', 'color', 'black'); %[0,0.45, 0.74]
% optimal param 1
h2 = plot([1, length(my_logs)], [BBT.optimalParam1, BBT.optimalParam1], '--',...
    'LineWidth', 1, 'LineStyle', '-.', 'color', 'red'); % [0.47, 0.67, 0.19]
% optimal param 1
h3 = plot([1, length(my_logs)], [BBT.optimalParam2, BBT.optimalParam2], '--',...
    'LineWidth', 1, 'LineStyle', '-.', 'color', 'blue'); % [0.47, 0.67, 0.19]
% mean param value
%plot(medfilt1(my_logs(:, 5),100), 'LineWidth', 2, 'color', [0.8, 0, 0]);
axis([1, length(my_logs), min(BBT.intervalle)-5, max(BBT.intervalle) + 5]);
xlabel('timesteps');
title('action parameter', 'fontWeight','normal');
legend([h1, h2, h3], {'executed param', 'optimal param1', 'optimal param2'});

end
% figure;
% hold all;
% box on;
% h1 = plot(my_logs(:, 6), 'linewidth', 2);
% xlabel('trials');
% title('exploration \sigma');
% 
% figure;
% hold all;
% box on;
% plot(my_logs(:, 8), 'linewidth', 2);
% xlabel('trials');
% title('\sigma 2Q');
% 
% figure;
% hold all;
% box on;
% h1 = plot(my_logs(:, 7), 'linewidth', 2);
% xlabel('trials');
% title('metaparam');
% 
% 
% figure;
% hold all;
% box on;
% h1 = plot(my_logs(:, 10) - my_logs(:, 11), 'linewidth', 2);
% % h2 = plot(my_logs(:, 11), 'linewidth', 2);
% % legend([h1, h2], {'start', 'mtar'});
% xlabel('trials');
% title('star - mtar');
