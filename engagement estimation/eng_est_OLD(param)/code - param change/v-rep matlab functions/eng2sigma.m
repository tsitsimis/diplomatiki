function sigma = eng2sigma(eng)
    n = 2;
    sigma = (pi/9) * (1 - (eng/10)^n)^(1/n);
    %sigma = (pi/ 9) / exp(eng);
end