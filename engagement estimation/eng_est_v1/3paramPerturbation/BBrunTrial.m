function [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a )
    
    % BBT = task used for Baby Robot learning
    % BBR = structure containing the baby robot
    % whichModel = 1 QL 2 kalman 3 sigma2Q 4 hybrid 5 schweig1 6 schweig2
    % s = state vector in which the robot was
    % a = structure containing [executed action, action parameter]
    
    %% UPDATING THE TASK STATE
    % Perform a step of the TASK by executing the action decided outside the
    % function
    % for the moment, only the pointing action can increase the child's
    % engagement
    %[y,r] = BBtaskStep(BBT, s, a); % y = new state ; r = new reward
    y = s; % new state
    r = 0; % reward
    olds = s; % storing s
    olda = a; % storing a
    
    %% UPDATING THE CHILD ENGAGEMENT
    % storing the old engagement
    oldEstEng = BBT.cEST_ENG;
    oldEng = BBT.cENG;
    % computing the new engagement
    if (a.action == BBT.optimal)
        %% gaussian child engagement
        % probaEng is between -1 (disengagement) and 1 (reengagement)
        probaEng = (exp((- (a.param - BBT.engMu) ^ 2) / (2 * BBT.engSig ^ 2)) - 0.5) * 2;
        if (probaEng >= 0)
            %BBT.cENG = BBT.cENG + probaEng * BBT.reeng * (BBT.maxENG - BBT.cENG);
            my_rate = BBT.reeng;%1 - abs(a.param - BBT.engMu) / 20;
            BBT.cENG = BBT.cENG + probaEng * my_rate * (BBT.maxENG - BBT.cENG);
        else
            BBT.cENG = BBT.cENG - probaEng * BBT.forget * (BBT.minENG - BBT.cENG);
        end
        
    else % the robot performed a non-optimal action
        BBT.cENG = BBT.cENG + BBT.forget * (BBT.minENG - BBT.cENG);
    end
    
    Q = 0.1;
    BBT.cENG = BBT.cENG + normrnd(0, Q);
    BBT.cENG = min(BBT.cENG, 10);
    BBT.cENG = max(BBT.cENG, 0);
    
    % observation
    % the human turns his head with variance 
    % inversely proportional to his engagement.
    H1 = -2;
    H2 = -1;
    H3 = -5;
    R1 = 1;
    R2 = 1;
    R3 = 1;
    sigma_yaw = H1 * (-10 + BBT.cENG);
    sigma_pitch = H2 * (-10 + BBT.cENG);
    sigma_roll = H3 * (-10 + BBT.cENG);
    
    % we measure the head yaw and pitch angles to estimate their standard deviation
    nSamples = 10;
    yaw_0 = 0; % yaw angle of optimal action
    sigma_yaw_est = sum((normrnd(yaw_0, sigma_yaw, [1, nSamples]) + normrnd(0, R1) - yaw_0).^2);
    sigma_yaw_est = sigma_yaw_est / nSamples;
    sigma_yaw_est = sqrt(sigma_yaw_est);
    
    pitch_0 = 0; % pitch angle of optimal action
    sigma_pitch_est = sum((normrnd(pitch_0, sigma_pitch, [1, nSamples]) + normrnd(0, R2) - pitch_0).^2);
    sigma_pitch_est = sigma_pitch_est / nSamples;
    sigma_pitch_est = sqrt(sigma_pitch_est);
    
    roll_0 = 0; % roll angle of optimal action
    sigma_roll_est = sum((normrnd(roll_0, sigma_roll, [1, nSamples]) + normrnd(0, R3) - roll_0).^2);
    sigma_roll_est = sigma_roll_est / nSamples;
    sigma_roll_est = sqrt(sigma_roll_est);
    
    % predict
    a1 = (exp((- sigma_yaw_est ^ 2) / (2 * 0.1 ^ 2)) * ...
        exp((- sigma_pitch_est ^ 2) / (2 * 0.2 ^ 2)) * ...
        exp((- sigma_roll_est ^ 2) / (2 * 0.5 ^ 2)) - 0.5) * 2;
    if a1 >=0 
        BBT.cEST_ENG = BBT.cEST_ENG + 0.1 * a1 * (BBT.maxENG - BBT.cENG);
    else
        BBT.cEST_ENG = BBT.cEST_ENG - 0.1 * a1 * (BBT.minENG - BBT.cENG);
    end
    BBT.P = BBT.P + Q;
    
    % update 1
    y1 = sigma_yaw_est - H1 * (-10 + BBT.cEST_ENG);
    S = R1 + H1 * BBT.P * H1;
    K = BBT.P * H1 / S;
    BBT.cEST_ENG = BBT.cEST_ENG + K * y1;
    BBT.P = (1 - K * H1) * BBT.P;
    
    % update 2
    y2 = sigma_pitch_est - H2 * (-10 + BBT.cEST_ENG);
    S = R2 + H2 * BBT.P * H2;
    K = BBT.P * H2 / S;
    BBT.cEST_ENG = BBT.cEST_ENG + K * y2;
    BBT.P = (1 - K * H2) * BBT.P;
    
    % update 3
    y3 = sigma_roll_est - H3 * (-10 + BBT.cEST_ENG);
    S = R3 + H3 * BBT.P * H3;
    K = BBT.P * H3 / S;
    BBT.cEST_ENG = BBT.cEST_ENG + K * y3;
    BBT.P = (1 - K * H3) * BBT.P;
    
    BBT.cEST_ENG = min(BBT.cEST_ENG, 10);
    BBT.cEST_ENG = max(BBT.cEST_ENG, 0);
    
    %% setting the reward as a function of the difference in engagement
    %r = BBT.cENG - oldEng; % reward = variations in child engagement
    %r = (BBT.cENG - 5) / 20; % reward = (child engagement - 5) / 5
    % mixed reward function
    %r = (1 - BBT.lambdaRwd) * (BBT.cENG - 5) / 5 + BBT.lambdaRwd * 2 * (BBT.cENG - oldEng);
    r = (1 - BBT.lambdaRwd) * (BBT.cEST_ENG - 5) / 5 + BBT.lambdaRwd * 2 * (BBT.cEST_ENG - oldEstEng);
    
    %% HAVE THE ROBOT LEARN FROM THIS OUTCOME
    deltaACT = BBR.ACT;
    BBR = BBrobotLearns( BBR, s, a, y, r );
    deltaACT = (s * BBR.wA) - deltaACT;

    %% NEXT STATE
    if (r > 0), % If game completed, reset the game
        s = BBT.P0;
    else % Otherwise, the next state becomes the current state
        s = y;
    end;
    
    %% HAVE THE ROBOT MAKE A DECISION
    [BBR, a, probaA, probaPISA] = BBrobotDecides( BBT, BBR, whichModel, s );
    
    % LOGS
    % logs n�cessaires � l'optimisation
    logs.reward = r;
    logs.engagement = BBT.cENG;
    logs.estEngagement = BBT.cEST_ENG;
    logs.estSigma = sigma_yaw_est;
    logs.yawSigma = sigma_yaw;
    logs.probaA = probaA;
    logs.probaPISA = probaPISA;
    % logs n�cessaires pour le main et l'affichage des figures
    logs.s = olds;
    logs.oldaction = olda.action;
    logs.oldparam = olda.param;
    logs.y = y;
    logs.action = a.action;
    logs.param = a.param;
    logs.delta = BBR.delta;
    logs.VC = BBR.VC;
    logs.ACT = BBR.ACT;
    logs.PA = BBR.PA;
    logs.deltaACT = deltaACT;
    logs.Q = BBR.Q;
    logs.RPEQ = BBR.RPEQ(olda.action);
    logs.varDelta = BBR.varDelta;
    logs.sigma = BBR.sigma;
    logs.dwA = BBR.varACT;
    logs.kalmanV = BBR.kalmanV;
    logs.kalmanCOV = diag(BBR.kalmanCOV)';
    logs.sigma2Q = BBR.sigma2Q(olda.action);
    logs.star = BBR.star;
    logs.ltar = BBR.ltar;
    logs.meta = BBR.metaparam;
    %logs
    
end