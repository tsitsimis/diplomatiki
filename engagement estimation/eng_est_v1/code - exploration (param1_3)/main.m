%% main

close all;
addpath(genpath('v-rep connection'));
addpath(genpath('v-rep matlab functions'));

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 200;
vertiBars = false;

DATA2 = [];
for ee = 1:1

% define Task
BBT = BBsetTask();

% define baby robot
BBR = BBrobot(BBT);
BBR.gainSigma = 10000;

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
% 1 eng / 2 simulated eng / 3 gaze var / 4 action / 5 param
% 6 sigma / 7 metaparam / 8 sigma2Q(action) / 9-14 Q / 15 realMSD
my_logs = [BBT.cENG,  BBT.cEST_ENG,  BBT.cMSD,              a.action, a.param,...
           BBR.sigma, BBR.metaparam, BBR.sigma2Q(a.action), BBR.Q,    BBT.realMSD];

%% RUN TASK
for iii = 1:episodeLength-1
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q,      logs.realMSD];
end

BBT.engMu = BBT.optimalParam2;
for iii = 1:episodeLength
   [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    my_logs = [my_logs; logs.engagement, logs.cEST_ENG, logs.cMSD,    logs.action, logs.param,...
                        logs.sigma,      logs.meta,     logs.sigma2Q, logs.Q,      logs.realMSD];
end

DATA2 = [DATA2; my_logs(:, 1)'];
end

%% plot figures
if true
figEng = figure;
hold all;
box on;
h1 = plot(my_logs(:, 1), 'linewidth', 1, 'LineStyle', '-', 'color', 'black');
h2 = plot(my_logs(:, 2), 'linewidth', 1, 'LineStyle', '--', 'color', 'black');
legend([h1, h2], {['real',sprintf('\n'),'engagement'], ['estimated',sprintf('\n'),'engagement']});
xlabel('trials');
title('engagement', 'fontWeight','normal');

figMSD = figure;
hold all;
box on;
h1 = plot(my_logs(:, 15), 'linewidth', 1, 'LineStyle', '-', 'color', 'black', 'marker', 'none');
h2 = plot(my_logs(:, 3), 'linewidth', 1, 'LineStyle', '--', 'color', 'black', 'marker', 'none');
xlabel('trials');
title('mean square deviation', 'fontWeight','normal');
legend([h1, h2], {'real msd', 'estimated msd'});

figParam = figure;
hold all;
box on;
% executed param
h1 = plot(my_logs(:, 5), 'LineWidth', 1, 'LineStyle', '-', 'color', 'black');
% optimal param
% h2 = plot([1, size(my_logs, 1)], [BBT.optimalParam1, BBT.optimalParam1], '--',...
%     'LineWidth', 1, 'LineStyle', '--', 'color', 'red');
h2 = plot(1:size(my_logs, 1), [ones(1, episodeLength)*BBT.optimalParam1, ones(1, episodeLength)*BBT.optimalParam2],...
   '--', 'LineWidth', 1, 'LineStyle', '--', 'color', 'red');
axis([1, size(my_logs, 1), min(BBT.intervalle)-1, max(BBT.intervalle) + 1]);
xlabel('trials');
title('action parameter', 'fontWeight','normal');
legend([h1, h2], {'executed param', 'optimal param'});

figure;
hold all;
box on;
h1 = plot(my_logs(:, 6), 'linewidth', 2);
xlabel('trials');
title('exploration \sigma');


figure;
hold all;
box on;
h1 = plot(my_logs(:, 8), 'linewidth', 2);
xlabel('trials');
title('\sigma 2Q');

figure;
hold all;
box on;
h1 = plot(my_logs(:, 7), 'linewidth', 2);
xlabel('trials');
title('metaparam');

else

gray = [173, 175, 178]/255;
mData = mean(DATA2, 1);
n = size(DATA2, 2);
stdUpData = zeros(1, n);
for i = 1:n
    ind = find(DATA2(:, i) >= mData(i));
    stdUpData(i) = sqrt(sum((DATA2(ind, i) - mData(i)) .^ 2) / n);
end
stdDownData = zeros(1, n);
for i = 1:n
    ind = find(DATA2(:, i) < mData(i));
    stdDownData(i) = sqrt(sum((DATA2(ind, i) - mData(i)) .^ 2) / n);
end
up = mData + stdUpData;
down = mData - stdDownData;
t = 1:size(my_logs, 1);
X = [t, fliplr(t)];
Y = [up, fliplr(down)];

figure;
hold all;
grid on;
box on;
h_fill = fill(X, Y, gray);
h_mean = plot(mData, 'LineWidth', 2, 'LineStyle', '-', 'color', 'blue');
plot(up, 'LineWidth', 1, 'LineStyle', '-', 'color', 'k');
plot(down, 'LineWidth', 1, 'LineStyle', '-', 'color', 'k');
legend([h_mean, h_fill], {'mean engagement', 'variance'});
xlabel('trials');
title('engagement', 'fontWeight','normal');
end