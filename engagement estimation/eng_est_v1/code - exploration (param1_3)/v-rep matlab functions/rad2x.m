function x = rad2x(th)
    x = 0.5*(1+3*th/pi);
end