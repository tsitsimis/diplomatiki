function [t1, t2] = calcAngles(x, y)
    %l1 = 0.16999;
    l1 = 0.104784588561486;
    %l2 = 0.155976;
    l2 = 0.086252941978810;
    % l1 + l2 = 0.191037530540296;

    t2 = x^2 + y^2 - (l1^2 + l2^2);
    t2 = t2 / (2 * l1 * l2);
    t2 = acos(t2);

    k1 = l1 + l2 * cos(t2);
    k2 = l2 * sin(t2);
    t1 = atan2(y, x) - atan2(k2, k1);
    
    t1 = rad2deg(t1);
    t2 = rad2deg(t2);
end