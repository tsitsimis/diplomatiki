% function sigma = eng2sigma(eng)
%     n = 2;
%     sigma = (pi/9) .* (1 - (eng/10).^n).^(1/n);
%     %sigma = (pi/ 9) / exp(eng);
% end

function sigma = eng2sigma(exec_param, optimal_param)
    sigma = abs(exec_param - optimal_param);
end