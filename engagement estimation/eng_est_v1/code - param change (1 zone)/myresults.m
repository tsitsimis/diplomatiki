close all;

x = [2, 3, 4, 5];

y2 = [5, 8, 12, 14]';
y3 = [8, 29, 16, 13]';
y4 = [40, 10, 16, 18]';
y5 = [40, 18, 36, 15]';

y = [y2, y3, y4, y5];

y_mean = mean(y);
y_std = std(y);

resFig = figure;
hold all;
box on;
errorbar(x, y_mean, y_std, 'x', 'MarkerSize', 10, ...
    'MarkerEdgeColor', 'k');
xlabel('\mu^*_2 / \mu^*_1');
ylabel('trials');
axis([1.5, 5.5, 0, 50]);

set(gca, 'XTick', x);
set(gca, 'XTickLabel', {'2', '3', '4', '5'});

