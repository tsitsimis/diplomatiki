function makeAction2(world, cube)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    
    opMode = world.oneshot_wait;
    
    angles = [-45, -30, -20, -4, 8, 18];
    vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, ...
        deg2rad(angles(cube)), opMode);
    
    vrep.simxSetJointTargetPosition(clientID, NAO.HeadYawHandle, ...
        deg2rad(angles(cube)), opMode);
end