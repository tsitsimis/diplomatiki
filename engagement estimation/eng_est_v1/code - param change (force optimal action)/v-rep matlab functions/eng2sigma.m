function sigma = eng2sigma(eng, sigmaMin, sigmaMax, engThres, engMax)
    sigma = sigmaMax - (eng - engThres) * (sigmaMax - sigmaMin) / (engMax - engThres);
%     a = (sigmaMax - sigmaMin) / (1/engThres - 1/engMax);
%     b = sigmaMax - a / engThres;
%     sigma = a / eng + b;
%     sigma = sigma * 0.01;
end