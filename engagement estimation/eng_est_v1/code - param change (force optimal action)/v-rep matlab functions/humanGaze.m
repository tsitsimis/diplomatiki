function neckAngle = humanGaze(target, eng)
    % gaussian pdf
    mu = target;
%     sigma = (pi/ 9) / exp(eng);
    n = 1.4;
    sigma = (pi/9) * (1 - (eng/10)^n)^(1/n);
    
    neckAngle = normrnd(mu, sigma);
    
    % human neck can only move between -pi/3 and pi/3
    neckAngle = max(neckAngle, -pi/3);
    neckAngle = min(neckAngle, pi/3);
    
    % add gaussian noise
    noiseSigma = pi/100;
    neckAngle = neckAngle + normrnd(0, noiseSigma);
end