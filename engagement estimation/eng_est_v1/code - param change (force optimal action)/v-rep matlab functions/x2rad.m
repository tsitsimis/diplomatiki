function th = x2rad(x)
    th = (pi/3)*(2*x - 1);
end