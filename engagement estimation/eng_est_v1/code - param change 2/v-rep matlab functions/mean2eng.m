function eng = mean2eng(gaze_mean, target_angle)
    d_min = 0;
    d_max = pi/8;
    eng_min = 0;
    eng_max = 10;
    d = abs(gaze_mean - target_angle);
    d = min(d, d_max);
    d = max(d, d_min);
    eng = eng_min + (eng_max - eng_min) * (d - d_max) / (d_min - d_max);
end