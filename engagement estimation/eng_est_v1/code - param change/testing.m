%theta = -(pi/3):0.01:(pi/3);
thetaLen = 100;
theta = linspace(-(pi/3), (pi/3), thetaLen);

mu = pi/10;
sigma = 0.5;

pdf = normpdf(theta, mu, sigma);
pdf = pdf / sum(pdf);

nSamples = 10;
samples = zeros(1, nSamples);
estDistr = zeros(1, thetaLen);
for i = 1:nSamples
    rndIndex = drand01(pdf);
    samples(i) = theta(rndIndex);
    estDistr(rndIndex) = estDistr(rndIndex) + 1;
end

estMu = mean(samples);
estSigma = std(samples);
% estSigma2 = sqrt(sum((samples - estMu).^2) / (nSamples-1));

estDistr = estDistr / sum(estDistr);
%figure; plot(theta, estDistr)