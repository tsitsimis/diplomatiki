close all;
% load stats6_20exp_noise05to5_trials_to_95;
load stats9_20exp_noise05to5_compare_eng2esteng;

nExperiments = 20;
myLineStyle = 'none';
outlierSymbol = '';
boxFillColor = [0, 0, 0]/255;
plot_style = 'traditional';
median_style = 'line';
x_label = 'noise \sigma';

noiseArray = 0.5:0.5:5;
noiseLen = length(noiseArray);

m = zeros(noiseLen, 4);
s = zeros(noiseLen, 4);
q25 = zeros(noiseLen, 4);
q75 = zeros(noiseLen, 4);

%% eng MAD - param MAD
figure;
subplot(2, 1, 1);
statsPert = reshape(stats(:, 1, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', outlierSymbol, 'Color', boxFillColor,...
    'plotstyle', plot_style, 'medianstyle', median_style);
% xlabel(x_label);
ylabel('engagement');
% ylim([-0.1, 1.1]);
%title('MAD values', 'FontWeight', 'normal');
% suptitle('MAD values');

m(:, 1) = mean(statsPert);
s(:, 1) = std(statsPert);
q25(:, 1) = quantile(statsPert, 0.25);
q75(:, 1) = quantile(statsPert, 0.75);

subplot(2, 1, 2);
statsPert = reshape(stats(:, 2, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', outlierSymbol, 'Color', boxFillColor,...
    'plotstyle', plot_style, 'medianstyle', median_style);
xlabel(x_label, 'interpreter', 'tex');
% ylim([0, 2.5]);
ylabel('action parameter');

m(:, 2) = mean(statsPert);
s(:, 2) = std(statsPert);
q25(:, 2) = quantile(statsPert, 0.25);
q75(:, 2) = quantile(statsPert, 0.75);


%% eng max AD - num of trials to 90%
figure;
subplot(2, 1, 1);
statsPert = reshape(stats(:, 3, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', outlierSymbol, 'Color', boxFillColor,...
    'plotstyle', plot_style, 'medianstyle', median_style);
% xlabel(x_label);
ylabel([' engagement  '; 'max deviation']);
% ylim([0.2, 8]);

m(:, 3) = mean(statsPert);
s(:, 3) = std(statsPert);
q25(:, 3) = quantile(statsPert, 0.25);
q75(:, 3) = quantile(statsPert, 0.75);

subplot(2, 1, 2);
statsPert = reshape(stats(:, 4, :), nExperiments, noiseLen);
boxplot(statsPert, noiseArray, 'Symbol', outlierSymbol, 'Color', boxFillColor,...
    'plotstyle', plot_style, 'medianstyle', median_style);
xlabel(x_label, 'interpreter', 'tex');
ylabel('#trials to 90%');
% ylim([-2, 30]);

m(:, 4) = mean(statsPert);
s(:, 4) = std(statsPert);
q25(:, 4) = quantile(statsPert, 0.25);
q75(:, 4) = quantile(statsPert, 0.75);


