#! /bin/sh
#PBS -N BBkalman2sepGrid
#PBS -o /home/khamassi/Data/NTUA/BBparallelOpti_grid02_model2kalmansep.out
#PBS -b /home/khamassi/Data/NTUA/BBparallelOpti_grid02_model2kalmansep.err
#PBS -m abe
#PBS -M khamassi@isir.upmc.fr
#PBS -l walltime=10000:00:00
#PBS -l nodes=1:ppn=30
#PBS -d /home/khamassi/Data/NTUA
cd /home/khamassi/Src/matlab-code/NTUA
/usr/local/bin/matlab -nodesktop -nosplash -r BBparallelOpti
