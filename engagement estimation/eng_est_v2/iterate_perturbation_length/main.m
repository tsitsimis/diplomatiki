%% main

%clear all
close all
%clc

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 100;
vertiBars = false;

nPerturbations = 10;
nExperiments = 20;

stats = zeros(nExperiments, 4, nPerturbations);

for perturbationLength = 1:10
disp(perturbationLength);

for ee = 1:nExperiments
    % define Task
    BBT = BBsetTask();

    % define baby robot
    BBR = BBrobot(BBT);

    % initialize model optimized parameters
    if (runOptimizedModels)
        load('bestModelsOptiSummer2016.mat')
        BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
    end

    % initial state
    s = BBT.P0;

    % initial action by the robot
    [BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

    % init LOGS
    LOG_FILES = [s a.action a.param 0 s a.action a.param BBT.cENG BBR.delta BBR.VC BBR.ACT BBR.PA...
        zeros(1, BBR.nA) zeros(1, BBR.nA) 0 BBR.varDelta BBR.sigma BBR.varACT BBR.kalmanV...
        diag(BBR.kalmanCOV)' BBR.sigma2Q(1) 0 0 0 ...
        BBT.cEST_ENG 0 0];

    %% RUN TASK
    for iii=1:episodeLength
        [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
        LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
            logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
            logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
            logs.estEngagement logs.estSigma logs.yawSigma]];
    end

    BBT.engMu = BBT.optimalParam2;
    for iii=1:perturbationLength
        [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
        LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
            logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
            logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
            logs.estEngagement logs.estSigma logs.yawSigma]];
    end

    BBT.engMu = BBT.optimalParam1;
    for iii=1:episodeLength
        [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
        LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param...
            logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta...
            logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta...
            logs.estEngagement logs.estSigma logs.yawSigma]];
    end

    %% statistics
    eng = LOG_FILES(episodeLength:episodeLength*2, 16);
    trial100 = find(eng <= 9.5);
    if length(trial100) >= 1
        trial100 = episodeLength + trial100(end);
    else
        trial100 = episodeLength*2;
    end
    
    %engWindow = LOG_FILES(101:200, 16);
    engWindow = LOG_FILES(episodeLength:trial100, 16);
    engMAD = mean(abs(engWindow - BBT.maxENG));

    %paramWindow = LOG_FILES(101:200, 15);
    paramWindow = LOG_FILES(episodeLength:trial100, 15);
    paramMAD = mean(abs(paramWindow - BBT.optimalParam1));

    EngMaxAD = max(abs(engWindow - BBT.maxENG));

    % trial90 = find(abs(engWindow(perturbationLength:end) - 9) <= 0.5);
    trial90 = find(engWindow(perturbationLength:end) <= 9);
    if length(trial90) >= 1
        trial90 = trial90(end);
    else
        trial90 = 0;
    end

    stats(ee, :, perturbationLength) = [engMAD, paramMAD, EngMaxAD, trial90];
end

end

% figure; plot(LOG_FILES(:, 16));
% figure; plot(LOG_FILES(:, 15)); ylim([-10, 10]);

% LOG_FILES
% 1-5 s / 6 old action / 7 old param / 8 r / 9-13 y / 14 action / 15 param
% 16 eng / 17 delta / 18 V / 19-24 ACT / 25-30 PA / 31-36 deltaACT
% 37-42 Q / 43 RPEQ / 44 varCriticDelta / 45 sigma / 46-51 varwA
% 52-57 kalmanV / 58-63 kalmanCOV / 64 sigma2Q / 65-66 star-ltar / 67 meta
% 68 est eng / 69 est gaze sigma / 70 real gaze sigma