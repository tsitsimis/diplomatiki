import numpy as np
import matplotlib.pyplot as plt


theta_c = 10
phi_c = 5

sigma_e = 20
theta = np.random.normal(theta_c, sigma_e, 20)
phi = np.random.normal(phi_c, sigma_e, 20)

fig = plt.figure(facecolor=".6")
ax = fig.add_subplot(111, axisbg=".6")

plt.xlim([-30, 30])
plt.ylim([-60, 60])
ax.scatter(theta, phi, c='red')
plt.show()


