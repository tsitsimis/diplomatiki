\documentclass[journal, twocolumn]{IEEEtran}


\usepackage{fontspec}
\setromanfont{FreeSerif}
\setsansfont{FreeSans}
\setmonofont{FreeMono}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{bm}
\usepackage{cite}
\usepackage{flushend}
\usepackage{booktabs}
\usepackage{float}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage{amsmath}
\usepackage[]{algpseudocode}
\usepackage{multirow}
\usepackage{caption}
\usepackage[table]{xcolor}

\setlength\parindent{0em} % Don't indent paragraphs.

\begin{document}
	
\title{Engagement Estimation}
\author{}
\maketitle

%% TODO
% + sign in reward function 

\section{Engagement Estimation Process in the V-REP HRI simulation}
In order to make the simulated HRI scenario even more realistic and obtain a more reliable assessment on the applicability of the developed learning algorithms in real use-case scenarios, we consider the human engagement to be unknown and further assume that an estimation process is in place based on non-verbal cues expressed by the human partner during human-robot interactions. The goal is to conduct an initial evaluation as to how scalable and generalizable the proposed learning algorithms are in more close to real-life scenarios and how the presence of an uncertainty on human engagement estimation may affect the performance of the system.

%In order to make our simulations more realistic, we consider the human engagement to be unknown and attempt to estimate it through non-verbal cues expressed by the human partner during human-robot interactions.
As mentioned in [Anzalone et al., 2015, Ooko et al., 2011], head pose data is proved to be highly correlated with human's engagement. In particular, clustering of the gaze, head stability as well as head pose and its variance constitute important features for the evaluation of human engagement in face-to-face, interactive scenarios. In the current implementation of our simulations, the human head pose changes according to his engagement. More specifically, pitch and yaw angles of the head are each generated by sampling a normal distribution centered around the position of the object pointed by the robot. The distribution's standard deviation is inversely proportional to human engagement. Thus, when the engagement drops, the head pose variance increases, meaning that the human is disengaged from the task and starts looking around. On the contrary, when engagement is high the attention of the person is focused on the pointed object which results in a stable head with low variance. This dependence is illustrated in Figure \ref{fig:head_var}.

\begin{figure}[htbp]
	\centering
	%\floatconts
	\includegraphics[width=8cm]{./figures/png/head_var}
	\caption{Left: Low head pose variance. Human engagement is high. Right: High head pose variance. Human engagement is low.}
	\label{fig:head_var}
\end{figure}

The engagement estimation is achieved by measuring the mean standard deviation (MSD) of the human's head pitch and yaw angles with respect to the cube's location projected on the pitch-yaw plane pointed by the robot in a specified time window. In particular, the robot collects $n$ observations of human's head pose before selecting and executing a new action. Given that the head pose is measured by visual means, the measurement error is taken into account and modeled as an additive Gaussian noise with zero mean and standard deviation $\sigma$ that depends on the accuracy of the visual head pose estimation.

The presence of uncertainties in observed measurements (head pose) for the estimation of an unknown variable (engagement) pushed us to use a Kalman filter for producing more accurate engagement estimates. Since the engagement model is unknown to the robot, the prediction step of the Kalman filtering process considers an engagement estimate based on the head pose variance. Specifically, we consider a reengagement function similar to [refer to H(x)] given by

\begin{equation}
\hat{H}(s_p, s_y)=2(exp(-k_1 \cdot s_p -k_2 \cdot s_y) - 0.5)
\end{equation}

where $s_p$ and $s_y$ are the head pitch and yaw MSD from the pointed cube in a time window and $k_1$, $k_2$ are positive constants. It is clear that $\hat{H}$ is maximized when the head pose variance is zero, or when human engagement is maximum. The estimated engagement increases up to 10 when the human's head pose variance is low ($\hat{H}(s_p, s_y) > 0$) and decreases down to 0 otherwise ($\hat{H}(s_p, s_y) < 0$). Of course this is a rough and unreliable estimate of the engagement, since it is based on noisy data. However, accuracy improves in the update step, where the measurement noise is taken into account and the estimate is updated according to the optimal Kalman gain. Figure \ref{fig:estimated_real_eng2} shows the accuracy of the engagement estimation when the robot collects 5 head pose observations per trial and the measurement noise has $\sigma=0.5$. After the human engagement is evaluated through the described process, it is provided to the robot as a reward. The reward function now considers the estimated engagement $\hat{e}$ and is computed as $r(t+1) = (1-\lambda)\hat{e}(t+1) + \lambda \Delta \hat{e}(t+1)$.

\begin{figure}[htbp]
	\centering
	%\floatconts
	\includegraphics[trim={3.2cm 8.5cm 3.8cm 9cm},clip,width=2.5in]{./figures/pdf/estimated_real_eng2}
	\caption{Real (simulated) vs. estimated engagement based on $n = 5$ head pose observations per trial and Gaussian measurement noise with $\sigma = 0.5$.}
	\label{fig:estimated_real_eng2}
\end{figure}

The first series of numerical experiments that we conducted involves step changes in the optimal (continuous) action parameter performed every 100 trials. A series of 20 runs for the same step-change scenario has been conducted and the results are shown in Figure \ref{fig:eng_param_20exp_1to4to0}. In this situation the optimal action parameter increases from 1 to 4 (300\% increase) and after 100 trials drops to 0. We calculated the mean value and standard deviation of the actual executed action parameter and the human engagement at every timestep. The results indicate that although the optimal action parameter initially quadrupled, the adaptation was fast enough to keep the engagement above a 70\% value and consistently make it converge to a value above 90\% after approximately 25 trials. In the next 100 trials the action parameter change was larger (dropped from 4 to 0) leading to a slightly wider engagement drop.

\begin{figure}[htbp]
	\centering
	%\floatconts
	\includegraphics[trim={5.2cm 7.5cm 5.2cm 6.5cm},clip,width=2.5in]{./figures/pdf/eng_param_20exp_1to4to0}
	\caption{Top: Executed action parameter (mean and variance after 20 runs) involving step changes. Bottom: Real (simulated) human engagement (mean and variance after 20 runs) when the optimal action parameter undergoes step changes every 100 trials as shown in bottom figure (engagement does not drop below a 60\% value).}
	\label{fig:eng_param_20exp_1to4to0}
\end{figure}

During a real human-robot interaction task, it is natural for a person and much more for a child to be distracted by an external event (loud noise, presence of other people, etc). We simulate such a perturbation as an abrupt and short in time(impulse-type) change of the optimal action parameter. The behavior of the algorithm is depicted in Figure \ref{fig:param_eng_pert1_2_5_10} for various durations of the perturbation impulse that begin at trial 100. In these experiments the optimal action parameter has a value of 1 that is changed to 5 during the perturbations. As the perturbation duration increases, the engagement and action parameter deviation from their optimal values become larger. However, we observe that when the perturbation is short (1-2 trials), the executed action parameter is almost unaffected and the human engagement drop is unnoticeable. In order to further quantify the performance of the algorithm, we calculate the mean absolute deviation (MAD) of the real (simulated) engagement and the action parameter from their optimal values for perturbation duration in the range of 1 to 10 trials. Figure \ref{fig:eng_param_mad2} indicates that longer perturbations lead to slower adaptation, resulting in larger MAD values. The same results are also numerically shown in Table \ref{tab:trials90} that presents the number of trials needed for the engagement to recover 90\% of its maximal value after the end of the perturbation. It should also be highlighted, though, that as illustrated by the obtained results, no matter how long the perturbation, the
algorithm will always reconverge to the optimal value.

%The same result is derived by Tables \ref{tab:eng_max_deviation} and \ref{tab:trials90} which show the maximum engagement deviation as well as the number of trials needed for the engagement to converge back to 90\% after the perturbation.

\begin{figure}[htbp]
	\centering
	%\floatconts
	\includegraphics[trim={6cm 7.5cm 6.5cm 8cm},clip,width=2.5in]{./figures/pdf/param_eng_pert1_2_5_10}
	\caption{Action parameter (top) and human engagement (bottom) during optimal action parameter perturbations with duration of 1 (solid black), 2 (dashed black), 5 (solid gray) and 10 (dashed gray) trials respectively.}
	\label{fig:param_eng_pert1_2_5_10}
\end{figure}

\begin{figure}[htbp]
	\centering
	%\floatconts
	\includegraphics[trim={7.5cm 10cm 8cm 10cm},clip,width=2.5in]{./figures/pdf/eng_param_mad2}
	\caption{Performance for increasing perturbation duration (number of trials). Top: Engagement deviation from its maximum value (10). Bottom: Action parameter deviation from its optimal value (1). The measurement noise has $\sigma=1$.}
	\label{fig:eng_param_mad2}
\end{figure}

\begin{table}[!t]
	\centering
  	\begin{tabular}{c | c c c c}
  	%\multicolumn{5}{c}{ Number of trials to 90\%} \\
    \hline
    \multirow{2}{*}{\bf Perturbation } &
          \multirow{2}{*}{\bf Mean} &
          \multirow{2}{*}{\bf STD} &
          \multirow{2}{*}{\bf 25\%} &
          \multirow{2}{*}{\bf 75\%} \\
          \bf duration & & & \bf percentile & \bf percentile \\
    \hline
    \hline
% one decimal place in std
1&1.8&1.5&0&3\\\hline
2&3.95&1.5&3&4.5\\\hline
3&8.85&7.6&3.5&11\\\hline
4&11.6&9.5&4&15.5\\\hline
5&11.05&6.1&7&11.5\\\hline
6&11.5&5.1&8&15\\\hline
7&12.8&4.5&9.5&16\\\hline
8&15.15&6.4&11&18\\\hline
9&14.35&5.5&11&18\\\hline
10&16.45&6.6&12&19\\\hline      
    
%1&1.8&1.5079&0&3\\\hline
%2&3.95&1.4681&3&4.5\\\hline
%3&8.85&7.5692&3.5&11\\\hline
%4&11.6&9.5001&4&15.5\\\hline
%5&11.05&6.0998&7&11.5\\\hline
%6&11.5&5.0628&8&15\\\hline
%7&12.8&4.4909&9.5&16\\\hline
%8&15.15&6.4177&11&18\\\hline
%9&14.35&5.4895&11&18\\\hline
%10&16.45&6.6449&12&19\\\hline    
\\
  	\end{tabular}
  	\caption{Number of trials needed for engagement to reach 90\% of its maximal value after a perturbation.}
  	\label{tab:trials90}
\end{table}

In a similar way, Figure \ref{fig:noise_eng_param_mad2} shows the engagement and action parameter deviation for an increasing $\sigma$ of the Gaussian head pose measurement noise. In the particular experiment the perturbation duration is 1 trial. It is clear that noisier pitch and yaw head angles measurements result in larger deviations of the estimated engagement from the real (simulated) engagement (Fig. \ref{fig:noise_eng_param_mad2} top). The same holds for the action parameter whose deviation from its optimal value is proportional to the amplitude of the measurement noise (Fig. \ref{fig:noise_eng_param_mad2} bottom).
%This means that no significant engagement deviations are introduced by measurement uncertainties but mostly by human-related factors such as perturbations or optimal action parameter alternations.


\begin{figure}[!t]
	\centering
	%\floatconts
	\includegraphics[trim={6.5cm 10cm 7cm 9cm},clip,width=2.5in]{./figures/pdf/noise_eng_param_mad2}
	\caption{Performance for increasing noise $\sigma$. Top: Engagement deviation from its maximum value (10). Bottom: Action parameter deviation from its optimal value (1).}
	\label{fig:noise_eng_param_mad2}
\end{figure}



\section{Conclusion}
Then, we showed how engagement estimation is affected by the presence of measurement noise. Although the algorithm is not significantly affected by small noise amplitudes, the performance drops when uncertainties in human engagement are high as shown by the increased action parameter deviation from its optimal value.




\end{document}