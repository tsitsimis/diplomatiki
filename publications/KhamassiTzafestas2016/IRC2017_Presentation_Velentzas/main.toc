\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Parameterized Actions}{13}{0}{2}
\beamer@sectionintoc {3}{Active Exploration Algorithm}{25}{0}{3}
\beamer@sectionintoc {4}{Experiments-Simulations}{55}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{97}{0}{5}
