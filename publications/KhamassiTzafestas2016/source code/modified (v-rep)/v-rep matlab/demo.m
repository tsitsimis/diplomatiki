[theta, pdf_low_eng] = gauss_demo(2);
[~, pdf_high_eng] = gauss_demo(8);

myColors = lines;
myBlue = myColors(1, :);
myRed = myColors(2, :);
myGreen = myColors(5, :);

figure;
hold on;
mu1 = pi/10;
plot([mu1 mu1], [0 max(pdf_high_eng)], '--', 'color', myRed);

plot(theta, pdf_low_eng, 'color', myBlue, 'linewidth', 2);
plot(theta, pdf_high_eng, 'color', myGreen, 'linewidth', 2);

xlabel('gaze direction (rad)');

legend('robot''s action', 'low engagement', 'high engagement');