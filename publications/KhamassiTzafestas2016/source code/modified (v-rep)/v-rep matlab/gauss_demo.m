function [theta, normalDistr] = gauss_demo(eng)
    theta = -(pi/3):0.01:(pi/3);

    % mean: current action, sigma ~ 1/eng
    mu = pi/10;

    sigmaMin = 0.04;
    sigmaMax = 0.5;
    engMax = 10;
    engThres = 0;

    sigma = eng2sigma(eng, sigmaMin, sigmaMax, engThres, engMax);
    normalDistr = normpdf(theta, mu, sigma);
    normalDistr = normalDistr / sum(normalDistr);
end