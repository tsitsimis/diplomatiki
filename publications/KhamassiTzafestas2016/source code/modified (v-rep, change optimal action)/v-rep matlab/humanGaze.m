function neckAngle = humanGaze(world, eng, action)
    vrep = world.vrep;
    clientID = world.clientID;
    Bill = world.Bill;
    angles = Bill.angles;
    
    opMode = world.oneshot_wait;
    
    %% Gaussian distribution of gazes
    % interval of Bill's neck angles
    theta = -(pi/3):0.01:(pi/3);
    
    % mean: current action, sigma ~ 1/eng
    mu = angles(action);
    
    sigmaMin = 0.04;
    sigmaMax = 0.5;
    engMax = 10;
    engThres = 0;
    
    %sigma = sigmaMax - (eng - engThres) * (sigmaMax - sigmaMin) / (engMax - engThres);
    sigma = eng2sigma(eng, sigmaMin, sigmaMax, engThres, engMax);
    normalDistr = normpdf(theta, mu, sigma);
    normalDistr = normalDistr / sum(normalDistr);
    
    %% Sample the Gaussian distr.
    if eng > engThres % normal distribution around current action
        neckAngle = theta(drand01(normalDistr));
        %fprintf('mu: %d, sigma: %d, neckAngle: %d \n', mu, sigma, rad2deg(neckAngle));
    else % random gaze (uniform distribution)
        neckAngle = theta(randi([1, length(theta)], 1, 1));
        %fprintf('random gaze, neckAngle: %d \n', rad2deg(neckAngle));
    end
    
    %% smooth neck transition from old to new angle
    [~, oldAngle] = vrep.simxGetJointPosition(clientID, Bill.NeckHandle, opMode);
    %n = 30;
    n = ceil(abs(neckAngle - oldAngle) / (pi/50));
    gazeAngles = linspace(oldAngle, neckAngle, n);
    
    for i = 1:n
        vrep.simxSetJointPosition(clientID, Bill.NeckHandle, gazeAngles(i), opMode);
        pause(0.02);
    end
    
end