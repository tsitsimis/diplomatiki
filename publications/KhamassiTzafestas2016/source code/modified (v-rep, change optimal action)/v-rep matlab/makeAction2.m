function makeAction2(world, cube)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    
    opMode = world.oneshot;
    opModeWait = world.oneshot_wait;
    
    angles = [-45, -30, -20, -4, 8, 18];
    
    % close arm
    vrep.simxSetJointTargetPosition(clientID, NAO.RElbowRoll3Handle, ...
        deg2rad(88), opMode);
    
    vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, ...
        deg2rad(18), opMode);
    
    pause(0.4);
    
    % move shoulder
    vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, ...
        deg2rad(angles(cube)), opMode);
    
    % move head
    vrep.simxSetJointTargetPosition(clientID, NAO.HeadYawHandle, ...
        deg2rad(angles(cube)), opMode);
    
    % open elbow
    vrep.simxSetJointTargetPosition(clientID, NAO.RElbowRoll3Handle, ...
        deg2rad(2), opMode);
end