function world = putNAOhandDown(world, hand)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    
    opMode = world.oneshot_wait;
    
    pauseTime = 1;

    if strcmp(hand, 'left')
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(76), opMode);
        pause(pauseTime);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, deg2rad(90), opMode);
        pause(pauseTime/2);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, 0, opMode);
        
        NAO.LHand = 0;
    elseif strcmp(hand, 'right')
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, deg2rad(-76), opMode);
        pause(pauseTime);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, deg2rad(90), opMode);
        pause(pauseTime/2);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, 0, opMode);
        
        NAO.RHand = 0;
    elseif strcmp(hand, 'both')
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(76), opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, deg2rad(-76), opMode);
        pause(pauseTime);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, deg2rad(90), opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, deg2rad(90), opMode);
        pause(pauseTime/2);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, 0, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, 0, opMode);
        
        NAO.LHand = 0;
        NAO.RHand = 0;
    end
    
    %pause(1);
    world.NAO = NAO;
end