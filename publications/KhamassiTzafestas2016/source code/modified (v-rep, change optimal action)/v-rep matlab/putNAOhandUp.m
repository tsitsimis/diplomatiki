function world = putNAOhandUp(world, hand)
    vrep = world.vrep;
    clientID = world.clientID;
    NAO = world.NAO;
    
    opMode = world.oneshot_wait;
    
    pauseTime = 1;

    if strcmp(hand, 'left')
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(76), opMode);
        %pause(pauseTime);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, 0, opMode);
%         pause(1);
%         vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, 0, opMode);
        
        NAO.LHand = 1;
    elseif strcmp(hand, 'right')
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, -deg2rad(76), opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, deg2rad(0), opMode);
        pause(1);
        vrep.simxSetJointTargetPosition(clientID,...
            NAO.RElbowRoll3Handle, deg2rad(88), opMode);
        
        vrep.simxSetJointTargetPosition(clientID,...
            NAO.RShoulderRoll3Handle, deg2rad(0), opMode);
        
        NAO.RHand = 1;
    elseif strcmp(hand, 'both')
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, deg2rad(76), opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, -deg2rad(76), opMode);
        %pause(pauseTime);
        vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderPitch3Handle, 0, opMode);
        vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderPitch3Handle, 0, opMode);
%         pause(1);
%         vrep.simxSetJointTargetPosition(clientID, NAO.LShoulderRoll3Handle, 0, opMode);
%         vrep.simxSetJointTargetPosition(clientID, NAO.RShoulderRoll3Handle, 0, opMode);
        
        NAO.LHand = 1;
        NAO.RHand = 1;
    end
    
    pause(pauseTime);
    world.NAO = NAO;
end