function sigma = eng2sigma(eng, sigmaMin, sigmaMax, engThres, engMax)
    sigma = sigmaMax - (eng - engThres) * (sigmaMax - sigmaMin) / (engMax - engThres);
end