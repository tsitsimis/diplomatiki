%% main

%clear all
close all
%clc

whichModel = 5;
runOptimizedModels = 1;
episodeLength = 1000;
vertiBars = false;

% define Task
BBT = BBsetTask();

% define baby robot
BBR = BBrobot(BBT);

% initialize model optimized parameters
if (runOptimizedModels)
    load('bestModelsOptiSummer2016.mat')
    BBR = BBinitModelParam( BBR, bestModels(whichModel,1:10), whichModel );
end

% initial state
s = BBT.P0;

% initial action by the robot
[BBR, a] = BBrobotDecides(BBT, BBR, whichModel, s);

% init LOGS
LOG_FILES = [s a.action a.param 0 s a.action a.param BBT.cENG BBR.delta BBR.VC BBR.ACT BBR.PA zeros(1, BBR.nA) zeros(1, BBR.nA) 0 BBR.varDelta BBR.sigma BBR.varACT BBR.kalmanV diag(BBR.kalmanCOV)' BBR.sigma2Q(1) 0 0 0];
LOG_FILES_ = LOG_FILES;
%% RUN TASK

% nExperiments = 1;
% nTrialsEng = zeros(1, nExperiments); % trials needed for engagement convergence
% nTrialsParam = zeros(1, nExperiments); % trials needed for optimal parameter convergence
% for j = 1:nExperiments
%     fprintf('Experiment: %d \n', j);

BBT_ = BBT;
BBR_ = BBR;
s_ = s;
a_ = a;
    
for iii=1:episodeLength
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta]];
    
    [ BBT_, BBR_, s_, a_, logs_ ] = BBrunTrial_no_meta( BBT_, BBR_, whichModel, s_, a_ );
    LOG_FILES_ = [LOG_FILES_ ; [logs_.s logs_.oldaction logs_.oldparam logs_.reward logs_.y logs_.action logs_.param logs_.engagement logs_.delta logs_.VC logs_.ACT logs_.PA logs_.deltaACT logs_.Q logs_.RPEQ logs_.varDelta logs_.sigma logs_.dwA logs_.kalmanV logs_.kalmanCOV logs_.sigma2Q logs_.star logs_.ltar logs_.meta]];
end

% change of optimal parameter
BBT.engMu = -25;
BBT_.engMu = -25;
for iii=1:episodeLength
    [ BBT, BBR, s, a, logs ] = BBrunTrial( BBT, BBR, whichModel, s, a );
    LOG_FILES = [LOG_FILES ; [logs.s logs.oldaction logs.oldparam logs.reward logs.y logs.action logs.param logs.engagement logs.delta logs.VC logs.ACT logs.PA logs.deltaACT logs.Q logs.RPEQ logs.varDelta logs.sigma logs.dwA logs.kalmanV logs.kalmanCOV logs.sigma2Q logs.star logs.ltar logs.meta]];
    
    [ BBT_, BBR_, s_, a_, logs_ ] = BBrunTrial( BBT_, BBR_, 2, s_, a_ );
    LOG_FILES_ = [LOG_FILES_ ; [logs_.s logs_.oldaction logs_.oldparam logs_.reward logs_.y logs_.action logs_.param logs_.engagement logs_.delta logs_.VC logs_.ACT logs_.PA logs_.deltaACT logs_.Q logs_.RPEQ logs_.varDelta logs_.sigma logs_.dwA logs_.kalmanV logs_.kalmanCOV logs_.sigma2Q logs_.star logs_.ltar logs_.meta]];
end

%end

% LOG_FILES
% 1-5 s / 6 old action / 7 old param / 8 r / 9-13 y / 14 action / 15 param
% 16 eng / 17 delta / 18 V / 19-24 ACT / 25-30 PA / 31-36 deltaACT
% 37-42 Q / 43 RPEQ / 44 varCriticDelta / 45 sigma / 46-51 varwA
% 52-57 kalmanV / 58-63 kalmanCOV / 64 sigma2Q / 65-66 star-ltar / 67 meta

%% README
switch (whichModel)
    case 1
        % simple Q-learning, fixed sigma:
        README_PARAMETERS = ['TASK: minENG ' num2str(BBT.minENG) ', maxENG ' num2str(BBT.maxENG) ', forget ' num2str(BBT.forget) ', reeng ' num2str(BBT.reeng) '. Gaussian Child Engagement, engMu ' num2str(BBT.engMu) ', engSig ' num2str(BBT.engSig) '. Rwd = (1 - lambdaRwd) * absolute child engagement + lambdaRwd * variation in child engagement. lambdaRwd = ' num2str(BBT.lambdaRwd) '. Optimal action 2 then 6 then 2 then 6 etc.. Caluwaerts learning rule (all deltas, delta*alphaA), action selection based on Q-values. No exploration bonus. Robot alphaC ' num2str(BBR.alphaC) ' alphaA ' num2str(BBR.alphaA) ' alphaQ ' num2str(BBR.alphaQ) ' beta ' num2str(BBR.beta) ' gamma ' num2str(BBR.gamma) ' softmax. Kalman QL with eta ' num2str(BBR.eta) ' kappa ' num2str(BBR.kappa) ' varObs ' num2str(BBR.varObs) ' initCov ' num2str(BBR.initCov) '. Schweighofer metalearning: tau1 ' num2str(BBR.tau1) ' tau2 ' num2str(BBR.tau2) ' mu ' num2str(BBR.mu) '. No active exploration. Fixed sigma ' num2str(BBR.sigma) '.'];
    case 2
        % Kalman-Q active exploration:
        README_PARAMETERS = ['TASK: minENG ' num2str(BBT.minENG) ', maxENG ' num2str(BBT.maxENG) ', forget ' num2str(BBT.forget) ', reeng ' num2str(BBT.reeng) '. Gaussian Child Engagement, engMu ' num2str(BBT.engMu) ', engSig ' num2str(BBT.engSig) '. Rwd = (1 - lambdaRwd) * absolute child engagement + lambdaRwd * variation in child engagement. lambdaRwd = ' num2str(BBT.lambdaRwd) '. Optimal action 2 then 6 then 2 then 6 etc.. Caluwaerts learning rule (all deltas, delta*alphaA), action selection based on KalmanV+diag(BBR.kalmanCOV)*explorationBonus ' num2str(BBR.explorationBonus) '. Robot alphaC ' num2str(BBR.alphaC) ' alphaA ' num2str(BBR.alphaA) ' alphaQ ' num2str(BBR.alphaQ) ' beta ' num2str(BBR.beta) ' gamma ' num2str(BBR.gamma) ' softmax. Kalman QL with eta ' num2str(BBR.eta) ' kappa ' num2str(BBR.kappa) ' varObs ' num2str(BBR.varObs) ' initCov ' num2str(BBR.initCov) '. Schweighofer metalearning: tau1 ' num2str(BBR.tau1) ' tau2 ' num2str(BBR.tau2) ' mu ' num2str(BBR.mu) '. Active exploration for dynamic sigma based on kalmanCOV*' num2str(BBR.gainSigma) '.'];
    case 3
        % sigma2Q-based active exploration:
        README_PARAMETERS = ['TASK: minENG ' num2str(BBT.minENG) ', maxENG ' num2str(BBT.maxENG) ', forget ' num2str(BBT.forget) ', reeng ' num2str(BBT.reeng) '. Gaussian Child Engagement, engMu ' num2str(BBT.engMu) ', engSig ' num2str(BBT.engSig) '. Rwd = (1 - lambdaRwd) * absolute child engagement + lambdaRwd * variation in child engagement. lambdaRwd = ' num2str(BBT.lambdaRwd) '. Optimal action 2 then 6 then 2 then 6 etc.. Caluwaerts learning rule (all deltas, delta*alphaA), action selection based on Q-values+sigma2Q*explorationBonus ' num2str(BBR.explorationBonus) '. Robot alphaC ' num2str(BBR.alphaC) ' alphaA ' num2str(BBR.alphaA) ' alphaQ ' num2str(BBR.alphaQ) ' beta ' num2str(BBR.beta) ' gamma ' num2str(BBR.gamma) ' softmax. Kalman QL with eta ' num2str(BBR.eta) ' kappa ' num2str(BBR.kappa) ' varObs ' num2str(BBR.varObs) ' initCov ' num2str(BBR.initCov) '. Schweighofer metalearning: tau1 ' num2str(BBR.tau1) ' tau2 ' num2str(BBR.tau2) ' mu ' num2str(BBR.mu) '. Active exploration for dynamic sigma based on sigma2Q*' num2str(BBR.gainSigma) '.'];
    case 4
        % Hybrid active exploration:
        README_PARAMETERS = ['TASK: minENG ' num2str(BBT.minENG) ', maxENG ' num2str(BBT.maxENG) ', forget ' num2str(BBT.forget) ', reeng ' num2str(BBT.reeng) '. Gaussian Child Engagement, engMu ' num2str(BBT.engMu) ', engSig ' num2str(BBT.engSig) '. Rwd = (1 - lambdaRwd) * absolute child engagement + lambdaRwd * variation in child engagement. lambdaRwd = ' num2str(BBT.lambdaRwd) '. Optimal action 2 then 6 then 2 then 6 etc.. Caluwaerts learning rule (all deltas, delta*alphaA), action selection based on Q-values+sigma2Q*explorationBonus ' num2str(BBR.explorationBonus) '. Robot alphaC ' num2str(BBR.alphaC) ' alphaA ' num2str(BBR.alphaA) ' alphaQ ' num2str(BBR.alphaQ) ' beta ' num2str(BBR.beta) ' gamma ' num2str(BBR.gamma) ' softmax. Kalman QL with eta ' num2str(BBR.eta) ' kappa ' num2str(BBR.kappa) ' varObs ' num2str(BBR.varObs) ' initCov ' num2str(BBR.initCov) '. Schweighofer metalearning: tau1 ' num2str(BBR.tau1) ' tau2 ' num2str(BBR.tau2) ' mu ' num2str(BBR.mu) '. Hybrid active exploration for dynamic sigma based on sum of sigmoids on sigma2Q and metaparam parametrized by gainSigma ' num2str(BBR.gainSigma) '.'];
    case {5,6}
        % schweighofer metalearning-based active explortion:
        README_PARAMETERS = ['TASK: minENG ' num2str(BBT.minENG) ', maxENG ' num2str(BBT.maxENG) ', forget ' num2str(BBT.forget) ', reeng ' num2str(BBT.reeng) '. Gaussian Child Engagement, engMu ' num2str(BBT.engMu) ', engSig ' num2str(BBT.engSig) '. Rwd = (1 - lambdaRwd) * absolute child engagement + lambdaRwd * variation in child engagement. lambdaRwd = ' num2str(BBT.lambdaRwd) '. Optimal action 2 then 6 then 2 then 6 etc.. Caluwaerts learning rule (all deltas, delta*alphaA), action selection based on Q-values. Robot alphaC ' num2str(BBR.alphaC) ' alphaA ' num2str(BBR.alphaA) ' alphaQ ' num2str(BBR.alphaQ) ' beta ' num2str(BBR.beta) ' gamma ' num2str(BBR.gamma) ' softmax. Kalman QL with eta ' num2str(BBR.eta) ' kappa ' num2str(BBR.kappa) ' varObs ' num2str(BBR.varObs) ' initCov ' num2str(BBR.initCov) '. Schweighofer metalearning: tau1 ' num2str(BBR.tau1) ' tau2 ' num2str(BBR.tau2) ' mu ' num2str(BBR.mu) '. Active exploration for dynamic sigma based on (10 * (1 / (1 + exp(BBR.metaparam)) - 0.5)) + 0.25) *' num2str(BBR.gainSigma) '.'];
end

%% DEBUGGING
% debuug = (LOG_FILES(:,6)==2)&(LOG_FILES(:,7)>=10)&(LOG_FILES(:,7)<=15);
% size(LOG_FILES(debuug,:))
% pointeur = 0;
% pointeur = argmax(debuug(pointeur+1:end)) + pointeur
% LOG_FILES(pointeur-1:pointeur+1,[6:8 17 32 19:24])



% engagement and action param comparison
figure;
box on;
subplot(2,2,1);
plot(LOG_FILES_(:,16), 'linewidth', 2);
hold on;
plot(LOG_FILES(:,16), 'linewidth', 2);
title('engagement', 'fontWeight', 'normal');
legend('Kalman-QL', 'meta-learning');
axis([900, 1700, -0.5, 10.5]);

subplot(2,2,3);
plot(LOG_FILES_(:,15), 'linewidth', 2);
hold on;
plot(LOG_FILES(:,15), 'linewidth', 2);
title('action parameter', 'fontWeight', 'normal');
legend('Kalman-QL', 'meta-learning');
axis([900, 1700, -70, 40]);

my_red = [0.85, 0.33, 0.1];
subplot(2,2,2);
plot(LOG_FILES(:,16), 'linewidth', 2, 'color', my_red);
% axis([998, 1029, -0.5, 10.5]);
axis([900, 1700, -0.5, 10.5]);

subplot(2,2,4);
plot(LOG_FILES(:,15), 'linewidth', 2, 'color', my_red);
% axis([998, 1029, -70, 40]);
axis([900, 1700, -70, 40]);

% engagement and action param (zoom)
% orange = [0.8500, 0.3250, 0.0980];
% figure;
% subplot(2,1,1);
% %plot(LOG_FILES_(:,16), 'linewidth', 2);
% %hold on;
% plot(LOG_FILES(:,16), 'linewidth', 2, 'Color', orange);
% grid on;
% title('engagement');
% axis([999, 1028, 7, 10.5]);
% 
% subplot(2,1,2);
% %plot(LOG_FILES_(:,15), 'linewidth', 2);
% %hold on;
% plot(LOG_FILES(:,15), 'linewidth', 2, 'Color', orange);
% grid on;
% title('action parameter');
% axis([999, 1028, -70, -10]);