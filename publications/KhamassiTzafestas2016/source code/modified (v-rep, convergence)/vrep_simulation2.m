close all;
clear;
clc;

addpath(genpath('v-rep binding'));
addpath(genpath('v-rep matlab'));

vrep = remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
vrep.simxFinish(-1); % just in case, close all opened connections
clientID = vrep.simxStart('127.0.0.1', 19999, true, true, 5000, 5);

if (clientID > -1)
    disp('Connected to remote API server');
    
    opMode = vrep.simx_opmode_oneshot_wait;
    
    %% Object Handles
    % *********** NAO ***********
    % NAO
    [~, NAOHandle] = vrep.simxGetObjectHandle(clientID, 'NAO', vrep.simx_opmode_blocking);
    
    % Left hand
    [~, LShoulderPitch3Handle] = vrep.simxGetObjectHandle(clientID, 'LShoulderPitch3', vrep.simx_opmode_blocking);
    [~, LShoulderRoll3Handle] = vrep.simxGetObjectHandle(clientID, 'LShoulderRoll3', vrep.simx_opmode_blocking);
    [~, LElbowRoll3Handle] = vrep.simxGetObjectHandle(clientID, 'LElbowRoll3', vrep.simx_opmode_blocking);
    
    % Right hand
    [~, RShoulderPitch3Handle] = vrep.simxGetObjectHandle(clientID, 'RShoulderPitch3', vrep.simx_opmode_blocking);
    [~, RShoulderRoll3Handle] = vrep.simxGetObjectHandle(clientID, 'RShoulderRoll3', vrep.simx_opmode_blocking);
    [~, RElbowRoll3Handle] = vrep.simxGetObjectHandle(clientID, 'RElbowRoll3', vrep.simx_opmode_blocking);
    
    % Head
    [~, HeadYawHandle] = vrep.simxGetObjectHandle(clientID, 'HeadYaw', vrep.simx_opmode_blocking);
    
    NAO.NAOHandle = NAOHandle;
    NAO.LShoulderPitch3Handle = LShoulderPitch3Handle;
    NAO.LShoulderRoll3Handle = LShoulderRoll3Handle;
    NAO.RShoulderPitch3Handle = RShoulderPitch3Handle;
    NAO.RShoulderRoll3Handle = RShoulderRoll3Handle;
    NAO.HeadYawHandle = HeadYawHandle;
    
    % *********** Cubes ***********
    nCubes = 6;
    [~, Cuboid1Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_1', vrep.simx_opmode_blocking);
    [~, Cuboid2Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_2', vrep.simx_opmode_blocking);
    [~, Cuboid3Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_3', vrep.simx_opmode_blocking);
    [~, Cuboid4Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_4', vrep.simx_opmode_blocking);
    [~, Cuboid5Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_5', vrep.simx_opmode_blocking);
    [~, Cuboid6Handle] = vrep.simxGetObjectHandle(clientID, 'Cuboid_6', vrep.simx_opmode_blocking);
    
    % *********** Human (Bill) ***********
    [~, BillHandle] = vrep.simxGetObjectHandle(clientID, 'Bill', vrep.simx_opmode_blocking);
    [~, BillNeckHandle] = vrep.simxGetObjectHandle(clientID, 'Bill_neck', vrep.simx_opmode_blocking);
    [~, BillAnkleHandle] = vrep.simxGetObjectHandle(clientID, 'Bill_ankleJoint', vrep.simx_opmode_blocking);
    
    Bill.Handle = BillHandle;
    Bill.NeckHandle = BillNeckHandle;
    Bill.AnkleHandle = BillAnkleHandle;
    
    %% initializations
    % get NAO position
    [~, NAOpos] = vrep.simxGetObjectPosition(clientID, NAOHandle, -1, opMode);
    NAO.NAOpos = NAOpos;
    
    % get angles of cubes relative to NAO
    cubePos = zeros(nCubes, 3);
    [~, cubePos(1, :)] = vrep.simxGetObjectPosition(clientID, Cuboid1Handle, -1, opMode);
    [~, cubePos(2, :)] = vrep.simxGetObjectPosition(clientID, Cuboid2Handle, -1, opMode);
    [~, cubePos(3, :)] = vrep.simxGetObjectPosition(clientID, Cuboid3Handle, -1, opMode);
    [~, cubePos(4, :)] = vrep.simxGetObjectPosition(clientID, Cuboid4Handle, -1, opMode);
    [~, cubePos(5, :)] = vrep.simxGetObjectPosition(clientID, Cuboid5Handle, -1, opMode);
    [~, cubePos(6, :)] = vrep.simxGetObjectPosition(clientID, Cuboid6Handle, -1, opMode);
    
    anglesN = zeros(1, nCubes);
    for i = 1:nCubes
        anglesN(i) = atan2(cubePos(i, 2) - NAOpos(2), cubePos(i, 1) - NAOpos(1));
    end
    NAO.angles = anglesN;
    
    NAO.LHand = 0;
    NAO.RHand = 0;
    
    % get Bill position
    [~, Billpos] = vrep.simxGetObjectPosition(clientID, BillHandle, -1, opMode);
    [~, BillNeckpos] = vrep.simxGetObjectPosition(clientID, BillNeckHandle, -1, opMode);
    Bill.Billpos = Billpos;
    Bill.BillNeckpos = BillNeckpos;
    
    % Bill gaze angles
    anglesB = zeros(1, nCubes);
    for i = 1:nCubes
        anglesB(i) = atan2(BillNeckpos(2)-cubePos(i, 2), BillNeckpos(1)-cubePos(i, 1)) - pi/2;
    end
    Bill.angles = anglesB;
    
    % set v-rep world variables
    world.vrep = vrep;
    world.clientID = clientID;
    world.NAO = NAO;
    world.Bill = Bill;
    world.oneshot_wait = vrep.simx_opmode_oneshot_wait;
    world.oneshot = vrep.simx_opmode_oneshot;
    
    putNAOhandUp(world, 'right');
    
    %% Run main code
    main
    
    %% exit
    % Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
	vrep.simxGetPingTime(clientID);

	% Now close the connection to V-REP:	
	vrep.simxFinish(clientID);
else
	disp('Failed connecting to remote API server');
end

vrep.delete(); % call the destructor!	
disp('Program ended');