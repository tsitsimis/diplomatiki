function neckAngle = humanGaze(world, eng, action)
    vrep = world.vrep;
    clientID = world.clientID;
    Bill = world.Bill;
    angles = Bill.angles;
    
    opModeWait = vrep.simx_opmode_oneshot_wait;
    
    %% Gaussian distribution of gazes
    % interval of Bill's neck angles
    theta_min = -pi/3;
    theta_max = pi/3;
    theta = theta_min:0.01:theta_max;
    
    % mean: current action, sigma ~ 1/eng
    mu = angles(action);
    
    sigmaMin = 0.01;
    sigmaMax = 0.5;
    engThres = 0.1;
    engMax = 10;
    sigma = eng2sigma(eng, sigmaMin, sigmaMax, engThres, engMax);
%     normalDistr = normpdf(theta, mu, sigma);
%     normalDistr = normalDistr / sum(normalDistr);
    
    %% Sample the Gaussian distr.
    if eng > engThres % normal distribution around current action
        %neckAngle = theta(drand01(normalDistr));
        neckAngle = normrnd(mu, sigma);
        neckAngle = max(neckAngle, theta_min);
        neckAngle = min(neckAngle, theta_max);
        %fprintf('mu: %d, sigma: %d, neckAngle: %d \n', mu, sigma, rad2deg(neckAngle));
    else % random gaze (uniform distribution)
        neckAngle = theta(randi([1, length(theta)]));
        %fprintf('random gaze, neckAngle: %d \n', rad2deg(neckAngle));
    end
    
    %% smooth neck transition from old to new angle
%     [~, oldAngle] = vrep.simxGetJointPosition(clientID, Bill.NeckHandle, opModeWait);
%     n = 30;
%     gazeAngles = linspace(oldAngle, neckAngle, n);
%     
%     for i = 1:n
%         vrep.simxSetJointPosition(clientID, Bill.NeckHandle, gazeAngles(i), opModeWait);
%     end
    
end