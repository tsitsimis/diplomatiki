\contentsline {section}{Executive Summary}{5}{section*.1}
\contentsline {section}{\numberline {1}Introduction}{7}{section.1}
\contentsline {section}{\numberline {2}Overview of the robot learning and control architecture}{8}{section.2}
\contentsline {section}{\numberline {3}Gestural kinematics: Teaching robot gestures by human demonstration}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Dynamic motor primitives framework}{11}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Motion recording setup and initial results}{12}{subsection.3.2}
\contentsline {section}{\numberline {4}Meta-learning and on-line adaptation in human-robot interaction environments}{14}{section.4}
\contentsline {subsection}{\numberline {4.1}Active exploration and parameterised reinforcement learning to maximise engagement in a simulated human-robot interaction scenario}{15}{subsection.4.1}
\contentsline {subsubsection}{Engagement Estimation Process in the simulated HRI scenario}{18}{subsubsection*.2}
\contentsline {subsection}{\numberline {4.2}Meta-learning in non-stationary multi-armed bandit tasks: Benchmark comparative evaluation}{21}{subsection.4.2}
\contentsline {section}{\numberline {5}Multi-agent reinforcement learning to enhance core robotic functionality}{23}{section.5}
\contentsline {subsection}{\numberline {5.1}Softened Approximate Policy Iteration for Markov Games}{24}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Learning Nash Equilibrium for General-Sum Markov Games from Batch Data}{25}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Batch Policy Iteration for Continuous Domains.}{27}{subsection.5.3}
\contentsline {section}{\numberline {6}Summary}{28}{section.6}
\contentsline {section}{\numberline {7}Future Research Workplan}{29}{section.7}
\contentsline {section}{Annex}{38}{section*.3}
