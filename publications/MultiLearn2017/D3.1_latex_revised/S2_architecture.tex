The first goal of  WP3 Task 3.4 ("Behavior-based control architecture") was to design and develop a behavior-based control architecture for the humanoid robot platforms (i.e. the Zeno robot), which incorporates different levels of abstraction and learning. This architecture aims at supporting different adaptation and developmental learning schemes, enabling the system to optimize control policies on the fly based on cognitive feedback provided by human action and intention recognition modules developed in the frames of WP2.

Figure \ref{fig:archi} presents the envisaged global learning and decision-making architecture of WP3. It is decomposed in the three levels:
\begin{itemize}
\item The \textbf{child monitoring level} constitutes the highest level of the hierarchy. It processes various cognitive feedback signals provided by human action and intention recognition modules developed in the frames of WP2. Its goal is to monitor each child's specific behavioral statistics (in order to achieve appropriate robot behavior) as well as current engagement in the task (in order to achieve online adaptation to variations in the child's engagement and behavior).
\item The \textbf{action level} constitutes the intermediate level of the hierarchy. Its goal is the choose at each timestep the discrete action to perform (or discrete action plan in the case of prospective planning) in order to maximize extrinsic rewards (achievement of the task) and social intrinsic rewards (joint engagement in the task). This level uses cognitive monitoring signals analyzed by the higher level and can tune continuous parameters of actions executed by the lower level in order to maximize child engagement.
\item The \textbf{movement level} constitutes the lowest level of the hierarchy. Its challenging goal is to ensure continuous learning of optimal and natural movements for the interaction (e.g. pointing, picking, placing, giving, receiving) in high dimensional continuous state and action spaces. The tools at hand include continuous reinforcement learning, inverse reinforcement learning (for imitation learning) and structured classification. This level will keep track of optimal movements over time, relying on real data.
\end{itemize}


\begin{figure}[!b] %[htbp]
	\centering
	%\floatconts
	\includegraphics[width=5.2in]{images/babyrobot_learning_architecture2}
	\caption{The global learning and decision-making architecture of BabyRobot.}
	\label{fig:archi}
	%\end{centering}
\end{figure}

One of the cognitive feedback signals that constituted a central drive for learning in the proposed architecture (see work presented in Section \ref{sec:Active}) is the current level of engagement of the child with which the robot is interacting.  The rationale here is that the robot should be able to adapt its behavior in order to maximize the child's engagement in the task, and thus to maximize the instantaneous and educational  benefits of the interaction for the child. Previous researches on human-robot interaction have highlighted the importance of taking into account the engagement of the human to facilitate the interaction \cite{anzalone15}. Nevertheless,
a robot's actions may have delayed effects on human's behavior and engagement. We thus chose here to drive some of the adaptation mechanisms developed in this workpackage by a reward function given by a dynamical system, based (in simulation) on the virtual engagement $e(t)$ of the human (which will be later provided on the fly during real interaction by human action intention recognition modules developed in the frames of WP2). This signal represents somehow the attention that the child pays to the robot, to its actions, and to the objects within the shared scene. Numerically speaking, we modeled this engagement $e(t)$ to be between 0 (no engagement) to 10 (maximum engagement), and the reward function was computed as $r(t+1)=(1-\lambda)e(t+1)-\lambda \Delta e(t+1)$ where $\lambda$ is a chosen weight (we chose 0.7 for our simulations; see Section \ref{sec:Active}).
