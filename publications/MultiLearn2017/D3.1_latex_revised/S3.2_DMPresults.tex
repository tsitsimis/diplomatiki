For the recording of the human demonstrated trajectories, we used an Ascension 3D Guidance `TrakStar' electromagnetic tracker system. This system allows us to record the position and orientation of each tracker with a frequency of about 200 Hz (and approximate precision of one millimeter in three dimensions), which is more than enough to capture human gestures with great accuracy and implement a DMP framework as described in the previous section.

In our initial tests we used only one tracker mounted on the right index finger. A rectangular grid was used that consisted of 1 cm by 1cm squares, laid on a table. The experimental procedure was as follows. We began by performing pointing gestures towards specific target positions on the grid, with the motion starting from the level of the table. In order to compare the trajectories in the three-dimensional task space and extract both visual and arithmetic measures of how well our model approaches the desired trajectories, we initially focused only on the 3D position of the tracker without considering its orientation. 
The deviation between different instances of the same gesture (same initial position, same goal position and same duration) was measured to evaluate the error introduced by a human actor (in repeated utterances of the same gesture) and how it compares to the error that is due to the generalisation properties of our DMP model.


In order to train the DMP model, the position, velocity and acceleration of the tracker motion is required, as described in the previous section. For this reason, a Savitzky-Golay filter was applied to smooth the raw position signals provided by the tracker sensor and calculate their (first- and second-order) derivatives. 
Subsequently, the equations of Section \ref{sec:DMPf} were applied to calculate the weight variables $w_i$ in the DMP models, which were then used along with initial and goal positions to generate trajectories. These DMP-generated trajectories were then compared to actual trajectories (having the same initial and goal positions) recorded from human demonstration (human actor executing pointing gestures, as described above).

The preliminary results obtained show that the trained DMP models perform quite well.
Table \ref{tab:errors} depicts the values of the Mean Absolute Error (MAE) and the Maximum Error (MAX) obtained (in millimeters) when comparing DMP-generated trajectories (trained from a single demonstration instance executed with a goal position at the center of the grid) to actual recorded trajectories (by a human actor demonstrating pointing gestures) with a variable goal position located right/left or front/back with respect to the center of the grid.

\begin{table}
  \centering
  \begin{tabular}{ l | c | c | c  || c | c | c  }
          & \multicolumn{3}{c||}{MAE} & \multicolumn{3}{c}{MAX} \\ \cline{2-7}
          & Left & Center & Right & Left & Center & Right \\ \hline\hline
    Front & 21.91 & 9.92 & 13.21 & 44.31 & 27.61 & 27.49\\ \hline
    Center & 20.89 & 1.59 & 18.78 & 52.61 & 3.34 & 49.39 \\ \hline
    Back & 13.21 & 17.39 & 13.50 & 29.18 & 47.32 & 36.19\\
    \hline
\end{tabular}
  \caption{Mean Absolute Error (MAE) and Maximum Error (MAX) values, comparing DMP-generated to actual recorded trajectories (by a human actor demonstrating pointing gestures)}
  \label{tab:errors}
\end{table}


\begin{figure}[bt] %[htbp]
	\centering
	%\floatconts
	\includegraphics[width=5.5in]{images/Trajectories_3D}
	\caption{Indicative DMP-generated trajectories compared to actual recorder trajectories (from human actor demonstrating pointing gestures)}
	\label{fig:dmp}
	%\end{centering}
\end{figure} 

Fig.~\ref{fig:dmp} depicts indicative DMP-generated trajectories compared to actual recorder trajectories (from human actor demonstrating pointing gestures). DMP model, in this figure, has been again trained from a single demonstration of pointing gesture towards the central position of the grid.  

Generally speaking, the DMP formulation provides a powerful framework to handle problems related to teaching robot gestures by human demonstrations. The performance achieved, as shown in these preliminary results, is adequate for the purpose of our objectives. We observed, however, some deviations in particular when the initial and the goal positions are very close, either in the learnt trajectory or in the generated trajectory. In the future we will modify the dynamic model appropriately to remedy this problem.
 Furthermore, as also stated in the previous section, we intend to expand the model in several ways: (a) to include other classes of gestures (in particular involving cooperative bi-manual motions), (b) to add coupling terms, as well as periodic segments in the motion, which can be used subsequently at a meta-learning stage in a parameterised space (for instance, to adapt the intensity or expressiveness of gestures), as further described in the following section. 