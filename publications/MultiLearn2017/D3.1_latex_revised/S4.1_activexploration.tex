Our contribution in this section includes research on the field of parameterized reinforcement learning for non-stationary environments, keeping in mind that the BabyRobot child-robot interaction (CRI) application scenarios can be modeled as such. More specifically, we considered a model of a robot-child interaction environment, with the robot and child being on the opposite sides of a desk. The initial scenario considered is as follows. Initially, robot and child are supposed to possess 3 cubes each and the task is to collaborate in order to built a tower at the center of the desk. Some important features of interest for state representation of this environment, would be the number of available cubes for manipulation at each episode (0-6), two boolean variables denoting the presence or not of cubes near the robot and near the child, and another two boolean variables denoting whether the robot hands and the child's hands are full (i.e they already hold a cube). The robot's actions could be modeled as \textit{do nothing}, \textit{point}, \textit{pick}, \textit{place}, \textit{give}, \textit{receive}, and all of them can have a set of parameters for tuning the natural components of each move, like speed, intensity, etc. The reward function of such a model can be based on child's engagement, or other pedagogically based utility functions that need to be explored in more extent. With the above scenario in mind, our research included the proposition of an active exploration algorithm for reinforcement learning (RL) in parameterized continuous action space. The basic theoretical formulation of this algorithm and the preliminary results obtained are briefly described in this section. More details of this work are presented in \cite{khamassi17} (included as Annex A in this deliverable), to be presented at the IEEE Conference on Robotic Computing 2017 (also being the subject of an ongoing extension for a future journal publication).

As a recent review for reinforcement learning applications to Robotics highlights \cite{Kober13}, many algorithms have been developed for different tasks. For stationary environments, human prior knowledge has also been used in order to determine the balance between exploration and exploitation, but such approach denotes weak performance for non-stationary environments. For continuous action spaces, important progresses have been made \cite{kober11,stulp13} with promising real world applications in the field of Robotics. Very recently though, the combination of discrete actions $A_d=\{a_1, a_2, ..., a_k\}$, where each action $a \in A_d$ is described by a set of $m_a$ continuous parameters $\{\theta_1^a, \theta_2^a, ...,\theta_{m_a}^a \} \in R^{m_z}$ was proposed for RL algorithms in structured Parameterized Action Space Markov Dedision Processes (PAMDP) \cite{masson16,hausknecht16}. This approach was applied to Robocup 2D soccer simulations, where an agent learned the optimal action (kick the ball, run, turn, etc) as also the optimal parameter value of each action (power, speed, angle, etc). Similar work has been done in \cite{hausknecht16}, where the optimal action and parameters are learned in parallel, instead of altering between learning phases, in order to ensure convergence. However, these methods use a fixed exploration-exploitation trade-off which results in issues discussed in \cite{Kober13}.

Exploration in parameterized action space described in \cite{hausknecht16} uses $\epsilon$-greedy exploration by picking a random discrete action $a \in A_d$ with probability $\epsilon$ and then sample the action's parameters $\theta_i^a$ out of a uniform distribution. The value of $\epsilon$ is decreased over time steps, but the decrease rate is set by hand, thus requiring human prior knowledge. In \cite{vanhasselt07} a Gaussian distribution is used instead, but with a fixed value $\sigma$. In our work, instead, we apply the meta-learning algorithm proposed in \cite{schweighofer03}, and use short-term and long-term reward running averages to both adaptively tune the inverse temperature parameter $\beta$ of Boltzmann softmax exploration function for action selection, as well as the width of the Gaussian distribution from which each action parameter is sampled around its current value. We applied our proposed algorithm to a simple simulated human-robot interaction task where the objective was to maximize human engagement (modeled with a function unknown to the robot). The results we obtained outperformed continuous parameterized RL both without active exploration and with active exploration when using Kalman RL-algorithm \cite{geist10} based on uncertainty variations. We also applied a simplified version of our algorithm on the same non-stationary multi-armed bandit setup of \cite{garivier08} where the achieved performance was similar to the state-of-the-art (however, we extended our research for such kind of problems and present our results in the following Section ~\ref{sec:MAB}).

Let us briefly describe a bit more in detail the theoretical formulation of this meta-learning algorithm (more details can be found in \cite{khamassi17} - Annex A). The proposed algorithm at first employs Q-Learning \cite{watkins92} in order to learn the value of action $a_t \in A_d$ of state $s_t$ at every timestep $t$, using the update rule:
\[Q_t(s_t, a_t) = Q_t(s_t, a_t)+ \alpha_Q \delta_t^Q\]
where $\alpha_Q$ is a learning rate and
\[ \delta_t^Q = r_t + \gamma \max_{a} (Q_t(s_{t+1}, a)) - Q_t(s_t, a_t)\]
with $\gamma$ denoting a discount factor and $r_t$ the immediate reward. An action $a_j$ is then taken with probability according to Boltzmann softmax function  $P(a_j|s_t, \beta_t) = exp(\beta_t, Q_t(s_t, a_t)) / \sum_a exp(\beta_t, Q_t(s_t, a))$, where $\beta_t$ is the dynamic inverse temperature meta-parameter which will be tuned with meta-learning. In parallel, continuous parameters $\widetilde{\theta}_{i,t}^{a_j}$ for this action are selected from a Gaussian exploration function centered on the current parameter values $\theta_{i,t}^{a_j}(s_t)$ of the current state, with
\[ P(\widetilde{\theta}_{i,t}^{a_j} | s_t, a_j, \sigma_t) = exp ( -(\widetilde{\theta}_{i,t}^{a_j} -  \theta_{i,t}^{a_j}(s_t))^2 / (2\sigma_t^2) ) / \sqrt{2\pi\sigma_t} \]
where $\sigma_t$ is also a meta-parameter which will be tuned through meta-learning and action parameters $\theta_{i,t}^{a}(s_t)$ are learned with a continuous actor-critic algorithm \cite{vanhasselt07}. We then used the reward prediction error of the critic as $\delta_t^V=r_t+\gamma V_t(s_t+1)-V_t(s_t)$ to update the parameter vectors $\omega_t^C$ and $\omega_t^A$ of the critic and actor of the neural network function approximations as described in \cite{caluwaerts12}, where:
 \[\omega_{i,t+1}^C=\omega_{i,t}^C +\alpha_C \delta_t^V (\delta V_t(s_t) / \delta \omega_{i,t}^C)\]
 and
 \[ \omega_{i,t+1}^A=\omega_{i,t}^A +\alpha_A \delta_t^V  (\widetilde{\theta}_{i,t}^{a} -  \theta_{i,t}^{a}(s_t))(\delta V_t(s_t) / \delta \omega_{i,t}^C)\]
 with $\alpha_C$ and $\alpha_A$ denoting learning rates. We used a noiseless version of meta-learning algorithm (contrary to \cite{schweighofer03} which can lead to instability), in order to update $\beta_t$ and $\sigma_t$. In more detail, we computed the short-term and long-term average rewards $\overline{r_t}$ and $\overline{\overline{r_t}}$ respectively, where $\Delta \overline{r_t} = (r_t-\overline{r_t}) / \tau_1$ and $\Delta \overline{\overline{r_t}} = (\overline{r_t}-\overline{\overline{r_t}}) / \tau_2$ (with $\tau_1$ and $\tau_2$ denoting two time constants) and updated $\beta_t$ with $\beta_{t+1}=(R \circ F)(\beta_t, \mu \tau_2 \Delta \overline{\overline{r_t}})$ and $\sigma_t$ with $\sigma_{t+1} = G(\mu \tau_2 \Delta \overline{\overline{r_t}} )$, where $R(\cdot)$ is a rectifier, $F(\cdot)$ is affine, $G(\cdot)$ is a sigmoid (with range equal to parameter range) and $\mu$ is a learning rate.


\begin{figure}[!b] %[htbp]
	\centering
	%\floatconts
	\includegraphics[width=6.6in]{images/active.png}
	\caption{A: no active exploration $\sigma=20$, B: no active exploration with $\sigma=10$, C: Kalman Q-Learning with exploration bonuses, D: Active exploration with meta-learning, E: active exploration with meta-learning (red), Kalman Q-Learning with exploration bonuses (green), Algorithm without active exploration (blue), F: Snapshot of the implemented V-Rep simulation environment}
	\label{fig:active}
	%\end{centering}
\end{figure}

We tested our algorithm in a simple simulated human-robot interaction task for a single state, 6 discrete actions and continuous action parameters between -100 and 100. We modeled the rewards to be returned only when the optimal action is chosen and the parameter is close to the optimal value $\mu^{\star}$ which changed every $n$ timesteps, requiring re-exploration and learning by the robot. Since during interaction tasks the robot actions may have delayed effects on human's behavior and engagement \cite{anzalone15}, we chose the reward to be given by a dynamical system, based on the virtual engagement $e(t)$ of the human, which represents the attention that the human pays to the robot. Numerically speaking, we modeled this attention to be between 0 (no attention) to 10 (maximum attention), and the reward function was computed as $r(t+1)=(1-\lambda)e(t+1)-\lambda \Delta e(t+1)$ where $\lambda$ is a chosen weight (we chose 0.7 for our simulations).


At every timestep our virtual engagement model was described by an optimal action $a^{\star}$ and an optimal parameter value of that action $\mu^{\star}$ with some variance $\sigma^{\star}$ as tolerance. The engagement was increased as $e(t+1) = e(t) + \eta_1 (e_{max}-e(t))H(\theta_t^a)$ when the optimal action was chosen and the value of $H(\theta_t^a) = 2 (exp(-(\theta_t^a-\mu^{\star})^2/(2\sigma^{\star 2}))-0.5)$, was greater or equal to zero, denoting that the parameter value was in the tolerance area. Similar types of updates which decrease virtual engagement where used when the optimal action was made but the action parameter was not in the range of tolerance, as also when the action was not the optimal one (different rules for each of them). This formulation has great meaning, since the reward is modeled by using both the engagement's current value as also its rate of change.

At first we run the simulation without active exploration (we used a fixed $\sigma = 20$) in a task where optimal action was $a_6$ and its optimal parameter value $\mu^{\star}$ was -20 for the first 200 timesteps (we used a fixed $\sigma^{\star}=10$ for all experiments). Then the optimal action switched to $a_2$ with the same optimal parameter until timestep 600. We observed that the engagement was never over 7.5 due to the large value of $\sigma$ (see Fig.\ref{fig:active}A). With smaller $\sigma$ values, higher engagement values were achieved, but when the optimal new action parameter was too far away from the previous one, the performance was poor due to lack of adequate exploration (see Fig.\ref{fig:active}B). We then tested active exploration with Kalman Q-Learning algorithm, where we altered the original version by adding the diagonal terms of the covariance matrix (after multiplication by some weight $\eta$) as action specific exploration bonuses $b_t^a$ to the Q-values of softmax function, and also used them to tune action specific $\sigma_t^a$. For our simulations, we altered the optimal tuple $(a^{\star}, \mu^{\star})$ from $(a_2, -20)$ to $(a_6, 20)$ every 400 timesteps. The diagonal terms of the covariance matrix in Kalman filter decrease monotonically with timesteps, resulting to poor performance after 4000 timesteps as seen in Fig.\ref{fig:active}C (about 50/50 action choice probability). Finally we tested active exploration with the meta-learning algorithm in a more difficult task (see Fig.\ref{fig:active}D), where the optimal action tuple alternated between $(a_2, -50)$ to $(a_6, 50)$ every 1000 time steps. When the engagement dropped, the inverse temperature parameter $\beta_t$ also dropped and the action parameter variance $\sigma_t$ of the gaussian exploration function increased. This resulted on re-engaging exploration when needed, without loosing the exploitation convergence on large stationary intervals.

We repeated the last task for all algorithms running 10 simulations for each one, and monitored the average and standard deviation of the engagement (Fig.\ref{fig:active}E). The algorithm without active exploration never exceeded an engagement over 6 for every interval and the Kalman Q-learning adapted fast but progressively decreased its achieved engagement. Our active exploration with meta-learning algorithm achieved the best performance, reaching high values of engagement (optimum at some intervals).

We also created a virtual simulation environment using V-REP robot simulator, in order to have a more realistic representation before any application to the real world (Fig.\ref{fig:active}F). In this simulation environment, we considered a preliminary scenario where 6 cubes were present in front of the robot. We assumed that the optimal action at this state was the pointing gesture and the task was to find the optimal action intensity which maximized the human attention to the pointed cube. In this preliminary work, the engagement value was returned to the robot at every timestep, however more realistic scenarios are also being considered and tested in simulation, as described in the following subsection.

\subsubsection{Engagement Estimation Process in the simulated HRI scenario}
In order to make the simulated HRI scenario more realistic and obtain a more reliable assessment on the applicability of the developed learning algorithms in real use-case scenarios, we consider the human engagement to be unknown and further assume that an estimation process is in place based on visual features measured during human-robot interactions. The goal is to conduct an initial evaluation as to how scalable and generalizable the proposed learning algorithms are in more close to real-life scenarios and how the presence of an uncertainty on human engagement estimation may affect the performance of the system.

As mentioned in \cite{anzalone15,ooko2011}, head pose data is proved to be highly correlated with human engagement. In the current implementation of the simulated HRI scenario, the human head pose is generated by sampling a normal distribution centered around the position of the object corresponding to the action (i.e. pointing gesture) currently performed by the robot. The standard deviation of this probability distribution is assumed to be proportional to the absolute difference between the robot's executed action parameter and its optimal value. Thus, when the action parameter deviates from its optimal value, the human engagement drops and the head pose variance increases, meaning that the human is disengaged from the task and essentially starts looking around.

The engagement estimation is achieved by measuring the mean standard deviation (MSD) of the human's head yaw angle with respect to the cube pointed by the robot in a specified time window. In particular, at each trial the robot collects and processes $n$ (here $n=5$) observations of the human head pose before selecting and executing a new action. The head pose measurement error is taken into account and modeled as an additive Gaussian noise with standard deviation $\sigma$ that depends on the accuracy of the visual head pose estimation. It is thus evident that the higher the head pose MSD, the lower the human engagement. Our estimator evaluates the current human engagement based on this MSD value and provides it to the robot as a reward. The reward function now considers the estimated engagement $\hat{e}$ and is computed as $r(t+1) = (1-\lambda)\hat{e}(t+1) + \lambda \Delta \hat{e}(t+1)$.

The first series of numerical experiments that we conducted involves step changes in the optimal (continuous) action parameter performed every 100 trials. Figure \ref{fig:fig0} depicts the results of a single run for the estimated vs. the real (simulated) engagement, when the robot is assumed to collect $n=5$ head pose observations per trial (for the human engagement estimation process) with a Gaussian measurement noise of $\sigma=1$ (one run involving a step change of $100\%$ after 100 trials). It is found that in such a situation the real (simulated) engagement does not drop below a value of $7$ (i.e. $70\%$ from the optimal engagement) and consistently converges rapidly to a value above $90\%$ after approximately $10-15$ trials.
A series of 50 runs for the same step-change scenario has also been conducted and the results are shown in Figure \ref{fig:fig1}.
It is assumed that the optimal action parameter undergoes a change of 100\% (i.e. doubles from a value of $5$ to a value of $10$) and after 100 trials goes back to its initial value. Figure \ref{fig:fig1} (left) shows the actual executed action parameter (mean and variance after 50 runs). These results show that although the optimal action parameter suddenly doubled, the adaptation was fast enough to keep the engagement above a $70\%$ value and consistently make it converge to a value above $90\%$ after approximately $15$ trials (Figure \ref{fig:fig1}, right).

\begin{figure}[tbp]
	\centering
	%\floatconts
	\includegraphics[width=8cm]{./figures/png/compare_estimated_engagement} %[width=\textwidth]
	\caption{Estimated vs. real (simulated) engagement in one run involving a step change (of $100\%$) after 100 trials, based on $n=5$ head pose observations per trial and Gaussian measurement noise with $\sigma=1$.}
	\label{fig:fig0}
\end{figure}

\begin{figure}[tbp]
	\centering
	%\floatconts
	\includegraphics[width=16cm]{./figures/png/step_change}
	\caption{Left: Executed action parameter (mean and variance after 50 runs) involving step changes. Right: Real (simulated) human engagement (mean and variance after 50 runs) when the optimal action parameter undergoes step changes every 100 trials as shown in left figure (engagement does not drop below a $70\%$ value).}
	\label{fig:fig1}
\end{figure}

\begin{figure}[tbp]
	\centering
	%\floatconts
	\includegraphics[width=16cm]{./figures/png/engagement_param_perturbation_duration}
	\caption{Action parameter perturbations with durations of 1, 2, 5 and 10 trials, respectively.}
	\label{fig:fig3}
\end{figure}


During a human-robot interaction task, it is natural for a person and much more for a child to be distracted by an external event (loud noise, presence of other people, etc). We simulate such a perturbation as an abrupt and short in time (impulse-type) change of the optimal action parameter. The behavior of the algorithm (mean and variance of 50 sample runs) is depicted in Figure \ref{fig:fig3} for various durations of the perturbation impulse. Here, the optimal parameter has a value of $5$ which is changed to $10$ during the perturbation.
We observe that when the perturbation lasts for only $1$ trial, the executed action parameter is almost unaffected and the human engagement does not drop lower than a value of $9$.


\begin{figure}[bt]
	\centering
	%\floatconts
	\includegraphics[width=5.5in]{./figures/png/engagement_parameter_MAD_values}
	\caption{Performance for increasing perturbation duration. Left: Engagement deviation from its maximum value (10). Right: Action parameter deviation from its optimal value (5). The measurement noise has a $\sigma=1$.}
	\label{fig:fig4}
\end{figure}


\begin{figure}[tb]
	\centering
	%\floatconts
	\includegraphics[width=5.5in]{./figures/png/noise_engagement_parameter_MAD_values}
	\caption{Performance for increasing noise $\sigma$. Left: Engagement deviation from its maximum value (10). Right: Action parameter deviation from its optimal value (5). The measurement noise has $\sigma=1$.}
	\label{fig:fig5}
\end{figure}


In order to further quantify the performance of the learning algorithm, we calculate the mean absolute deviation (MAD) of the real (simulated) engagement and of the executed action parameter from their optimal values, for perturbation durations in the range of $1$ to $10$ trials. The results are depicted in Figure \ref{fig:fig4}, indicating that longer perturbations lead to slower adaptation and result in larger MAD values. The same results are also numerically shown in Tables \ref{tab:eng_mad} to \ref{tab:trials90} (end of the report), including the maximum engagement deviation as well as the number of trials needed for the engagement to recover $90\%$ of its maximal value after the end of the perturbation.
It should also be highlighted, though, that as illustrated by the obtained results, no matter how long the perturbation,
the algorithm will always reconverge to the optimal value.

In a similar way, Figure \ref{fig:fig5} shows the engagement and action parameter deviation for an increasing value of $\sigma$ representing the head pose measurement noise. In this particular numerical experiment, the perturbation duration is assumed to last for a single trial. It is clear from this figure that larger $\sigma$ values (i.e. amplitude of noise in the head pose data and consequently in the human engagement estimation process) result in larger deviations for the engagement and for the action parameter from their optimal values. However, it is also apparent that there is a range of values in $\sigma$ (corresponding to a range of uncertainty in the human engagement estimation process) that results in a quite robust system performance (numerically, in this case, up to a value of $\sigma = 2$). Evaluating the robustness of the learning mechanisms under assumed measurement noise and proposing counter-measures in the presence of large estimation uncertainties is a work-in-progress and will be further explored during year 2 of the project.


%
%	\def\arraystretch{1.1}
%    \setlength\tabcolsep{12pt}
	\begin{table}[H]
	\centering
  	\begin{tabular}{c | c c c c}
  	\multicolumn{5}{c}{\bf Engagement MAD values} \\
    \hline
    \multirow{2}{*}{\bf Perturbation } &
          \multirow{2}{*}{\bf Mean} &
          \multirow{2}{*}{\bf STD} &
          \multirow{2}{*}{\bf 25\%} &
          \multirow{2}{*}{\bf 75\%} \\
          \bf duration & & & \bf percentile & \bf percentile \\
    \hline
    \hline
1&0.34939&0.17679&0.23192&0.45413\\\hline
2&0.52831&0.14063&0.44253&0.60892\\\hline
3&0.68629&0.22575&0.53915&0.77973\\\hline
4&0.89065&0.17484&0.77258&0.99155\\\hline
5&1.0118&0.31078&0.81102&1.1657\\\hline
6&1.2232&0.34815&0.99221&1.3546\\\hline
7&1.2957&0.61759&0.96951&1.4515\\\hline
8&1.249&0.38641&1.0064&1.5177\\\hline
9&1.3374&0.39675&1.036&1.6112\\\hline
10&1.4415&0.44524&1.1093&1.7176\\\hline
  	\end{tabular}
  	\caption{Engagement MAD values}
  	\label{tab:eng_mad}
	\end{table}


	\begin{table}[H]
	\centering
  	\begin{tabular}{c | c c c c}
  	\multicolumn{5}{c}{\bf Action parameter MAD values} \\
    \hline
    \multirow{2}{*}{\bf Perturbation } &
          \multirow{2}{*}{\bf Mean} &
          \multirow{2}{*}{\bf STD} &
          \multirow{2}{*}{\bf 25\%} &
          \multirow{2}{*}{\bf 75\%} \\
          \bf duration & & & \bf percentile & \bf percentile \\
    \hline
    \hline
1&1.0337&0.42043&0.7643&1.2693\\\hline
2&1.1584&0.35795&0.89604&1.3987\\\hline
3&1.2774&0.31868&1.0767&1.4055\\\hline
4&1.3416&0.29543&1.1319&1.5626\\\hline
5&1.4847&0.30732&1.2685&1.7003\\\hline
6&1.6898&0.35503&1.4562&1.9139\\\hline
7&1.5712&0.44293&1.2896&1.7477\\\hline
8&1.7163&0.3111&1.5788&1.9216\\\hline
9&1.9387&0.48988&1.5668&2.2712\\\hline
10&1.8471&0.37307&1.5986&2.1704\\\hline
  	\end{tabular}
  	\caption{Action parameter MAD values} \vspace*{2cm}
  	\label{tab:param_mad}
	\end{table}

	\begin{table}[H]
	\centering
  	\begin{tabular}{c | c c c c}
  	\multicolumn{5}{c}{\bf Engagement max deviation} \\
    \hline
    \multirow{2}{*}{\bf Perturbation } &
          \multirow{2}{*}{\bf Mean} &
          \multirow{2}{*}{\bf STD} &
          \multirow{2}{*}{\bf 25\%} &
          \multirow{2}{*}{\bf 75\%} \\
          \bf duration & & & \bf percentile & \bf percentile \\
    \hline
    \hline
1&1.1558&0.48699&0.88976&1.3209\\\hline
2&1.7664&0.36121&1.6266&1.8775\\\hline
3&2.2081&0.56905&1.9838&2.6328\\\hline
4&2.8998&0.45312&2.5507&3.2255\\\hline
5&3.2186&0.75891&2.7269&3.7832\\\hline
6&3.8114&0.72485&3.3217&4.3216\\\hline
7&4.0132&1.1746&3.5341&4.6\\\hline
8&4.0311&0.98245&3.3356&4.8395\\\hline
9&4.2334&1.1234&3.3556&5.0483\\\hline
10&4.5097&1.0771&3.8666&5.2737\\\hline
  	\end{tabular}
  	\caption{Engagement max deviation}
  	\label{tab:eng_max_deviation}
	\end{table}

	\begin{table}[H]
	\centering
  	\begin{tabular}{c | c c c c}
  	\multicolumn{5}{c}{\large\bf Number of trials to 90\%} \\
    \hline
    \multirow{2}{*}{\bf Perturbation } &
          \multirow{2}{*}{\bf Mean} &
          \multirow{2}{*}{\bf STD} &
          \multirow{2}{*}{\bf 25\%} &
          \multirow{2}{*}{\bf 75\%} \\
          \bf duration & & & \bf percentile & \bf percentile \\
    \hline
    \hline
1&1.4&2.8714&0&0\\\hline
2&5.72&3.5516&3&8\\\hline
3&6.2&4.6861&3&7\\\hline
4&5.3&3.6936&3&6\\\hline
5&7.66&4.7536&4&11\\\hline
6&9&5.9074&5&11\\\hline
7&9.42&6.2632&5&11\\\hline
8&11.5&7.0226&7&14\\\hline
9&11.6&8.1039&6&16\\\hline
10&12.2&9.6637&6&16\\\hline
  	\end{tabular}
  	\caption{Number of trials to 90\%}
  	\label{tab:trials90}
	\end{table}


%	\begin{table}[H]
%	\centering
%  	\begin{tabular}{c | c c c c}
%  	\multicolumn{5}{c}{\bf Number of trials to 90\%} \\
%    \hline
%    \multirow{2}{*}{\bf Perturbation } &
%          \multirow{2}{*}{\bf Mean} &
%          \multirow{2}{*}{\bf STD} &
%          \multirow{2}{*}{\bf 25\%} &
%          \multirow{2}{*}{\bf 75\%} \\
%          \bf duration & & & \bf percentile & \bf percentile \\
%    \hline
%    \hline
%1&4.54&4.464&2&5\\\hline
%2&5.78&4.2919&3&7\\\hline
%3&6.96&5.2604&4&8\\\hline
%4&8.66&5.8645&4&10\\\hline
%5&9.58&4.5629&6&12\\\hline
%6&12.14&6.6762&7&17\\\hline
%7&9.9&7.3297&4&14\\\hline
%8&14.02&6.8288&9&17\\\hline
%9&14.16&7.6568&9&20\\\hline
%10&15.14&8.4877&9&22\\\hline
%  	\end{tabular}
%  	\caption{Number of trials to 90\%}
%  	\label{tab:trials90}
%	\end{table}




%\begin{figure}[htbp]
%	\centering
%	%\floatconts
%	\includegraphics[width=5in]{./figures/png/noise_engagement_max_deviation_trials_to_90}
%	\caption{Left: Engagement maximum deviation. Right: Number of trials to 90\%}
%	\label{fig:fig6}
%\end{figure}

