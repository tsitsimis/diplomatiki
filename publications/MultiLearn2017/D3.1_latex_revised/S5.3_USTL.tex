This section shows how we can modify a Value-based approach in Reinforcement Learning with continuous state-action spaces in order to retrieve a Policy Search approach namely Policy-Gradient. This allows us to make the link between Policy Search and Value-based methods and give us some new insights to create algorithm to solve Robotics problem with RL.

Policy Search (PS)~\cite{ng2000pegasus,fix2012monte,levine2013guided,deisenroth2013survey} and more specifically Policy Gradient (PG) methods~\cite{sutton1999policy,peters2005natural,degris2012model,silver2014deterministic,lillicrap2016continuouss} are well known to have a practical edge compared to value-based methods when it comes to Reinforcement Learning (RL) for Markov Decisions Processes (MDPs) with continuous state and action spaces. This principally comes from the fact that value-based methods, either inspired by the Policy Iteration (PI)~\cite{lagoudakis2003least} or Value Iteration (VI) algorithms~\cite{ernst2005tree,riedmiller2005neural}, rely on the computation of the maximum of the action-value function over the set of actions: $\max_{a\in A} Q(\pi,s,a)$ (greedy step). If this set is infinite, then those methods become intractable. However, it is possible to adapt Policy Iteration-based RL algorithms, such as Least Squares Policy Iteration (LSPI)~\cite{lagoudakis2003least}, by changing this global greedy step into a local improvement of the policy made by a gradient step. Indeed, let us take a closer look to the PI method for a finite action space. It consists in two steps, a first step of evaluation of the current policy $\pi_k$ in order to obtain the action-value function $Q(\pi_k,s,a)$ followed by a greedy step $\pi_{k+1}(s)=\argmax_{a\in A}Q(\pi_k,s,a)$ which improves the action-value function:
\begin{tcolorbox}[enhanced,
  watermark opacity=1,watermark zoom=1,
  colback=white,colframe=black,
  fonttitle=\bfseries, title=Discrete PI scheme]
\begin{itemize}
\item Evaluation of $Q(\pi_k,.,.)$.
\item Greedy step: $\pi_{k+1}(s)=\argmax_{a\in A}Q(\pi_k,s,a)$.
\end{itemize}
\end{tcolorbox}
In practice, when faced with batch data, the evaluation step can be realized for instance by the Least Square Temporal Difference (LSTD) algorithm~\cite{bradtke1996linear} if features for the action-value function are provided. When no features are provided, one could easily adapt for instance the Fitted-$Q$ algorithm~\cite{ernst2005tree} that evaluates the optimal action value function in order to evaluate the current value function by replacing the optimality-operator by the evaluation operator.  

Obviously, when the action space is continuous such a greedy step becomes cumbersome. However, a local improvement is still possible if we are able to compute the gradient of the actions-value function over the possible actions $\frac{\partial Q(\pi_k,s,\pi_k(s))}{\partial a}$. Then, the global greedy step can be replaced by a local gradient-like improvement step: $\pi_{k+1}(s)=\pi_k(s) +\alpha_k \frac{\partial Q(\pi_k,s,\pi_k(s))}{\partial a}$, where $\alpha_k\in\mathbb{R}_+$. Thus, a canonical adaptation of the PI method in the continuous context is:
\begin{tcolorbox}[enhanced,
  watermark opacity=1,watermark zoom=1,
  colback=white,colframe=black,
  fonttitle=\bfseries, title=Continuous PI scheme]
\begin{itemize}
\item Evaluation of $Q(\pi_k,.,.)$.
\item Local improvement step: $\pi_{k+1}(s)=\pi_k(s) +\alpha_k \frac{\partial Q(\pi_k,s,\pi_k(s))}{\partial a}$.
\end{itemize}
\end{tcolorbox}
Our main contribution consists in showing, after introducing some notations relative to MDPs and differentiation, that this PI method for continuous states and actions MDPs (called continuous PI) is in fact a sound algorithm. More precisely, it is a direct derivation of the PG method when the differentiation of the mean value $J_\nu(\pi)=\int_S V^\pi(s)\nu(ds)$ is directly done over the policy without parameterizing it. This establishes a strong link between PI and PG methods in the continuous setting. It should be noted that this link between PI and PG methods have already been studied in the finite action setting~\cite{scherrer2014local,schulman2015trust} where the relation between Conservative Policy Iteration (CPI)~\cite{kakade2002approximately} and PG methods has been highlighted. Finally, as a second contribution, we provide several practical instantiations of this continuous PI method which can be either feature-based or no.