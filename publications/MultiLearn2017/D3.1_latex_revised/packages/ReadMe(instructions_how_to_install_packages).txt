Instructions on how to install a new latex package

1. Run "latex <package_name>.ins"
2. Move <package_name> folder (containing .sty file) to the "C:\texmf\tex\latex" folder.
3. Run "texhash" to update package list
