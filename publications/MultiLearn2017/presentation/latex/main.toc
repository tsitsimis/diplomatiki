\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Parameterized Actions}{3}{0}{2}
\beamer@sectionintoc {3}{Active Exploration Algorithm}{6}{0}{3}
\beamer@sectionintoc {4}{Experiments-Simulations}{7}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{13}{0}{5}
